#include "Button.h"


Button::Button(std::string aSpriteName, int x, int y, int w, int h )
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
	spriteName = aSpriteName;
	load_files();
	set_clips();
    //Set the default sprite
    clip = &clips[ CLIP_MOUSEOUT ];
}

bool Button::handle_events(SDL_Event evt)
{
    //The mouse offsets
    int x = 0, y = 0;
   
    //If a mouse button was pressed
    if( evt.type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was pressed
        if( evt.button.button == SDL_BUTTON_LEFT )
        {
            //Get the mouse offsets
            x = evt.button.x;
            y = evt.button.y;

            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
                //Set the button sprite
                clip = &clips[ CLIP_MOUSEDOWN ];
				return true;
            }
			return false;
        }
		return false;
    }
	return false;
}

void Button::show(SDL_Renderer* renderer, int screenWidth, int screenHeight)
{
	
    //Show the button   

	SDL_Rect SrcRInferior;
    SrcRInferior.x = 0;
	SrcRInferior.y = 0;
	SrcRInferior.w = box.w;
	SrcRInferior.h = box.h;
	
	SDL_Rect DestRInferior;
	DestRInferior.x = box.x;
	DestRInferior.y = box.y;
	DestRInferior.w = box.w;
	DestRInferior.h = box.h;

	SDL_Texture * txt = SDL_CreateTextureFromSurface(renderer, buttonSheet);
	SDL_RenderCopy(renderer, txt , &SrcRInferior, &DestRInferior);
	SDL_DestroyTexture(txt);
}


SDL_Surface* Button::load_image( std::string filename )
{
    //The image that's loaded
    SDL_Surface* loadedImage = NULL;

    //The optimized surface that will be used
    SDL_Surface* optimizedImage = NULL;

    //Load the image
    return IMG_Load( filename.c_str() );

    //If the image loaded
    if( loadedImage != NULL )
    {
        //Create an optimized surface
        optimizedImage = SDL_ConvertSurfaceFormat(loadedImage,SDL_PIXELFORMAT_UNKNOWN,0);

        //Free the old surface
        SDL_FreeSurface( loadedImage );

        //If the surface was optimized
        if( optimizedImage != NULL )
        {
            //Color key surface
            SDL_SetColorKey( optimizedImage, SDL_TRUE, SDL_MapRGB( optimizedImage->format, 0, 0xFF, 0xFF ) );
        }
    }

    //Return the optimized surface
    return optimizedImage;
}


bool Button::load_files()
{
    //Load the button sprite sheet
    buttonSheet = load_image( "./resources/images/"+spriteName+ ".png" );

    //If there was a problem in loading the button sprite sheet
    if( buttonSheet == NULL )
    {
        return false;
    }

    //If everything loaded fine
    return true;
}

void Button::clean_up()
{
    //Free the surface
    SDL_FreeSurface( buttonSheet );

    //Quit SDL
    SDL_Quit();
}

void Button::set_clips()
{
    //Clip the sprite sheet
    clips[ CLIP_MOUSEOVER ].x = 0;
    clips[ CLIP_MOUSEOVER ].y = 0;
    clips[ CLIP_MOUSEOVER ].w = 320;
    clips[ CLIP_MOUSEOVER ].h = 240;

    clips[ CLIP_MOUSEOUT ].x = 320;
    clips[ CLIP_MOUSEOUT ].y = 0;
    clips[ CLIP_MOUSEOUT ].w = 320;
    clips[ CLIP_MOUSEOUT ].h = 240;

    clips[ CLIP_MOUSEDOWN ].x = 0;
    clips[ CLIP_MOUSEDOWN ].y = 240;
    clips[ CLIP_MOUSEDOWN ].w = 320;
    clips[ CLIP_MOUSEDOWN ].h = 240;

    clips[ CLIP_MOUSEUP ].x = 320;
    clips[ CLIP_MOUSEUP ].y = 240;
    clips[ CLIP_MOUSEUP ].w = 320;
    clips[ CLIP_MOUSEUP ].h = 240;
}