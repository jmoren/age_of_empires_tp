#ifndef BUTTON_HEADER
#define BUTTON_HEADER

//The headers
#include "SDL.h"
#include "SDL_image.h"
#include <string>
#include "../Stage/Headers/Mapa.h"
//The button
class Button
{
    private:
    //The attributes of the button
    SDL_Rect box;

    //The part of the button sprite sheet that will be shown
    SDL_Rect* clip;

	SDL_Rect clips[ 4 ];

	//The button states in the sprite sheet
	static const int CLIP_MOUSEOVER = 0;
	static const int CLIP_MOUSEOUT = 1;
	static const int CLIP_MOUSEDOWN = 2;
	static const int CLIP_MOUSEUP = 3;
	
	SDL_Surface *buttonSheet;

	SDL_Surface* Button::load_image( std::string filename);

	std::string spriteName;

    public:
    //Initialize the variables
    Button(std::string aSpriteName, int x, int y, int w, int h );

    //Handles events and set the button's sprite region
    bool handle_events(SDL_Event evt);

    //Shows the button on the screen
    void show(SDL_Renderer* renderer, int screenWidth, int screenHeight);

	void set_clips();

	bool load_files();

	void clean_up();
	

};

#endif