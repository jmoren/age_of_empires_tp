#include "SDL.h"
#include "SDL_image.h"
#include "parser/parser.h"
#include "parser/config.h"
#include "logger/logger.h"
#include "window/Window.h"

#include <exception>
using namespace std;

#define RESTART 30

void returnErrorMessage(int value){
	Logger *logger = Logger::Instance();
	string message;

	switch (value){
	case WINDOW_FAILED:
		message = "No se pudo inicializar la ventana";
		break;
	case CONNECTION_FAILED:
		message = "No se pudo establecer la conextion con el server";
		break;
	case MAPA_FAILED:
		message = "No se pudo inicializar el mapa";
		break;
	case -3:
		message = "No se encontro la key en la configuracion";
		break;
	case -2:
		message = "Error en el archivo";
		break;
	case -1:
		message = "Archivo no existente";
		break;
	case 0:
		message = "Exito!";
		break;
	default:
		message = "Error desconocido";
		break;
	}

	logger->log(Logger::TypeMessage::LOG_ERROR, message);
}

int main(int argc, char *argv[]){
	Logger *logger = Logger::Instance();
	Config *config = new Config();
	
	Parser *parser;
	Window *window;
	int status = RESTART;

	while(status == RESTART){
	
		parser = new Parser(config);
		status = parser->parse();
			
		if (status < 0)
			return status;
		
		status = -12;
		window = new Window(config);
		
		while(status == -12){
			status = window->parseConnectionResponse();
		}

		if (config)
			delete(config);
		if (parser)
			delete(parser);
		
		if (window && status != -11 && status != -12)
			delete(window);
	}
	
	if (status < 0)
		returnErrorMessage(status);

	if (logger)
		delete(logger);

	return status;
}

