#include "Tile.h"
#include "Player.h"

Tile::~Tile(){
	this->elem = nullptr;
	this->spriteAsociado = nullptr;
}

void Tile::render(int cameraOffsetX, int cameraOffsetY, float zoom, string realPlayerName){

	SDL_Texture *texture = this->spriteAsociado->Textura;
	EntityBase *element = nullptr;

	if (this->elem != nullptr){
		element = this->elem;
	}

	/*this->visited = true;
	this->lit = true;*/
	if (this->lit){
		SDL_SetTextureAlphaMod(texture, 255);
		if (element != nullptr){
			elem->blendValue = 255;
			elem->spriteAsociado->setDynamicBlend(255);
		}
		this->lit = false;
	}
	else{
		if (this->visited){
			SDL_SetTextureAlphaMod(texture, 100);
			if (element != nullptr){
				elem->blendValue = 100;
				elem->spriteAsociado->setDynamicBlend(0);
			}
		}
		else{
			SDL_SetTextureAlphaMod(texture, 0);
			if (element != nullptr){
				elem->blendValue = 0;
				elem->spriteAsociado->setDynamicBlend(0);
			}
		}
	}

	this->spriteAsociado->RenderTileSprite(cameraOffsetX, cameraOffsetY, zoom, 0, this->posx, this->posy);
}

void Tile::setSprite(Sprite *sprite){
	this->spriteAsociado = sprite;
}

Sprite* Tile::getSprite(){
	return this->spriteAsociado;
}


bool Tile::selectedValidUnit(string realPlayerName){
	if (this->elem != nullptr){
		if (string(this->elem->clientName).compare(realPlayerName) == 0){
			return true;
		}
	}
	return false;
}

EntityBase* Tile::getElement(){

	return this->elem;
}

int Tile::getRelativePositionX(int cameraOffsetX, int cameraOffsetY, float zoom){
	return EntityBase::getRelativePositionX(cameraOffsetX, cameraOffsetY, zoom);
}

int Tile::getRelativePositionY(int cameraOffsetX, int cameraOffsetY, float zoom){
	return EntityBase::getRelativePositionY(cameraOffsetX, cameraOffsetY, zoom);
}

bool Tile::is_walkable(){
	return this->walkable;
}

void Tile::linkElement(EntityBase* newEntityBase){
	this->elem = newEntityBase;
}

void Tile::unlinkElement(){
	this->elem = nullptr;
}

void Tile::setWalkable(){
	this->walkable = true;
}

void Tile::setNotWalkable(){
	this->walkable = false;
}

void Tile::setOccupied(){
	this->occupied = true;
}

void Tile::setNotOccupied(){
	this->occupied = false;
}
