#include "Player.h"
#include <algorithm>
Player::Player(){
}

Player::Player(string clientName, int id, string name, Sprite *spriteAsociado, int tileX, int tileY) :
		EntityBase(clientName, id, name, spriteAsociado, tileX, tileY) {
		this->life = 100;	
		this->destx = tileX;
		this->desty = tileY;
		this->move = new Mover();
		this->performingAction = false;
		this->spriteAsociado->setMoviendo(false);

		this->is_moving = false;
		this->spriteAsociado->Recollecting = false;
		this->spriteAsociado->Building = false;
		this->spriteAsociado->Attacking = false;

		this->actionsList.clear();

};	

void Player::addAction(EntityStatus newAction){
	this->actionsList.push_back(newAction);
}

bool Player::hasNextAction(){
	if (this->actionsList.size() > 0){
		return true;
	}
	return false;
}

void Player::clearActions(){
	this->actionsList.clear();
}

EntityStatus Player::popAction(){
	EntityStatus action = this->actionsList.front();
	this->actionsList.erase(this->actionsList.begin());
	return action;
}


Player::~Player(){
	this->move = nullptr;
	delete this->move;
}
void Player::setDirection(std::string direction){
	this->getSprite()->direction = direction;
}

void Player::cleanDirection(){
	this->getSprite()->direction = "";
}

void Player::mover(int positionClickPixelsX, int positionClickPixelsY, int entityPositionX, int entityPositionY, int velocity){
	this->move->moveEntity((*this),positionClickPixelsX,positionClickPixelsY,entityPositionX,entityPositionY,velocity);
}
void Player::setMotion(){
	this->move->setMotion();
}

void Player::render(int cameraOffsetX, int cameraOffsetY, float zoom){
	Sprite *s = this->getSprite();
	
	s->setPosX(this->posx);
	s->setPosY(this->posy);

	if (s->EnMovimiento){
		s->RenderDynamicSprite(cameraOffsetX, cameraOffsetY, zoom);
	} else if (s->Recollecting) {
		s->RenderDynamicSpriteRecollecting(cameraOffsetX, cameraOffsetY, zoom, 0);
	} else if (s->Building) {
		s->RenderDynamicSpriteBuilding(cameraOffsetX, cameraOffsetY, zoom, 0);
	} else if (s->Attacking) {
		s->RenderDynamicSpriteAttacking(cameraOffsetX, cameraOffsetY, zoom, 0);
	} else {
		s->RenderDynamicSpriteIdle(cameraOffsetX, cameraOffsetY, zoom, 0);
	}
}

void Player::renderDisconnected(int cameraOffsetX, int cameraOffsetY, float zoom){
	
}


void Player::setSprite(Sprite *sprite){
	EntityBase::setSprite(spriteAsociado);
}

Sprite* Player::getSprite(){
	return EntityBase::getSprite();
}

int Player::getRelativePositionX(int cameraOffsetX, int cameraOffsetY, float zoom){
	return EntityBase::getRelativePositionX(cameraOffsetX, cameraOffsetY, zoom);
}

int Player::getRelativePositionY(int cameraOffsetX, int cameraOffsetY, float zoom){
	return EntityBase::getRelativePositionY(cameraOffsetX, cameraOffsetY, zoom);
}

void Player::addTilesVisited(Tile* aTile){
	auto it = find_if(this->tilesVisited.begin(), this->tilesVisited.end(), 
		[=](Tile* p){
		return p->posx == aTile->posx && p->posy == aTile->posy;
	});

	if(it == this->tilesVisited.end())
	{
		this->tilesVisited.push_back(aTile);
	}
}

void Player::cleanTilesVisited(){
	this->tilesVisited.clear();
}

void Player::updateData(EntityStatus es){
	this->destx = es.posx;
	this->desty = es.posy;
	this->life = es.value;
	EntityBase::life = es.value;

	if(es.state == STATUS::MOVING){
		this->move->setMotion();
		this->is_moving = true;
	}
	else{
		this->is_moving = false;
	}
}
