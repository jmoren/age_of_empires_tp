#ifndef REALPLAYER_H_
#define REALPLAYER_H_

#include <iostream>
#include "../network/Data.h"
#include "../entity/Player.h"
#include <string>
#include <vector>

class RealPlayer{

public:

	//Preguntar si aca puede ir el estado de  GANO/PERDIO
	int status;

	std::string name;
	int gold;
	int wood;
	int stone;
	int food;
	string getInfo(string recurso);
	RealPlayer();
	RealPlayer(int stone, int wood, int gold, int food, int status, std::string playerName);
	void UpdateStatus(int stone, int wood, int gold, int food, int status);
	string rellenarAIzquierda(string valor);
	~RealPlayer();
	/*vector<Edificio*> edificios;
	vector<Player*> unidades;
	Edificio* crearEdificio(string name, Sprite* spriteAsociado, int tileX, int tileY);

	void addEdificio(Edificio* edificio);
	void removeEdificio(Edificio* edificio);
	void addUnidad(Player* unidad);
	void removeUnidad(Player* unidad);*/
//private:
//
	//string name;
	//int contadorCentroCivico;
	//int contadorCuartel;
	/*CentroCivico* crearCentroCivico(string name, Sprite* spriteAsociado, int tileX, int tileY, SDL_Renderer* aRenderer, SDL_Window *aWindow, Config *aConfig);
	Cuartel* crearCuartel(string name, Sprite* spriteAsociado, int tileX, int tileY, SDL_Renderer* aRenderer, SDL_Window *aWindow, Config *aConfig);*/

};

#endif /* REALPLAYER_H_ */