#ifndef CENTROCIVICO_HEADER
#define CENTROCIVICO_HEADER

#include "Edificio.h"
#include "../person/Aldeano.h"
#include <string>
#include "../../parser/config.h"

class CentroCivico : public Edificio{
public:
	CentroCivico(string clientName, int id,string name, Sprite *spriteAsociado, int tileX, int tileY); 

	~CentroCivico();

	void Destruir();

	virtual void atacar(EntityBase* param);
	string getInfo();
	//Aldeano* CrearAldeano(string name, Sprite *spriteAsociado, int tileX, int tileY);

};

#endif