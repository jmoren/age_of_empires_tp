#ifndef CUARTEL_HEADER
#define CUARTEL_HEADER

#include "Edificio.h"
#include "../person/Soldado.h"
#include "../person/Pikeman.h"
#include <string>
#include <stdint.h>

class Cuartel : public Edificio{

public:
	Cuartel(string clientName, int id,string name, Sprite *spriteAsociado, int tileX, int tileY);
	
	~Cuartel();

	void Destruir();
	Sprite *sBuilding;
	Sprite *sBuilded;
	virtual void atacar(EntityBase* param);
	string getInfo();	

};

#endif