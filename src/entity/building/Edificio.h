#ifndef EDIFICIO_HEADER
#define EDIFICIO_HEADER

#include "../Element.h"
#include "../../parser/config.h"
#include <string>


class Edificio : public Element{
public:
	Edificio(string clientName, int id, string name, Sprite* spriteAsociado, int tileX, int tileY);
	~Edificio(); 
};

#endif