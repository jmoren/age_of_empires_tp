#include "Aldeano.h"

Aldeano::Aldeano(string clientName, int id, string name, Sprite *spriteAsociado, int tileX, int tileY): 
Player(clientName, id, name,spriteAsociado,tileX,tileY)
{
	this->is_moving = false;
	this->kind = "Unidad";
};

Aldeano::~Aldeano(){

}

void Aldeano::Gather(){
	this->changeToRecollectingSprite();
}

void Aldeano::Move(){
	this->changeToWalkingSprite();
}

void Aldeano::Build(){
	this->changeToBuildingSprite();
}

void Aldeano::RecolectarRecurso(){

}

void Aldeano::ConstruirEdificio(){
}