#ifndef SOLDADO_HEADER
#define SOLDADO_HEADER

#include "../EntityBase.h"
#include "../Player.h"
#include <string>

class Soldado : public Player{
public:
	Soldado(string clientName, int id, string name, Sprite *spriteAsociado, int tileX, int tileY);
	~Soldado();	
	void Attack();
	void Move();
};

#endif