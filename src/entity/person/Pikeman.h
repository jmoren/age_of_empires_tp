#ifndef PIKEMAN_H_
#define PIKEMAN_H_

#include "../Player.h"
#include <string>

class Pikeman : public Player{
public:
	Pikeman(string clientName, int id, string name, Sprite *spriteAsociado, int tileX, int tileY);
	~Pikeman();	
	void Attack();
	void Move();	
};

#endif