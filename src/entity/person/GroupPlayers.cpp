#include "GroupPlayers.h"

GroupPlayers::GroupPlayers(vector<EntityBase*> entities,string clientName, int id, string name, Sprite *spriteAsociado, int tileX, int tileY): 
Player(clientName, id, name,spriteAsociado,tileX,tileY){
	this->entities = entities;
};

vector<Message> GroupPlayers::action(std::string realPlayerName,EntityBase *e, int posx, int posy, bool groupMoving, bool pressButtonCuartel, bool isValidTerrainForBuilding){
	Message m;
	vector<Message> msjs;

	for(int i=0; i < this->entities.size();i++){
		m = this->entities.at(i)->action(realPlayerName,e, posx, posy, groupMoving, pressButtonCuartel,false,false,false,isValidTerrainForBuilding);
		msjs.push_back(m);
	}
	return msjs;
}

void GroupPlayers::clearActions(){
	for(int i=0; i < this->entities.size();i++)
		((Player *)this->entities.at(i))->actionsList.clear();
}

std::string GroupPlayers::getSpriteName(){
	bool equalNames = true;
	std::string name = this->entities.back()->sprite_name;
	for(int i=0; i < this->entities.size(); i++){
		if(name != this->entities.at(i)->sprite_name)
			equalNames= false;
	};

	if(equalNames)
		return this->entities.back()->sprite_name;
	return "-1";
}

std::string GroupPlayers::getClientName(){
	bool equalNames = true;
	std::string name = this->entities.back()->getClientName();
	for(int i=0; i < this->entities.size(); i++){
		if(name != this->entities.at(i)->getClientName())
			equalNames= false;
	};

	if(equalNames)
		return this->entities.back()->getClientName();
	return "-1";
}


string GroupPlayers::getInfo(){
	
	int contadorAldeano = 0;
	int contadorSoldado = 0;
	int contadorPikeman = 0;
	int contadorRey = 0;

	for(int i=0; i < this->entities.size(); i++){
		string name = this->entities.at(i)->sprite_name;
		if(name == "Aldeano")
			contadorAldeano++;
		if(name == "Soldado")
			contadorSoldado++;
		if(name == "Arquero")
			contadorPikeman++;
		if(name == "Rey")
			contadorRey++;
	}

	if(this){

		string msj = "";
		if(contadorRey > 0)
			msj.append("Rey \n");
		if(contadorAldeano > 0)
			msj.append("Aldeano x " + std::to_string((long long)contadorAldeano) + "\n");
		if(contadorSoldado > 0)
			msj.append("Soldado x "+ std::to_string((long long)contadorSoldado) + "\n");
		if(contadorPikeman > 0)
			msj.append("Lanzero x " + std::to_string((long long)contadorPikeman) + "\n");
			
		msj.append("\n");
		msj.append("\n");
		return msj;
	}
	else
		return "";

}