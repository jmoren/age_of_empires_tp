#ifndef GROUPPLAYERS_HEADER
#define GROUPPLAYERS_HEADER

#include <vector>
#include "../Player.h"
#include "../EntityBase.h"
#include "../../network/Data.h"

#include <string>

class GroupPlayers : public Player{
public:	
	GroupPlayers(vector<EntityBase*> entities,string clientName, int id, string name, Sprite *spriteAsociado, int tileX, int tile);
	~GroupPlayers();

	vector<EntityBase*> entities;
	vector<Message> action(std::string realPlayerName,EntityBase *e, int posx, int posy, bool groupMoving, bool pressButtonCuartel, bool isValidTerrainForBuilding);
	std::string getSpriteName();
	std::string getClientName();
	void clearActions();
	string getInfo();
};

#endif