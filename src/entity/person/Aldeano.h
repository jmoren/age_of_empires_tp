#ifndef ALDEANO_HEADER
#define ALDEANO_HEADER

#include "../Player.h"
#include <string>

class Aldeano : public Player{
public:	
	Aldeano(string clientName, int id, string name, 
		Sprite* spriteAsociado, int tileX, int tileY);

	~Aldeano();
	void Gather();
	void Move();
	void Build();
	void RecolectarRecurso();
	void ConstruirEdificio();

};

#endif