#ifndef REY_HEADER
#define REY_HEADER

#include "../EntityBase.h"
#include "../Player.h"
#include <string>

class Rey : public Player{
public:
	Rey(string clientName, int id, string name, Sprite *spriteAsociado, int tileX, int tileY);
	~Rey();	
	void Attack();
	void Move();
};

#endif