#include "Element.h"

Element::~Element(){

};

void Element::render(int cameraOffsetX, int cameraOffsetY, float zoom){
	Sprite *s = this->getSprite();

	SDL_Texture* texture = this->getSprite()->Textura;
	LTexture* lTexture = this->getSprite()->FluidTexture;

	if (texture != nullptr){
		SDL_SetTextureAlphaMod(texture, blendValue);
	}

	if (lTexture != nullptr){
		lTexture->setAlpha(blendValue);
	}

	if (s->spriteState == Sprite::State::Static){
		s->RenderStaticSprite(cameraOffsetX, cameraOffsetY, zoom, 0, this->posx, this->posy);
	}
	else if (s->spriteState == Sprite::State::Fluid){
		s->RenderFluidSprite(cameraOffsetX, cameraOffsetY, zoom, this->posx, this->posy);
	}
}

void Element::renderWithFilter(int cameraOffsetX, int cameraOffsetY, float zoom, bool success){
	Sprite *s = this->getSprite();

	SDL_Texture* texture = this->getSprite()->Textura;
	LTexture* lTexture = this->getSprite()->FluidTexture;

	if (texture != nullptr){
		SDL_SetTextureAlphaMod(texture, blendValue);
	}

	if (lTexture != nullptr){
		lTexture->setAlpha(blendValue);
	}

	if (s->spriteState == Sprite::State::Static){
		if (success) {
			SDL_SetTextureColorMod(s->Textura, 0, 255, 0);
		} else {
			SDL_SetTextureColorMod(s->Textura, 255, 0, 0);
		}

		s->RenderStaticSprite(cameraOffsetX, cameraOffsetY, zoom, 0, this->posx, this->posy);
	}
	else if (s->spriteState == Sprite::State::Fluid){
		s->RenderFluidSprite(cameraOffsetX, cameraOffsetY, zoom, this->posx, this->posy);
	}
}

void Element::updateData(int posx, int posy, int value){
	this->posx = posx;
	this->posy = posy;
	this->value = value;
}

void Element::setSprite(Sprite *sprite){
	EntityBase::setSprite(spriteAsociado);
}

Sprite* Element::getSprite(){
	return EntityBase::getSprite();
}

int Element::getRelativePositionX(int cameraOffsetX, int cameraOffsetY, float zoom){
	return EntityBase::getRelativePositionX(cameraOffsetX, cameraOffsetY, zoom);
}

int Element::getRelativePositionY(int cameraOffsetX, int cameraOffsetY, float zoom){
	return EntityBase::getRelativePositionY(cameraOffsetX, cameraOffsetY, zoom);
}

string Element::getInfo(){
	
	if(this){
		if(this->kind == "resource"){		
			return "Nombre: " + this->sprite_name + "\n" +
				"Disponible: " + std::to_string((long long)this->value) + "\n" +
				"\n" +
				"\n" +
				"\n";
		}	
		return "";
	}
	else
		return "";

}