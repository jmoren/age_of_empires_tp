#include "RealPlayer.h"

RealPlayer::RealPlayer(){
	this->food = 0;
	this->wood = 0;
	this->stone = 0;
	this->gold = 0;
	this->name = "";
	this->status = PLAYER_STATUS::DISCONNECTED;
}

RealPlayer::RealPlayer(int mystone, int mywood, int mygold, int myfood, int mystatus, std::string playerName){
	this->food = myfood;
	this->wood = mywood;
	this->stone = mystone;
	this->gold = mygold;
	this->name = playerName;
	this->status = mystatus;
}

void RealPlayer::UpdateStatus(int mystone, int mywood, int mygold, int myfood, int mystatus){
	this->food = myfood;
	this->wood = mywood;
	this->stone = mystone;
	this->gold = mygold;
	this->status = mystatus;
}

string RealPlayer::getInfo(string recurso){	
	if(recurso == "Wood")
		return rellenarAIzquierda(std::to_string((long long)this->wood));
	if(recurso == "Food")
		return rellenarAIzquierda(std::to_string((long long)this->food));
	if(recurso == "Gold")
		return rellenarAIzquierda(std::to_string((long long)this->gold));
	if(recurso == "Stone")
		return rellenarAIzquierda(std::to_string((long long)this->stone));
}

string RealPlayer::rellenarAIzquierda(string valor){
	if(valor.length() < 3)
		valor = "00" + valor;
	if(valor.length() < 4)
		valor = "0" + valor;

	return valor;
}

RealPlayer::~RealPlayer(){

}

//RealPlayer::RealPlayer(string aName){
//	this->name = aName;
//	this->contadorCentroCivico = 1;
//	this->contadorCuartel = 0;
//}
//
//RealPlayer::~RealPlayer(){
//}
//
//
//Edificio* RealPlayer::crearEdificio(string name, Sprite* spriteAsociado, int tileX, int tileY, SDL_Renderer* aRenderer, SDL_Window *aWindow, Config *aConfig){
//	Edificio* edificio = nullptr;
//	if (name == "CentroCivico")
//		edificio = this->crearCentroCivico(name + std::to_string((long long)this->contadorCentroCivico), spriteAsociado, tileX, tileY, aRenderer, aWindow, aConfig);
//	if (name == "Cuartel")
//		edificio = this->crearCuartel(name + std::to_string((long long)this->contadorCentroCivico), spriteAsociado, tileX, tileY, aRenderer, aWindow, aConfig);
//	this->addEdificio(edificio);
//	return edificio;
//}
//
//CentroCivico* RealPlayer::crearCentroCivico(string name, Sprite* spriteAsociado, int tileX, int tileY, SDL_Renderer* aRenderer, SDL_Window *aWindow, Config *aConfig){
//	this->contadorCentroCivico++;
//	return new CentroCivico(name, spriteAsociado, tileX, tileY, aRenderer, aWindow, aConfig);
//}
//
//Cuartel* RealPlayer::crearCuartel(string name, Sprite* spriteAsociado, int tileX, int tileY, SDL_Renderer* aRenderer, SDL_Window *aWindow, Config *aConfig){
//	this->contadorCuartel++;
//	return new Cuartel(name, spriteAsociado, tileX, tileY, aRenderer, aWindow, aConfig);
//
//}
//void RealPlayer::addEdificio(Edificio* edificio){
//	this->edificios.push_back(edificio);
//}
//
//void RealPlayer::removeEdificio(Edificio* edificio){
//	for (int i = 0; i < this->edificios.size(); i++){
//		Edificio* e = this->edificios.at(i);
//		if (e->name == edificio->name)
//			this->edificios.erase(this->edificios.begin() + i);
//	}
//}
//
//void RealPlayer::addUnidad(Player* unidad){
//	this->unidades.push_back(unidad);
//}
//
//void RealPlayer::removeUnidad(Player* unidad){
//	for (int i = 0; i < this->unidades.size(); i++){
//		Player* u = this->unidades.at(i);
//		if (u->name == unidad->name)
//			this->unidades.erase(this->unidades.begin() + i);
//	}
//}