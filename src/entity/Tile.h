#ifndef TILE_HEADER
#define TILE_HEADER

#include "EntityBase.h"
#include "Element.h"
#include "Player.h"
class Player;
class Tile : public EntityBase{
public:
	Tile(std::string name, Sprite *spriteAsociado, int tileX, int tileY) :
		EntityBase("",0, name, spriteAsociado, tileX, tileY)
	{
		// Tile constructor
		this->elem = nullptr;
		this->occupied = false;
		this->visited = false;
		this->lit = false;
		this->walkable = true;
	};

	~Tile();

	void render(int cameraOffsetX, int cameraOffsetY, float zoom, string realPlayerName);
	
	// ---- EntityBase ---- //
	void setSprite(Sprite *sprite);
	Sprite* getSprite();

	int getRelativePositionX(int cameraOffsetX, int cameraOffsetY, float zoom);
	int getRelativePositionY(int cameraOffsetX, int cameraOffsetY, float zoom);
	// ----  end  ----- //

	bool is_walkable();

	void linkElement(EntityBase* newEntityBase);
	void unlinkElement();
	void setWalkable();
	void setNotWalkable();
	void setOccupied();
	void setNotOccupied();
	bool selectedValidUnit(string realPlayerName);
	EntityBase* getElement();
	bool visited;
	bool occupied;
	bool walkable;
	bool lit;
private:
	
	EntityBase *elem;
};

#endif
