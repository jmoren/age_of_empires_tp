#ifndef ELEMENT_HEADER
#define ELEMENT_HEADER

#include "EntityBase.h"

class Element : public EntityBase {

public:


	Element(string clientName, int id, string name, Sprite *spriteAsociado, int tileX, int tileY) :
		EntityBase(clientName, id, name, spriteAsociado, tileX, tileY)
	{		
		blendValue = 0;
		// Element constructor
	};

	~Element();

	string clientName;
	string name;
	int value;
	bool walkable;
	bool occupied;
	// ---- EntityBase ---- //
	void setSprite(Sprite *sprite);
	Sprite* getSprite();

	int getRelativePositionX(int cameraOffsetX, int cameraOffsetY, float zoom);
	int getRelativePositionY(int cameraOffsetX, int cameraOffsetY, float zoom);
	// ----  end  ----- //

	void updateData(int posx, int posy, int value);

	void render(int cameraOffsetX, int cameraOffsetY, float zoom);
	void renderWithFilter(int cameraOffsetX, int cameraOffsetY, float zoom, bool success);
	string getInfo();
};

#endif
