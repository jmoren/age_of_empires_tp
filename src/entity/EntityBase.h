#ifndef ENTITYBASE_H_
#define ENTITYBASE_H_

#include <string>
#include <iostream>
#include "../sprite/Sprite.h"
#include "../network/data.h"

using namespace std;

class EntityBase{
	public:
		EntityBase();
		EntityBase(string aClientName, int aId, std::string name, Sprite *spriteAsociado, int tileX, int tileY);
		~EntityBase();
		string clientName;
		int id;
		string kind;
		std::string sprite_name;
		Sprite *spriteAsociado;
		int life;
		Sprite* walkSprite;
		void setWalkingSprite(Sprite* walkingSprite);
		void changeToWalkingSprite();
		Sprite* BuildSprite;
		void setBuildingSprite(Sprite* walkingSprite);
		void changeToBuildingSprite();
		Sprite* RecollectSprite;
		void setRecollectingSprite(Sprite* walkingSprite);
		void changeToRecollectingSprite();
		Sprite* attackSprite;
		void setAttackingSprite(Sprite* walkingSprite);
		void changeToAttackingSprite();
		Message actionInfanteria(std::string realPlayerName, EntityBase *e, int posx, int posy, bool groupMoving);

		int posx;
		int posy;

		bool builded;

		void setSprite(Sprite *sprite);
		Sprite* getSprite();

		int getRelativePositionX(int cameraOffsetX, int cameraOffsetY, float zoom);
		int getRelativePositionY(int cameraOffsetX, int cameraOffsetY, float zoom);
		string getInfo();
		std::string getSprite_name(){
			return sprite_name;
		}
		int getId(){
			return id;
		}
		string getClientName(){
			return clientName;
		}
		int blendValue;

		Message action(std::string realPlayerName,EntityBase *e, int posx, int posy, bool groupMoving, bool pressButtonCuartel, bool pressButtonAldeano, bool pressButtonSoldado, bool pressButtonPikeman, bool isValidTerrainForBuilding);
		Message actionAldeano(std::string realPlayerName,EntityBase *e, int posx, int posy, bool groupMoving, bool pressButtonCuartel, bool isValidTerrainForBuilding);
		Message actionCentroCivico(std::string realPlayerName,int posx, int posy, bool pressButtonAldeano);
		Message actionCuartel(std::string realPlayerName,int posx, int posy, bool pressButtonSoldado,bool pressButtonPikeman);		
		Message actionNull();
		Message actionRey(std::string realPlayerName, EntityBase *e, int posx, int posy, bool groupMoving);
		enum UNIDAD {ALDEANO = 0,SOLDADO = 1,PIKEMAN = 2 };
};

#endif
