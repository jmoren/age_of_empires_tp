#include "EntityBase.h"

EntityBase::EntityBase(){
	this->posx = 0;
	this->posy = 0;
}

EntityBase::EntityBase(string aClientName, int aId, string name, Sprite *spriteAsociado, int tileX, int tileY){
	this->kind = "";
	this->clientName = aClientName;
	this->id = aId;
	this->sprite_name = name;
	this->spriteAsociado = spriteAsociado;
	this->posx = tileX;
	this->posy = tileY;
	this->life = 0;
}

EntityBase::~EntityBase(){

}

void EntityBase::setWalkingSprite(Sprite* walkingSprite){
	this->walkSprite = walkingSprite;
}

void EntityBase::setBuildingSprite(Sprite* buildingSprite){
	this->BuildSprite = buildingSprite;
}

void EntityBase::setRecollectingSprite(Sprite* recollectingSprite){
	this->RecollectSprite = recollectingSprite;
}

void EntityBase::setAttackingSprite(Sprite* attackingSprite){
	this->attackSprite = attackingSprite;
}

void EntityBase::changeToWalkingSprite(){
	this->spriteAsociado = this->walkSprite;
}

void EntityBase::changeToBuildingSprite(){
	this->spriteAsociado = this->BuildSprite;
}

void EntityBase::changeToRecollectingSprite(){
	this->spriteAsociado = this->RecollectSprite;
}

void EntityBase::changeToAttackingSprite(){
	this->spriteAsociado = this->attackSprite;
}

void EntityBase::setSprite(Sprite *sprite){
	this->spriteAsociado = sprite;
}

Sprite* EntityBase::getSprite(){
	return this->spriteAsociado;
}

int EntityBase::getRelativePositionX(int cameraOffsetX, int cameraOffsetY, float zoom){
	int pixelTileX = this->spriteAsociado->getPixelX(cameraOffsetX, cameraOffsetY, zoom, posx, posy);
	return pixelTileX;
}

int EntityBase::getRelativePositionY(int cameraOffsetX, int cameraOffsetY, float zoom){
	int pixelTileY = this->spriteAsociado->getPixelY(cameraOffsetX, cameraOffsetY, zoom, posx, posy);
	return pixelTileY;
}


string EntityBase::getInfo(){
	
	if(this){
		
		if(this->sprite_name == "Arquero"){
			return "Nombre: Lanzero \nVida: " 
			+ std::to_string((long long)this->life) + "\n" +
			"\n" +
			"\n" +
			"\n";
		}
		else{
			 return "Nombre: " + this->sprite_name + "\n" +
			"Vida: " + std::to_string((long long)this->life) + "\n" +
			"\n" +
			"\n" +
			"\n";
		}
	}
	else
		return "";

}

Message EntityBase::action(std::string realPlayerName,EntityBase *e, int posx, int posy, bool groupMoving, bool pressButtonCuartel, bool pressButtonAldeano,bool pressButtonSoldado, bool pressButtonPikeman, bool isValidTerrainForBuilding){
		
	if(this->sprite_name == "Rey")
		return this->actionRey(realPlayerName,e,posx,posy,groupMoving);
	if(this->sprite_name == "Aldeano")	
		return this->actionAldeano(realPlayerName,e,posx,posy,groupMoving,pressButtonCuartel,isValidTerrainForBuilding);
	if (this->sprite_name == "Soldado" || this->sprite_name == "Arquero")
		return this->actionInfanteria(realPlayerName, e, posx, posy, groupMoving);
	if(this->sprite_name == "CentroCivico")	
		return this->actionCentroCivico(realPlayerName,posx,posy,pressButtonAldeano);
	if(this->sprite_name == "Cuartel" && this->life > 50 && this->builded)
		return this->actionCuartel(realPlayerName,posx,posy,pressButtonSoldado,pressButtonPikeman);	
	return this->actionNull();

}

Message EntityBase::actionNull(){
	Message msj;
	string vacio = "";
	strcpy_s(msj.name, vacio.c_str());
	return msj;
}


Message EntityBase::actionRey(std::string realPlayerName, EntityBase *e, int posx, int posy, bool groupMoving){

	Message msj;
	msj.groupMoving = groupMoving;
	strcpy_s(msj.name, realPlayerName.c_str());
	msj.id = this->id;
	msj.type = MSG_TYPE::MOVEMENT;
	msj.posx = posx;
	msj.posy = posy;
	
	return msj;
}

Message EntityBase::actionInfanteria(std::string realPlayerName, EntityBase *e, int posx, int posy, bool groupMoving){

	Message msj;
	msj.groupMoving = groupMoving;

	if (e)
	{
		if(e->kind.compare("Flag") == 0 && e->clientName != realPlayerName){

			strcpy_s(msj.name,realPlayerName.c_str());
			msj.id = this->id;
			msj.type = MSG_TYPE::CAPTURE;	
			msj.posx = posx;
			msj.posy = posy;
		}
		if (e->kind.compare("Edificio") == 0 && e->clientName != realPlayerName){

			strcpy_s(msj.name, realPlayerName.c_str());
			msj.id = this->id;
			msj.type = MSG_TYPE::ATTACK;
			msj.posx = posx;
			msj.posy = posy;
		}
		else if (e->kind.compare("Unidad") == 0 && e->clientName != realPlayerName)
		{
			strcpy_s(msj.name, realPlayerName.c_str());
			msj.id = this->id;
			msj.type = MSG_TYPE::ATTACK;
			msj.posx = posx;
			msj.posy = posy;
		}
	}
	else
	{
		strcpy_s(msj.name, realPlayerName.c_str());
		msj.id = this->id;
		msj.type = MSG_TYPE::MOVEMENT;
		msj.posx = posx;
		msj.posy = posy;
	}

	return msj;
}

Message EntityBase::actionAldeano(std::string realPlayerName,EntityBase *e, int posx, int posy, bool groupMoving, bool pressButtonCuartel, bool isValidTerrainForBuilding){

	Message msj;
	msj.groupMoving = groupMoving;

	if(e)
	{
		if(e->kind.compare("Flag") == 0 && e->clientName != realPlayerName){

			strcpy_s(msj.name,realPlayerName.c_str());
			msj.id = this->id;
			msj.type = MSG_TYPE::CAPTURE;	
			msj.posx = posx;
			msj.posy = posy;
		}
		if(e->kind.compare("resource") == 0){

			strcpy_s(msj.name,realPlayerName.c_str());
			msj.id = this->id;
			msj.type = MSG_TYPE::GATHER;	
			msj.posx = posx;
			msj.posy = posy;
		}
		else if(e->kind.compare("Edificio") == 0 && isValidTerrainForBuilding && e->clientName != realPlayerName){

			strcpy_s(msj.name,realPlayerName.c_str());
			msj.id = this->id;
			msj.type = MSG_TYPE::BUILD;
			msj.posx = posx;
			msj.posy = posy;
		}
		else
		{
			strcpy_s(msj.name,realPlayerName.c_str());
			msj.id = this->id;
			msj.type = MSG_TYPE::MOVEMENT;	
			msj.posx = posx;
			msj.posy = posy;				
		}
	}
	else
	{
		if(pressButtonCuartel && isValidTerrainForBuilding){
			strcpy_s(msj.name,realPlayerName.c_str());
			msj.id = this->id;
			msj.type = MSG_TYPE::BUILD;
			msj.posx = posx;
			msj.posy = posy;
		}
		else{
			strcpy_s(msj.name,realPlayerName.c_str());
			msj.id = this->id;
			msj.type = MSG_TYPE::MOVEMENT;	
			msj.posx = posx;
			msj.posy = posy;				
		}
	}

	return msj;
}

Message EntityBase::actionCentroCivico(std::string realPlayerName,int posx, int posy, bool pressButtonAldeano){

	
	if(pressButtonAldeano)
	{
		Message msj;
		strcpy_s(msj.name,realPlayerName.c_str());
		msj.id = this->id;			
		msj.type = MSG_TYPE::CREATE;	
		msj.posx = posx;
		msj.posy = posy;
		msj.target = UNIDAD::ALDEANO;
		return msj;
	}
	return this->actionNull();

}

Message EntityBase::actionCuartel(std::string realPlayerName,int posx, int posy, bool pressButtonSoldado, bool pressButtonPikeman){

	
	if(pressButtonSoldado|| pressButtonPikeman)
	{
		Message msj;
		strcpy_s(msj.name,realPlayerName.c_str());
		msj.id = this->id;			
		msj.type = MSG_TYPE::CREATE;	
		msj.posx = posx;
		msj.posy = posy;
		if(pressButtonPikeman)
			msj.target = UNIDAD::PIKEMAN;
		if (pressButtonSoldado)
			msj.target = UNIDAD::SOLDADO;
		return msj;

	}
	return this->actionNull();
	
	
}


