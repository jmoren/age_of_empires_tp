#ifndef PLAYER_HEADER
#define PLAYER_HEADER

#include "EntityBase.h"
#include "Element.h"
#include <string>
#include <vector>
#include "../Stage/Headers/Mover.h"
#include "Tile.h"
#include "../network/Data.h"

struct Position{
	int x;
	int y;
};
class Mover;
class Tile;
class Player : public EntityBase{
public:
	Player();
	Player(string clientName, int id, string name, Sprite *spriteAsociado, int tileX, int tileY);
	~Player();
	// ---- EntityBase ---- //
	void setSprite(Sprite *sprite);
	Sprite* getSprite();

	int getRelativePositionX(int cameraOffsetX, int cameraOffsetY, float zoom);
	int getRelativePositionY(int cameraOffsetX, int cameraOffsetY, float zoom);
	void render(int cameraOffsetX, int cameraOffsetY, float zoom);
	void renderDisconnected(int cameraOffsetX, int cameraOffsetY, float zoom);
	
	vector<EntityStatus> actionsList;
	EntityStatus currentAction;
	bool performingAction;
	void addAction(EntityStatus);
	EntityStatus popAction();
	bool hasNextAction();
	void clearActions();

	// ----  end  ----- //

	void setDirection(string direction);
	void cleanDirection();
	void recolectarElemento(Element *elem);
	string getInfo();
	void updateData(EntityStatus es);
	string name;
	int destx;
	int desty;
	int life;
	bool is_moving;

	bool connected;
	bool isSelected;
	Mover *move;
	void mover(int positionClickPixelsX, int positionClickPixelsY, int entityPositionX, int entityPositionY, int velocity);
	void setMotion();
	vector<Tile*> tilesVisited;
	void addTilesVisited(Tile* tile);
	void cleanTilesVisited();
	void updateData(int posx, int posy, int value);
};

#endif
