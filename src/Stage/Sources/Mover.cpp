#include "../Headers/Mover.h"

Mover::Mover(){
	this->movementIsProducing = false;
	this->selectedTileX = 0;
	this->selectedTileY = 0;
	this->direction = "";
	logger = Logger::Instance();
}

Mover::~Mover(){
	
}

void Mover::setMotion(){
	this->movementIsProducing = true;
}

void Mover::stopMotion(){
	this->movementIsProducing = false;
}

void Mover::userClickedTile(int tileX, int tileY){
	this->selectedTileX = tileX;
	this->selectedTileY = tileY;
	this->direction = "";
}

int Mover::getDistance(int X1, int Y1, int X2, int Y2){
	double differenceX = X2 - X1;
	double differenceY = Y2 - Y1;
	return floor(sqrt((differenceX *differenceX) + (differenceY * differenceY)));
}

void Mover::moveEntity(Player &aPlayer, int positionClickPixelsX, int positionClickPixelsY, int entityPositionX, int entityPositionY, int velocity){
	
	if (this->movementIsProducing){
		Player *pPlayer = &aPlayer;
		pPlayer->performingAction = true;
		setDirection(pPlayer);

		int distance = this->getDistance(entityPositionX, entityPositionY, positionClickPixelsX, positionClickPixelsY);
	
		this->dy = positionClickPixelsY - entityPositionY;
		this->dx = positionClickPixelsX - entityPositionX;

		this->stepx = (dx < 0 ? -1 : 1) * velocity;
		this->stepy = (dy < 0 ? -1 : 1) * velocity;

		int adx = abs(dx);
		int ady = abs(dy);

		this->px = 2 * ady- adx;
		this->py = 2 * adx - ady;


		if (distance > (velocity - 1)){
			pPlayer->getSprite()->setMoviendo(true);
			
			if (adx >= ady){
				entityPositionX += stepx;

				if (px < 0){
					px = px + (2 * ady);
				}
				else{
					px = px + 2 * (ady - adx);
					entityPositionY += stepy;
					pPlayer->getSprite()->setPixelY(entityPositionY);
					pPlayer->getSprite()->desplazamientoY += stepy;
					pPlayer->getSprite()->setPosY(pPlayer->posy);
				}

				pPlayer->getSprite()->setPixelX(entityPositionX);
				pPlayer->getSprite()->desplazamientoX += stepx;
				pPlayer->getSprite()->setPosX(pPlayer->posx);

			}
			else{
				entityPositionY += stepy;

				if (py < 0){
					py = py + (2 * adx);
				}
				else{
					py = py + 2 * (adx - ady);
					entityPositionX += stepx;
					pPlayer->getSprite()->setPixelX(entityPositionX);
					pPlayer->getSprite()->desplazamientoX += stepx;
					pPlayer->getSprite()->setPosX(pPlayer->posx);
				}

				pPlayer->getSprite()->setPixelY(entityPositionY);
				pPlayer->getSprite()->desplazamientoY += stepy;
				pPlayer->getSprite()->setPosY(pPlayer->posy);
			}
		}
		else{
			//llego a destino.
			Sprite *s = pPlayer->getSprite();
			s->setPosX(pPlayer->destx);
			s->setPosY(pPlayer->desty);

			pPlayer->posx = pPlayer->destx;
			pPlayer->posy = pPlayer->desty;
			pPlayer->performingAction = false;

			if(!pPlayer->is_moving){
				this->stopMotion();
				
				s->setMoviendo(false);
			}
				s->desplazamientoX = 0;
				s->desplazamientoY = 0;
				//s->direction = "";

				//pPlayer->setDirection("");

				this->direction = "";
				dx = dy = stepx = stepy = px = py = 0;
		}

	}
}

void Mover::setDirection(Player *entityToMove){
	int a = entityToMove->destx; 
	int b = entityToMove->desty;
	
	int x = entityToMove->getSprite()->getPosX();
	int y = entityToMove->getSprite()->getPosY();

	dx = a - x;
	dy = b - y;
	
	if (dx > 0)
		this->direction = dy == 0 ? "SE" : dy > 0 ? "E" : "S";
	if (dx < 0)
		this->direction = dy == 0 ? "NW" : dy > 0 ? "N" : "W";
	if (dx == 0)
		this->direction = dy < 0 ? "SW" : "NE";
	
	entityToMove->setDirection(this->direction);
}