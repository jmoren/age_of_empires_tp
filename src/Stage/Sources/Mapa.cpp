#include "../Headers/Mapa.h"

void Mapa::setCurrentTiles(int posX,int posY, int destX, int destY){

	for(int i=0; i < this->currentTiles.size(); i++){
		current_tile = this->currentTiles.at(i);
		if(current_tile){	
			delete current_tile;
			current_tile = nullptr;
		}
		this->currentTiles.clear();
	}

	Sprite *selected = spriteDictionary->getSpriteFromName("current");

	if(posX <= destX && posY <= destY){
		for(int i=posX; i <= destX;i++){
			for(int j= posY; j <= destY;j++){
					Tile* t = this->getTile(i,j);
					current_tile = new Tile("current", selected, t->posx, t->posy);
					currentTiles.push_back(current_tile);
				}
			}
		}else

	if(posX >= destX && posY <= destY){
		for(int i=posX; i >= destX;i--){
			for(int j= posY; j <= destY;j++){
					Tile* t = this->getTile(i,j);
					current_tile = new Tile("current", selected, t->posx, t->posy);
					currentTiles.push_back(current_tile);
				}
			}
		}else

	if(posX <= destX && posY >= destY){
		for(int i=posX; i <= destX;i++){
			for(int j= posY; j >= destY;j--){
					Tile* t = this->getTile(i,j);
					current_tile = new Tile("current", selected, t->posx, t->posy);
					currentTiles.push_back(current_tile);
				}
			}
		}else

	if(posX >= destX && posY >= destY){
		for(int i=posX; i >= destX;i--){
			for(int j= posY; j >= destY;j--){
					Tile* t = this->getTile(i,j);
					current_tile = new Tile("current", selected, t->posx, t->posy);
					currentTiles.push_back(current_tile);
				}
			}
		}
}

Mapa::Mapa(SDL_Renderer* renderer, SDL_Window *ventana, Config *aConfig)
{
	Renderer = renderer;
	Ventana = ventana;
	config = aConfig;
	
	MaxPixelXTileIndex = 0;
	MinPixelXTileIndex = 0;
	MaxPixelYTileIndex = 0;
	MinPixelYTileIndex = 0;

	sizex = config->config.size_x;
	sizey = config->config.size_y;
	
}

void Mapa::load(){
	this->loadSpriteDictionary();
	this->loadTable();
	this->loadDesign();
	this->loadRealPlayers();
	this->loadPlayers();
}

void Mapa::loadTable(){
	// create table
	Sprite *tile = spriteDictionary->getSpriteFromName("plains");
	Sprite *selected = spriteDictionary->getSpriteFromName("current");

	current_tile = new Tile("current", selected, 0, 0);
	
	int i = 0;
	for (int y = 0; y < sizey; y++) {
		for (int x = 0; x < sizex; x++) {
			Tile *t = new Tile("plains", tile, x, y);
			
			this->tiles.push_back(t);

			if ((x == 0) && (y == 0))
 				MinPixelXTileIndex = i;
 			else if ((x == sizex - 1) && (y == sizey - 1))
		 		MaxPixelXTileIndex = i;
	 		else if ((x == 0) && (y == sizey - 1))
		 		MinPixelYTileIndex = i;
	 		else if ((x == sizex - 1) && (y == 0))
 				MaxPixelYTileIndex = i;
	 		i++;
		}
	}
}


Tile * Mapa::getTile(int x, int y){
	return this->tiles.at(y * this->sizey + x);
}

void Mapa::loadDesign(){
	for (int i = 0; i < config->ground.size(); i++){
		GameElement ge = config->ground.at(i);
		loadTerrain(ge);
	}
	for (int i = 0; i < config->elements.size(); i++){
		GameElement ge = config->elements.at(i);
		loadElement(ge);
	}
	for (int i = 0; i < config->resources.size(); i++){
		GameElement ge = config->resources.at(i);
		loadResource(ge);
	}
}

void Mapa::loadTerrain(GameElement ge){

	int posX = ge.posx;
	int posY = ge.posy;

	Tile *mapTile = this->getTile(posX, posY);
	mapTile->occupied = ge.occupied;
	mapTile->walkable = ge.walkable;

	string entityName = ge.name;

	Sprite *associatedSprite = this->spriteDictionary->getSpriteFromName(entityName);
	if (associatedSprite){
		mapTile->spriteAsociado = associatedSprite;
	}
	else{
		mapTile->spriteAsociado = this->spriteDictionary->getSpriteFromName("tileNotFound");
	}
}

void Mapa::loadElement(GameElement ge){
	Tile *mapTile;

	int posX = ge.posx;
	int posY = ge.posy;

	string entityName = ge.name;
	Sprite *spriteAsociado = this->spriteDictionary->getSpriteFromName(entityName);

	if (spriteAsociado){
		mapTile = this->getTile(posX, posY);
			
		spriteAsociado = spriteAsociado;
		Element *entity = new Element(ge.client,ge.id, entityName, spriteAsociado, posX, posY);
		entity->kind = ge.kind;		
		entity->value = ge.value;		
		entity->occupied = ge.occupied;
		
		// set the entity linked to the tile

		mapTile->linkElement(entity);
		mapTile->walkable = ge.walkable;
		mapTile->occupied = ge.occupied;		
		int sizeOnX = 1;
		int sizeOnY = 1;

		for(int i=0; i < this->config->entities.elements.size(); i++){
			if(this->config->entities.elements.at(i).name == entityName){
				sizeOnX = this->config->entities.elements.at(i).size_x;
				sizeOnY = this->config->entities.elements.at(i).size_y;
			}
		}

		for(int i=0; i < sizeOnX ; i++){
			for(int j=0; j < sizeOnY; j++){
					mapTile = this->getTile(posX + i, posY - j);
					mapTile->linkElement(entity);
					mapTile->walkable = ge.walkable;
					mapTile->occupied = ge.occupied;					
					//cout << mapTile->posx << "," << mapTile->posy << " no caminable" << endl;

				}
			}

		this->elementsOnMapList.insert(make_pair(entity->id,entity));
	}

}

void Mapa::addColorToRealPlayer(string realPlayerName, string color){
	this->realPlayersColors[realPlayerName] = color;
}

string Mapa::getRealPlayerColor(string realPlayerName){
	return this->realPlayersColors[realPlayerName];
}

void Mapa::loadResource(GameElement ge){
	Tile *mapTile;

	int posX = ge.posx;
	int posY = ge.posy;

	string entityName = ge.name;
	Sprite *spriteAsociado = this->spriteDictionary->getSpriteFromName(entityName);

	if (spriteAsociado){

		spriteAsociado = spriteAsociado;
		Element *entity = new Element(ge.client,ge.id,entityName, spriteAsociado, posX, posY);

		entity->kind = ge.kind;		
		entity->value = ge.value;	
		this->linkear(entity);
		this->elementsOnMapList.insert(make_pair(entity->id,entity));
	}
}

void Mapa::loadPlayers(){
	
	EntityBase* entityBase = nullptr;
	for (size_t i = 0; i < config->gameEntities.size(); i++){

		GameEntity entity = config->gameEntities.at(i);
		Tile* mapTile = this->getTile(entity.posx, entity.posy);
		//agrego las entidades al mapa
		if (string(entity.name).compare("Aldeano") == 0){
			entityBase = new Aldeano(entity.client, entity.id, entity.name, getSpriteEntity(entity), entity.posx, entity.posy);	
			entityBase->life = entity.value;
			entityBase->setWalkingSprite(getSpriteEntity(entity));
			entityBase->setRecollectingSprite(getNewSpriteWithAction(entity, "Recolectando"));
			entityBase->setBuildingSprite(getNewSpriteWithAction(entity, "Building"));
			entityBase->setAttackingSprite(getNewSpriteWithAction(entity, "Attacking"));

			entityBase->kind = "Unidad";			
			Player* p = (Player*)entityBase;
			p->addTilesVisited(mapTile);
			this->unitsOnMapList[entity.id] = (Player*)entityBase;
		}
		if (string(entity.name).compare("Rey") == 0){
			entityBase = new Rey(entity.client, entity.id,entity.name, getSpriteEntity(entity), entity.posx, entity.posy);
			entityBase->setWalkingSprite(getSpriteEntity(entity));
			entityBase->life = entity.value;
			entityBase->kind = "Unidad";			
			Player* p = (Player*)entityBase;
			p->addTilesVisited(mapTile);
			this->unitsOnMapList[entity.id] = p;
		}		
		if (this->IsInfantry(string(entity.name))){
			if(string(entity.name).compare("Arquero") == 0)
				entityBase = new Pikeman(entity.client, entity.id,entity.name, getSpriteEntity(entity), entity.posx, entity.posy);
			else
				entityBase = new Soldado(entity.client, entity.id,entity.name, getSpriteEntity(entity), entity.posx, entity.posy);
			entityBase->setWalkingSprite(getSpriteEntity(entity));
			entityBase->life = entity.value;
			entityBase->setAttackingSprite(getNewSpriteWithAction(entity, "Atacando"));
			entityBase->kind = "Unidad";			
			Player* p = (Player*)entityBase;
			p->addTilesVisited(mapTile);
			this->unitsOnMapList[entity.id] = p;
		}	
		if (string(entity.name).compare("CentroCivico") == 0){
			
			entityBase = new CentroCivico(entity.client, entity.id,entity.name,  getSpriteElement(entity), entity.posx, entity.posy);			
			entityBase->kind = "Edificio";
			entityBase->life = entity.value;
			this->edificiosOnMapList[entity.id] = (Edificio*)entityBase;
		}	
		if (string(entity.name).compare("Cuartel") == 0){
			entityBase = new Cuartel(entity.client, entity.id,entity.name,  getSpriteElement(entity), entity.posx, entity.posy);
			entityBase->kind = "Edificio";
			entityBase->life = entity.value;			
			this->edificiosOnMapList[entity.id] = (Edificio*)entityBase;
		}	
	
		if (string(entity.name).compare("Flag") == 0){
			entityBase = new Cuartel(entity.client, entity.id,entity.name,  getSpriteElement(entity), entity.posx, entity.posy);
			entityBase->kind = "Flag";
			entityBase->life = entity.value;			
			this->edificiosOnMapList[entity.id] = (Edificio*)entityBase;
		}	
		// set the entity linked to the tile
		this->linkear(entityBase);
		

	}

}

void Mapa::linkear(EntityBase* entityBase){
	int sizeOnX = 1;
	int sizeOnY = 1;
	Tile* mapTile;
	string sprite_name;
	if (entityBase->kind.compare("resource") != 0){
		sprite_name = entityBase->sprite_name + "-" + this->realPlayersColors[entityBase->clientName];
	}
	else
		sprite_name = entityBase->sprite_name;
	
	for(int i=0; i < this->config->entities.elements.size(); i++){
		if(this->config->entities.elements.at(i).name == sprite_name){
			sizeOnX = this->config->entities.elements.at(i).size_x;
			sizeOnY = this->config->entities.elements.at(i).size_y;
		}
	}

	for(int i=0; i < sizeOnX ; i++){
		for(int j=0; j < sizeOnY; j++){
			mapTile = this->getTile(entityBase->posx + i, entityBase->posy - j);
			mapTile->linkElement(entityBase);
			mapTile->walkable = false;
			mapTile->occupied = true;					


		}
	}

}


void Mapa::deslinkear(EntityBase* entityBase){
	int sizeOnX =1;
	int sizeOnY = 1;
	Tile* mapTile;
	string sprite_name;
	if (entityBase->kind.compare("resource") != 0){
		sprite_name = entityBase->sprite_name + "-" + this->realPlayersColors[entityBase->clientName];
	}
	else
		sprite_name = entityBase->sprite_name;	

	for(int i=0; i < this->config->entities.elements.size(); i++){
		if(this->config->entities.elements.at(i).name == sprite_name){
			sizeOnX = this->config->entities.elements.at(i).size_x;
			sizeOnY = this->config->entities.elements.at(i).size_y;
		}
	}

	for(int i=0; i < sizeOnX ; i++){
		for(int j=0; j < sizeOnY; j++){
			mapTile = this->getTile(entityBase->posx + i, entityBase->posy - j);
			mapTile->unlinkElement();
			mapTile->walkable = true;
			mapTile->occupied = false;	
		}
	}

}
void Mapa::loadRealPlayers(){
	for (size_t i = 0; i < config->realPlayers.size(); i++){

		PlayerStatus p = config->realPlayers.at(i);

		RealPlayer* newPlayer = new RealPlayer(p.stone, p.wood, p.gold, p.food, p.status, p.name);

		if(string(p.name).compare(this->realPlayerName) == 0)
			this->realPlayer = newPlayer;

		this->realPlayersList.insert(make_pair(p.name, newPlayer));

	}

}

// Inicializamos el diccionario de sprites
void Mapa::loadSpriteDictionary(){
	this->spriteDictionary = new SpriteMap();

	int cx = 0;
	int cy = 0;
	// load selected tile
	Sprite *current_sprite = new Sprite("resources/images/iso/selection.png", 0, 0, Renderer, Ventana);
	this->spriteDictionary->AddSprite("current", current_sprite);

	////carga de imagenes no encontradas
	//Sprite *tileNotFound = new Sprite("resources/images/iso/TileNotFound.png", 0, 0, Renderer, Ventana);
	//this->spriteDictionary->AddSprite("tileNotFound", tileNotFound);

	//Sprite *elementNotFound = new Sprite("resources/images/iso/ElementNotFound.png", 0, 0, Renderer, Ventana);
	//this->spriteDictionary->AddSprite("elementNotFound", elementNotFound);

	Sprite *disconnectedPlayer = new Sprite("resources/images/iso/PlayerDisconnected.png", 0, 0, Renderer, Ventana);
	this->spriteDictionary->AddSprite("playerDisconnected", disconnectedPlayer);

	// load ground
	for (int i = 0; i < config->entities.ground.size(); i++){
		GroundSprite g = config->entities.ground.at(i);
		Sprite *ground = new Sprite(g.path, 0, 0, Renderer, Ventana);
		this->spriteDictionary->AddSprite(g.name, ground);
	}

	// load elements
	for (int i = 0; i < config->entities.elements.size(); i++){
		ElementSprite e = config->entities.elements.at(i);
		Sprite *elem = new Sprite(e.path, e.entityType, e.ref_x, e.ref_y, e.width, e.height, 0, Renderer, Ventana);
		elem->fps = e.fps;
		elem->delayFluid = e.delay;
		this->spriteDictionary->AddSprite(e.name, elem);
	}

	// load characters
	for(map<string, CharacterSprite>::const_iterator it = config->entities.characters.begin();it != config->entities.characters.end(); ++it){
		CharacterSprite character = it->second;
		int fps = character.fps;
		int rx = character.ref_x;
		int ry = character.ref_y;
		Sprite *personaje = new Sprite(character.path, character.tipo, cx, cy, rx, ry, 0, Renderer, Ventana);
		personaje->fps = fps;
		this->spriteDictionary->AddSprite(character.name, personaje);
	}
}

CharacterSprite Mapa::searchCharacterWithName(string aName){
	return config->entities.characters[aName];
}

ElementSprite Mapa::searchElementWithName(string aName){
	for (int i = 0; i < config->entities.elements.size(); i++){
		ElementSprite e = config->entities.elements.at(i);
		if(e.name == aName)
			return e;
	}
}

Sprite* Mapa::getNewSpriteWithAction(EntityStatus entity, string action){
	
	string color = this->realPlayersColors[entity.client];
	string entityNameWithColor = string(entity.name) + "-" + color;
	string entityNameWithAction = entityNameWithColor + "-" + action;
	CharacterSprite character = searchCharacterWithName(entityNameWithAction);
	int fps = character.fps;
	int rx = character.ref_x;
	int ry = character.ref_y;
	Sprite *characterSprite = new Sprite(character.path, character.tipo, entity.posx, entity.posy, character.unitWidth, character.unitHeight, character.frames, Renderer, Ventana);
	characterSprite->fps = fps;
	characterSprite->pixelOffsetX = rx;
	characterSprite->pixelOffsetY = ry;

	return characterSprite;
}

Sprite* Mapa::getNewSpriteWithAction(GameEntity entity, string action){

	string color = this->realPlayersColors[entity.client];
	string entityNameWithColor = string(entity.name) + "-" + color;
	string entityNameWithAction = entityNameWithColor + "-" + action;
	CharacterSprite character = searchCharacterWithName(entityNameWithAction);
	int fps = character.fps;
	int rx = character.ref_x;
	int ry = character.ref_y;
	Sprite *characterSprite = new Sprite(character.path, character.tipo, entity.posx, entity.posy, character.unitWidth, character.unitHeight, character.frames, Renderer, Ventana);
	characterSprite->fps = fps;
	characterSprite->pixelOffsetX = rx;
	characterSprite->pixelOffsetY = ry;

	return characterSprite;
}

Sprite* Mapa::getSpriteEntity(GameEntity entity){
	
	string color = this->realPlayersColors[entity.client];
	string entityNameWithColor = string(entity.name) + "-" + color;
	CharacterSprite character = searchCharacterWithName(entityNameWithColor);
	int fps = character.fps;
	int rx = character.ref_x;
	int ry = character.ref_y;
	Sprite *characterSprite = new Sprite(character.path, character.tipo, entity.posx, entity.posy, character.unitWidth, character.unitHeight, character.frames, Renderer, Ventana);
	characterSprite->fps = fps;
	characterSprite->pixelOffsetX = rx;
	characterSprite->pixelOffsetY = ry;
	
	return characterSprite;
}

Sprite* Mapa::getSpriteEntity(EntityStatus entity){

	string color = this->realPlayersColors[entity.client];
	string entityNameWithColor = string(entity.name) + "-" + color;
	CharacterSprite character = searchCharacterWithName(entityNameWithColor);
	int fps = character.fps;
	int rx = character.ref_x;
	int ry = character.ref_y;
	Sprite *characterSprite = new Sprite(character.path, character.tipo, entity.posx, entity.posy, character.unitWidth, character.unitHeight, character.frames, Renderer, Ventana);
	characterSprite->fps = fps;
	characterSprite->pixelOffsetX = rx;
	characterSprite->pixelOffsetY = ry;

	return characterSprite;
}


Sprite* Mapa::getSpriteElement(GameEntity entity){	
	string entityNameWithColor = (string)entity.name + "-" + this->realPlayersColors[entity.client];
	//string entityName = entity.name;
	return getSpriteElement(entityNameWithColor, entity.posx, entity.posy);
}

Sprite* Mapa::getSpriteElement(EntityStatus entity){
	string entityNameWithColor = (string)entity.name + "-" + this->realPlayersColors[entity.client];
	//string entityName = entity.name;
	return getSpriteElement(entityNameWithColor, entity.posx, entity.posy);
}

Sprite* Mapa::getSpriteElement(string entityName, int posX, int posY){
	
	ElementSprite eSprite = searchElementWithName(entityName);
	int fps = eSprite.fps;
	int rx = eSprite.ref_x;
	int ry = eSprite.ref_y;
	Sprite *sp = new Sprite(eSprite.path, eSprite.entityType, rx, ry, eSprite.width, eSprite.height, 0, Renderer, Ventana);
	sp->fps = fps;
	
	return sp;
}


// render content
void Mapa::render(int camaraoffset_x, int camaraoffset_y, float zoom){
	int i = 0;
	int y = 0;
	int x = 0;

	//vemos las acciones de los players.
	this->doPlayersActions(camaraoffset_x, camaraoffset_y, zoom);

	// we can move players
	this->movePlayers(camaraoffset_x, camaraoffset_y, zoom);

	this->renderTiles(camaraoffset_x, camaraoffset_y, zoom);

	// renderizamos el selected tile
	for(int i=0; i < this->currentTiles.size(); i++){
		current_tile = this->currentTiles.at(i);
		current_tile->getSprite()->RenderCurrentTileSprite(camaraoffset_x, camaraoffset_y, zoom, 0, current_tile->posx, current_tile->posy);
	}

	// renderizamos elementos	
	this->renderElements(camaraoffset_x, camaraoffset_y, zoom);

	this->renderPlayers(camaraoffset_x, camaraoffset_y, zoom);

	this->renderStructures(camaraoffset_x, camaraoffset_y, zoom);
}

void Mapa::renderStructures(int camaraoffset_x, int camaraoffset_y, float zoom){
	
	map<int, Edificio*>::iterator it;

	for (it = this->edificiosOnMapList.begin(); it != this->edificiosOnMapList.end(); it++)
	{
		Element *elem = it->second;
		if (elem &&	isInMapRange((EntityBase*)elem, camaraoffset_x, camaraoffset_y, zoom)){
			elem->render(camaraoffset_x, camaraoffset_y, zoom);
		}
	}
	
}

void Mapa::movePlayers(int camaraOffsetX, int camaraOffsetY, float zoom){

	map<int, Player*>::iterator it;

	for (it = this->unitsOnMapList.begin(); it != this->unitsOnMapList.end(); it++)
	{
		if (it->second){
			this->movePlayer(it->second, camaraOffsetX, camaraOffsetY, zoom);
		}
	}
}

void Mapa::doPlayersActions(int camaraOffsetX, int camaraOffsetY, float zoom){
	map<int, Player*>::iterator it;

	for (it = this->unitsOnMapList.begin(); it != this->unitsOnMapList.end(); it++)
	{
		if (it->second){
			this->performUnitAction(it->second, camaraOffsetX, camaraOffsetY, zoom);
		}
	}
}

void Mapa::performUnitAction(Player* aPlayer, int camaraOffsetX, int camaraOffsetY, int zoom){
	if (!aPlayer->performingAction && aPlayer->hasNextAction()){
		//tomo la siguiente accion a ejecutar, si hay una.
		EntityStatus newActionToPerform = aPlayer->popAction();
		aPlayer->currentAction = newActionToPerform;

		if (newActionToPerform.state == STATUS::MOVING){
			aPlayer->changeToWalkingSprite();
			aPlayer->spriteAsociado->Recollecting = false;
			aPlayer->spriteAsociado->Building = false;
			aPlayer->spriteAsociado->Attacking = false;
			Tile* t = this->getTile(aPlayer->posx, aPlayer->posy);
			Tile* tvisited = this->getTile(newActionToPerform.posx, newActionToPerform.posy);
			t->unlinkElement();
			t->lit = false;
			t->occupied = false;
			t->walkable = true;
			tvisited->linkElement(aPlayer);
			tvisited->lit = true;
			tvisited->occupied = true;
			tvisited->walkable = false;

			if (newActionToPerform.client == this->realPlayerName){
				this->renderTorch(newActionToPerform.posx, newActionToPerform.posy, 0, 0, 0);
				aPlayer->addTilesVisited(tvisited);
			}
		}
		else if (newActionToPerform.state == STATUS::RECOLLECTING){
			aPlayer->changeToRecollectingSprite();
			aPlayer->spriteAsociado->Recollecting = true;
			aPlayer->spriteAsociado->Building = false;
			aPlayer->spriteAsociado->Attacking = false;
		}
		else if (newActionToPerform.state == STATUS::BUILDING){
			aPlayer->changeToBuildingSprite();
			aPlayer->spriteAsociado->Building = true;
			aPlayer->spriteAsociado->Recollecting = false;
			aPlayer->spriteAsociado->Attacking = false;
		}
		else if (newActionToPerform.state == STATUS::ATTACKING){
			aPlayer->changeToAttackingSprite();
			aPlayer->spriteAsociado->Attacking = true;
			aPlayer->spriteAsociado->Recollecting = false;
			aPlayer->spriteAsociado->Building = false;
		}
		else if (newActionToPerform.state == STATUS::IDLE){
			aPlayer->changeToWalkingSprite();
			aPlayer->spriteAsociado->setMoviendo(false);
			aPlayer->is_moving = false;
			aPlayer->spriteAsociado->Recollecting = false;
			aPlayer->spriteAsociado->Building = false;
			aPlayer->spriteAsociado->Attacking = false;

			Tile* t = this->getTile(newActionToPerform.posx, newActionToPerform.posy);
			t->linkElement(aPlayer);
			t->occupied = true;
			t->walkable = false;
		}

		//actualizo sus datos.
		aPlayer->updateData(newActionToPerform);
		
	}
}

void Mapa::renderPlayers(int camaraOffsetX, int camaraOffsetY, float zoom){
	
	map<int, Player*>::iterator it;

	for (it = this->unitsOnMapList.begin(); it != this->unitsOnMapList.end(); it++)
	{
		if (it->second){
			if (it->second->life != 0){
				it->second->render(camaraOffsetX, camaraOffsetY, zoom);
			}
		}
	}
}

void Mapa::drawAnjacentOf(int posXVisited, int posYVisited, int camaraoffset_x, int camaraoffset_y, float zoom){
	if(this->valid_pos(posXVisited,posYVisited)){
		this->getTile(posXVisited,posYVisited)->visited = true;
		SDL_SetRenderDrawColor(Renderer,204,255,204,255);

		SDL_Rect terreno5 = { camaraoffset_x + ( posXVisited +  posYVisited) * zoom, camaraoffset_y + ( posXVisited -  posYVisited) * zoom, ceil(2 * zoom), ceil(zoom) };
		SDL_RenderFillRect(Renderer, &terreno5);
	}

}

void Mapa::drawAdjacents(int posXVisited, int posYVisited, int camaraoffset_x, int camaraoffset_y, float zoom){
	this->drawAnjacentOf(posXVisited + 1, posYVisited,camaraoffset_x, camaraoffset_y, zoom);
	this->drawAnjacentOf(posXVisited + 1, posYVisited + 1,camaraoffset_x, camaraoffset_y, zoom);
	this->drawAnjacentOf(posXVisited, posYVisited + 1,camaraoffset_x, camaraoffset_y, zoom);
	this->drawAnjacentOf(posXVisited - 1, posYVisited + 1,camaraoffset_x, camaraoffset_y, zoom);
	this->drawAnjacentOf(posXVisited - 1, posYVisited,camaraoffset_x, camaraoffset_y, zoom);
	this->drawAnjacentOf(posXVisited - 1, posYVisited - 1,camaraoffset_x, camaraoffset_y, zoom);
	this->drawAnjacentOf(posXVisited, posYVisited - 1,camaraoffset_x, camaraoffset_y, zoom);
	this->drawAnjacentOf(posXVisited + 1, posYVisited - 1,camaraoffset_x, camaraoffset_y, zoom);
}

void Mapa::renderRect(int offset_x,int offset_y, int camaraoffset_x, int camaraoffset_y, float zoom){
	//if(this->valid_pos(offset_x,offset_y)){
		SDL_SetRenderDrawColor(Renderer,255,255,0,255);
		SDL_Rect terreno5 = {camaraoffset_x + (offset_x + offset_y) * zoom, camaraoffset_y + (offset_x - offset_y) * zoom, ceil(2 * zoom), ceil(zoom) };
		SDL_RenderFillRect(Renderer, &terreno5);
	//}
}


void Mapa::renderMiniatura(int camaraoffset_x, int camaraoffset_y, float zoom, float anoffset_x, float anoffset_y){
	int x, y;
	SDL_GetMouseState(&x, &y);
	int offset_x = this->getIsoX(x, y, anoffset_x, anoffset_y, 1.0);
	int offset_y = this->getIsoY(x, y, anoffset_x, anoffset_y, 1.0);
	
	this->renderRect(offset_x + 2,offset_y, camaraoffset_x, camaraoffset_y, zoom);
	this->renderRect(offset_x - 2,offset_y, camaraoffset_x, camaraoffset_y, zoom);
	this->renderRect(offset_x,offset_y + 2, camaraoffset_x, camaraoffset_y, zoom);
	this->renderRect(offset_x,offset_y - 2, camaraoffset_x, camaraoffset_y, zoom);

	for(int i = 0; i < this->config->ground.size(); i++){
		GameElement elem = this->config->ground.at(i);
		if(this->getTile(elem.posx,elem.posy)->visited){
			if(string(elem.name) == "water")
				SDL_SetRenderDrawColor(Renderer,0,0,255,255);	
			if(string(elem.name) =="sand")
				SDL_SetRenderDrawColor(Renderer,0,255,0,255);

			SDL_Rect terreno = {camaraoffset_x + (elem.posx + elem.posy) * zoom, camaraoffset_y + (elem.posx - elem.posy) * zoom, ceil(2*zoom), ceil(zoom) };
			SDL_RenderFillRect(Renderer, &terreno);
		}
	}

	map<int, Element*>::iterator itElem;

	for (itElem = this->elementsOnMapList.begin(); itElem != this->elementsOnMapList.end(); itElem++)
	{		
		Element* elem = itElem->second;
		if(elem && this->getTile(elem->posx,elem->posy)->visited){
			
			if(elem->sprite_name.compare("forest") == 0)
				SDL_SetRenderDrawColor(Renderer,0,255,0,255);
			if (elem->sprite_name.compare("tree") == 0)
				SDL_SetRenderDrawColor(Renderer,0,255,0,255);
			if (elem->sprite_name.compare("gold") == 0)
				SDL_SetRenderDrawColor(Renderer,255,255,0,255);
			if (elem->sprite_name.compare("stone") == 0)
				SDL_SetRenderDrawColor(Renderer,255,255,255,255);
			if (elem->sprite_name.compare("wood") == 0)
				SDL_SetRenderDrawColor(Renderer,216,148,31,255);
			if (elem->sprite_name.compare("food") == 0)
				SDL_SetRenderDrawColor(Renderer,255,128,0,255);

		}
		else
			SDL_SetRenderDrawColor(Renderer,0,0,0,255);

		SDL_Rect terreno = { camaraoffset_x + (elem->posx + elem->posy) * zoom, camaraoffset_y + (elem->posx - elem->posy) * zoom, ceil(2*zoom), ceil(zoom) };
		SDL_RenderFillRect(Renderer, &terreno);

	}

	map<int, Player*>::iterator it;

	for (it = this->unitsOnMapList.begin(); it != this->unitsOnMapList.end(); it++)
	{		
		EntityBase* e = (EntityBase*)it->second;
		if (it->second && e->clientName == this->realPlayerName){
			
			for(int i=0; i < it->second->tilesVisited.size(); i++){
				int posXVisited = it->second->tilesVisited.at(i)->posx;
				int posYVisited = it->second->tilesVisited.at(i)->posy;
					SDL_SetRenderDrawColor(Renderer,204,255,204,2500);

					SDL_Rect terreno5 = { camaraoffset_x + ( posXVisited +  posYVisited) * zoom, camaraoffset_y + ( posXVisited -  posYVisited) * zoom, ceil(2* zoom), ceil(zoom) };
					SDL_RenderFillRect(Renderer, &terreno5);
					this->drawAdjacents(posXVisited, posYVisited, camaraoffset_x, camaraoffset_y, zoom);
				}

				SDL_SetRenderDrawColor(Renderer,127,0,255,255);
			SDL_Rect terreno = { camaraoffset_x + ( it->second->destx +  it->second->desty) * zoom, camaraoffset_y + ( it->second->destx -  it->second->desty) * zoom, ceil(2*zoom), ceil(zoom) };
				SDL_RenderFillRect(Renderer, &terreno);

			
		}
		else if(it->second && e->clientName != this->realPlayerName){
				SDL_SetRenderDrawColor(Renderer,255,0,0,255);
			SDL_Rect terreno = { camaraoffset_x + ( it->second->destx +  it->second->desty) * zoom, camaraoffset_y + ( it->second->destx -  it->second->desty) * zoom, ceil(2*zoom), ceil(zoom) };
				SDL_RenderFillRect(Renderer, &terreno);
			}

	}

	map<int, Edificio*>::iterator itEdificio;
	for (itEdificio = this->edificiosOnMapList.begin(); itEdificio != this->edificiosOnMapList.end(); itEdificio++)
	{		
		Element* e = (Element*)itEdificio->second;
		if (itEdificio->second && e->clientName == this->realPlayerName){
			
			SDL_SetRenderDrawColor(Renderer,127,0,255,255);
			SDL_Rect terreno = { camaraoffset_x + ( itEdificio->second->posx +  itEdificio->second->posy) * zoom, camaraoffset_y + ( itEdificio->second->posx -  itEdificio->second->posy) * zoom, ceil(2*zoom), ceil(zoom) };
			SDL_RenderFillRect(Renderer, &terreno);

			
		}
		else if(itEdificio->second && e->clientName != this->realPlayerName){
			SDL_SetRenderDrawColor(Renderer,255,0,0,255);
			SDL_Rect terreno = { camaraoffset_x + ( itEdificio->second->posx +  itEdificio->second->posy) * zoom, camaraoffset_y + ( itEdificio->second->posx -  itEdificio->second->posy) * zoom, ceil(2*zoom), ceil(zoom) };
			SDL_RenderFillRect(Renderer, &terreno);
		}

	}
	
	SDL_SetRenderDrawColor(Renderer,0,0,0,255);

}

void Mapa::renderElements(int camaraoffset_x, int camaraoffset_y, float zoom){
	map<int, Element*>::iterator itElem;

	for (itElem = this->elementsOnMapList.begin(); itElem != this->elementsOnMapList.end(); itElem++)
	{		
		Element* elem = itElem->second;
		if (isInMapRange((EntityBase*) elem, camaraoffset_x, camaraoffset_y, zoom)){
			elem->render(camaraoffset_x, camaraoffset_y, zoom);
		}
	}
}

void Mapa::renderTiles(int camaraoffset_x, int camaraoffset_y, float zoom){
	int minRenderTileX = this->getIsoCoordinateX(0, 0, camaraoffset_x, camaraoffset_y, zoom);
	int maxRenderTileX = this->getIsoCoordinateX(this->screenWidth, this->screenHeight, camaraoffset_x, camaraoffset_y, zoom);
	int minRenderTileY = this->getIsoCoordinateY(0, this->screenHeight, camaraoffset_x, camaraoffset_y, zoom);
	int maxRenderTileY = this->getIsoCoordinateY(this->screenWidth, 0, camaraoffset_x, camaraoffset_y, zoom);

	if (minRenderTileX < 0)
		minRenderTileX = 0;
	if (minRenderTileY < 0)
		minRenderTileY = 0;
	if (maxRenderTileX >= config->config.size_x)
		maxRenderTileX = config->config.size_x - 1;
	if (maxRenderTileY >= config->config.size_y)
		maxRenderTileY = config->config.size_y - 1;

	for (int i = minRenderTileX; i <= maxRenderTileX; i++)
	{
		for (int j = minRenderTileY; j <= maxRenderTileY; j++){
			Tile *tile = this->getTile(i, j);
			if (tile->selectedValidUnit(this->realPlayerName)){
				this->renderTorch(tile->posx, tile->posy, camaraoffset_x, camaraoffset_y, zoom);
			}
			/*if (tile->posx < (this->player->posx + 3) && tile->posy < (this->player->posy + 3)
				&& tile->posx >(this->player->posx - 3) && tile->posy >(this->player->posy - 3)){
				tile->visited = true;
			}*/
			tile->render(camaraoffset_x, camaraoffset_y, zoom, this->realPlayerName);
		}
	}
}

void Mapa::renderTorchBase(int posx, int posy, int sizeX, int sizeY, int camaraoffset_x, int camaraoffset_y, float zoom){
	int minRenderTileX = 0;
	int maxRenderTileX = config->config.size_x - 1;
	int minRenderTileY = 0;
	int maxRenderTileY = config->config.size_y - 1;

	for (int i = posx; i <= sizeX; i++)
	{
		for (int j = posy; j <= sizeY; j++)
		{
			this->getTile(i, j)->visited = true;
		}
		
	}
}

void Mapa::renderTorch(int posx, int posy, int camaraoffset_x, int camaraoffset_y, float zoom){
	int minRenderTileX = 0;
	int maxRenderTileX = config->config.size_x - 1;
	int minRenderTileY = 0;
	int maxRenderTileY = config->config.size_y - 1;

	this->getTile(posx, posy)->visited = true;
	this->getTile(posx, posy)->lit = true;

	for (int j = 0; j <= 4; j++)
	{
		for (int i = (posx - (4 - j)); i <= (posx + (4 - j)); i++)
		{
			if (this->tileExist(i, posy + j)){
				this->getTile(i, posy + j)->visited = true;
				this->getTile(i, posy + j)->lit = true;
			}

			if (this->tileExist(i, posy - j)){
				this->getTile(i, posy - j)->visited = true;
				this->getTile(i, posy - j)->lit = true;
			}
		}
	}

	//renderizo torch con radio I
	/*for (int i = 1; i < 2; i++)
	{
		if ((posx - i) >= 0){
			this->getTile(posx - i, posy)->visited = true;
			this->getTile(posx - i, posy)->lit = true;
			if (posy - i >= 0){
				this->getTile(posx - i, posy - i)->visited = true;
				this->getTile(posx, posy - i)->visited = true;
				this->getTile(posx - i, posy - i)->lit = true;
				this->getTile(posx, posy - i)->lit = true;
			}
			if ((posx + i) <= config->config.size_x - 1){
				this->getTile(posx + i, posy)->visited = true;
				this->getTile(posx + i, posy)->lit = true;
				if ((posy + i) <= config->config.size_x - 1){
					this->getTile(posx + i, posy + i)->visited = true;
					this->getTile(posx, posy + i)->visited = true;
					this->getTile(posx + i, posy + i)->lit = true;
					this->getTile(posx, posy + i)->lit = true;
				}
			}
		}

		if ((posx + i) <= config->config.size_x - 1){
			if (posy - i >= 0){
				this->getTile(posx + i, posy - i)->visited = true;
				this->getTile(posx + i, posy - i)->lit = true;
			}
		}
		
		if ((posx - i) >= 0){
			if ((posy + i) <= config->config.size_x - 1){
				this->getTile(posx - i, posy + i)->visited = true;
				this->getTile(posx - i, posy + i)->lit = true;
			}
		}

	}*/

	//renderizo bordes
	//this->getTile(posx - 2, posy + 2)->visited = true;
	//this->getTile(posx - 2, posy + 2)->lit = true;
	//this->getTile(posx + 2, posy - 2)->visited = true;
	//this->getTile(posx + 2, posy - 2)->lit = true;
}

bool Mapa::tileExist(int tilePosX, int tilePosY){
	if (tilePosX >= 0 && tilePosX <= (this->sizex - 1)){
		if (tilePosY >= 0 && tilePosY <= (this->sizey - 1)){
			return true;
		}
	}
	return false;
}

bool Mapa::isInMapRange(EntityBase* currentEntity, int camaraoffset_x, int camaraoffset_y, float zoom){
	int pixelX = this->getPixelX(currentEntity->posx, currentEntity->posy, camaraoffset_x, camaraoffset_y, zoom);
	int pixelY = this->getPixelY(currentEntity->posx, currentEntity->posy, camaraoffset_x, camaraoffset_y, zoom);

	return (pixelX < (this->screenWidth + 3 * TAMANIO_TILE) && pixelX >(0 - 3 * TAMANIO_TILE)
		&& pixelY < (this->screenHeight + 3 * TAMANIO_TILE) && pixelY >(0 - 3 * TAMANIO_TILE));
}

void Mapa::movePlayer(Player *aPlayer, int camaraoffset_x, int camaraoffset_y, float zoom){

	int positionClickPixelsX = getPixelX(aPlayer->destx, aPlayer->desty, camaraoffset_x, camaraoffset_y, zoom);
	int positionClickPixelsY = getPixelY(aPlayer->destx, aPlayer->desty, camaraoffset_x, camaraoffset_y, zoom);
	
	int entityPositionX = aPlayer->getSprite()->getPixelX(camaraoffset_x, camaraoffset_y, zoom);
	int entityPositionY = aPlayer->getSprite()->getPixelY(camaraoffset_x, camaraoffset_y, zoom);
	int velocity = this->config->config.velocity;
	
	aPlayer->mover(positionClickPixelsX, positionClickPixelsY, entityPositionX, entityPositionY, velocity);

}

// Get data from map
int Mapa::getMaxPixelX(float zoom){
	return this->tiles[MaxPixelXTileIndex]->spriteAsociado->getPixelX(0, 0, zoom, sizex-1,sizey-1);
}

int Mapa::getMinPixelX(float zoom)
{
	return this->tiles[MinPixelXTileIndex]->spriteAsociado->getPixelX(0, 0, zoom,0,0);
}

int Mapa::getMaxPixelY(float zoom)
{
	return this->tiles[MaxPixelYTileIndex]->spriteAsociado->getPixelY(0, 0, zoom, sizex-1,0);
}

int Mapa::getMinPixelY(float zoom)
{
	return this->tiles[MinPixelYTileIndex]->spriteAsociado->getPixelY(0, 0, zoom,0,sizey-1);
}

int Mapa::getIsoX(float x, float y, int cameraoffset_x, int cameraoffset_y, float zoom)
{
	int pos_x = (x + cameraoffset_x + 2.0*(y + cameraoffset_y)) / (TAMANIO_TILE*zoom) - 0.9;
	return pos_x;
}

int Mapa::getIsoY(float x, float y, int cameraoffset_x, int cameraoffset_y, float zoom)
{
	int pos_y = (x + cameraoffset_x - 2.0*(y + cameraoffset_y)) / (TAMANIO_TILE*zoom) + 0.9;
	return pos_y;
}

int Mapa::getIsoCoordinateX(float x, float y, int cameraoffset_x, int cameraoffset_y, float zoom)
{
	int pos_x = getIsoX(x, y, cameraoffset_x, cameraoffset_y, zoom);
	return pos_x;
}

int Mapa::getIsoCoordinateY(float x, float y, int cameraoffset_x, int cameraoffset_y, float zoom)
{
	int pos_y = getIsoY(x, y, cameraoffset_x, cameraoffset_y, zoom);
	return pos_y;
}

int Mapa::getPixelX(float x, float y, int cameraoffset_x, int cameraoffset_y, float zoom)
{
	int pixel_x = 0;
	pixel_x = (TAMANIO_TILE*zoom * x * 0.5) + (TAMANIO_TILE*zoom * y * 0.5) - cameraoffset_x;

	return pixel_x;
}

int Mapa::getPixelY(float x, float y, int cameraoffset_x, int cameraoffset_y, float zoom){
	int pixel_y = 0;
	pixel_y = ((TAMANIO_TILE*zoom * x * 0.25) - (TAMANIO_TILE*zoom * y * 0.25)) - cameraoffset_y;

	return pixel_y;
}

Mapa::~Mapa(){
	/*if (mover)
		delete mover;*/
	for(int i=0; i < this->currentTiles.size(); i++){
		current_tile = this->currentTiles.at(i);
		if(current_tile){	
			delete current_tile;
			current_tile = nullptr;
		}
	}

	for(int i=0; i < this->tiles.size(); i++){
		delete this->tiles[i];
		this->tiles[i] = nullptr;
	}

	map<int, Element*>::iterator itElem;
	for (itElem = this->elementsOnMapList.begin(); itElem != this->elementsOnMapList.end(); itElem++)
	{		
		delete itElem->second;
		itElem->second = nullptr;
	}
	
	if(realPlayer){
		delete realPlayer;
		realPlayer = nullptr;
	}

	if(spriteDictionary){
		delete spriteDictionary;
		spriteDictionary = nullptr;
	}	
}

bool Mapa::valid_pos(int x, int y){
	if(x<0 || y < 0) return false;
	if(x >= this->sizex || y >= this->sizey) return false;
	
	Tile *t = this->tiles.at(((y * this->sizey) + x));
	return this->isPositionValid(x,y);
}

bool Mapa::isPositionValid(int x, int y){
	return (((x >= 0) && (x <= (sizex - 1))) && ((y >= 0) && (y <= (sizey - 1))));
}


void Mapa::processMessage(GameUpdate message){

	this->processElementStatus(message);
	this->processPlayerStatus(message);
	this->processEntityStatus(message);
}

bool Mapa::evaluateBuildingArea(int mousePosX, int mousePosY, int sizeX, int sizeY){
	
	bool validTerrain = true;

	if ((mousePosX + sizeX) > (this->sizex - 1)){
		return false;
	}
	else if ((mousePosY + sizeY) > (this->sizey - 1)){
		return false;
	}
	else if ((mousePosX < 0) || (mousePosY < 0)){
		return false;
	}
	else if ((mousePosY - sizeY) < 0){
		return false;
	}

	for (int i = mousePosX; i < (mousePosX + sizeX); i++)
	{
		for (int j = mousePosY; j > (mousePosY - sizeY); j--)
		{
			if (this->getTile(i, j)->occupied){
				validTerrain = false;
				return validTerrain;
			}
		}
	}

	return validTerrain;
}

bool Mapa::IsUnit(string entityName){	
	return entityName.compare("Aldeano") == 0 || this->IsInfantry(entityName) || entityName.compare("Rey") == 0;
}

bool Mapa::IsBuilding(string entityName){	
	return entityName.compare("Cuartel") == 0 || entityName.compare("CentroCivico") == 0;
}

bool Mapa::IsInfantry(string entityName){	
	return entityName.compare("Soldado")==0 ||entityName.compare("Arquero") == 0;
}


void Mapa::processEntityStatus(GameUpdate m){
	Aldeano *a;
	Player *p;
	Cuartel * c;
	Soldado *s;
	Pikeman *pikeman;
	Rey *r;
	EntityBase *en = nullptr;
	EntityStatus entity;
	Tile* mapTile;

	for (int i = 0; i < m.entities.size(); i++){
		entity = m.entities[i];
		mapTile = this->getTile(entity.posx, entity.posy);
		
		if (this->IsUnit(string(entity.name)) || this->IsInfantry(string(entity.name))){
			en = (EntityBase*)unitsOnMapList[entity.id];
		}

		if (this->IsBuilding(string(entity.name))){
			en = (EntityBase*)edificiosOnMapList[entity.id];
		}

		if (en){ 
			if (entity.deleted){
				if (string(entity.name).compare("Cuartel") != 0)
					this->deleteEntityWithId(entity.id);
			}
			else{
				if ((string(entity.name).compare("Aldeano") == 0) || this->IsInfantry(string(entity.name)) || string(entity.name) == "Rey"){
					p = (Player*)en;
					p->life = entity.value;
					p->addAction(entity);
				}
				if (string(entity.name).compare("CentroCivico") == 0){
					en->life = entity.value;
					
				}
				if (string(entity.name).compare("Cuartel") == 0){
					en->life = entity.value;
					
					if (en->life >= 290 && !((Edificio*)en)->builded){
						en->life = 300;
						en->setSprite(((Cuartel*)en)->sBuilded);
						((EntityBase*)en)->builded = true;
					}
					if (en->life > 20 && ((EntityBase*)en)->builded){
						en->setSprite(((Cuartel*)en)->sBuilded);
					}
					if (en->life < 20 && ((EntityBase*)en)->builded){
						en->setSprite(((Cuartel*)en)->sBuilding);
					}
				}
			}
		}
		else
		{
			if (string(entity.name).compare("Rey") == 0){
				r = new Rey(entity.client, entity.id, entity.name, getSpriteEntity(entity), entity.posx, entity.posy);
				((EntityBase*)r)->life = entity.value;

				r->setWalkingSprite(getSpriteEntity(entity));			
				r->kind = "Unidad";				

				((Player*)r)->addTilesVisited(mapTile);
				this->unitsOnMapList[entity.id] = (Player*)r;
				this->linkear((EntityBase*)r);
			}

			if (string(entity.name).compare("Aldeano") == 0){
				a = new Aldeano(entity.client, entity.id, entity.name, getSpriteEntity(entity), entity.posx, entity.posy);
				((EntityBase*)a)->life = entity.value;

				a->setWalkingSprite(getSpriteEntity(entity));
				a->setRecollectingSprite(getNewSpriteWithAction(entity, "Recolectando"));
				a->setBuildingSprite(getNewSpriteWithAction(entity, "Building"));
				a->setAttackingSprite(getNewSpriteWithAction(entity, "Attacking"));

				a->kind = "Unidad";				

				((Player*)a)->addTilesVisited(mapTile);
				this->unitsOnMapList[entity.id] = (Player*)a;
				this->linkear((EntityBase*)a);
			}

			if (string(entity.name).compare("Soldado") == 0){
				s = new Soldado(entity.client, entity.id, entity.name, getSpriteEntity(entity), entity.posx, entity.posy);
				((EntityBase*)s)->life = entity.value;

				s->setWalkingSprite(getSpriteEntity(entity));
				s->setAttackingSprite(getNewSpriteWithAction(entity, "attacking"));
				s->kind = "Unidad";			
				
				((Player*)s)->addTilesVisited(mapTile);
				this->unitsOnMapList[entity.id] = (Player*)s;
				this->linkear((EntityBase*)s);
			}

			if (string(entity.name).compare("Arquero") == 0){
				pikeman = new Pikeman(entity.client, entity.id, entity.name, getSpriteEntity(entity), entity.posx, entity.posy);
				((EntityBase*)pikeman)->life = entity.value;

				pikeman->setWalkingSprite(getSpriteEntity(entity));
				pikeman->setAttackingSprite(getNewSpriteWithAction(entity, "attacking"));
				pikeman->kind = "Unidad";
				//this->soldiersOnMapList[entity.id] =pikeman;
				
				((Player*)pikeman)->addTilesVisited(mapTile);
				this->unitsOnMapList[entity.id] = (Player*)pikeman;
				this->linkear((EntityBase*)pikeman);
			}

			if (string(entity.name).compare("Cuartel") == 0){
				string cuartel = "Cuartel-" + this->realPlayersColors[entity.client];
				Sprite *sBuilding = getSpriteElement("construccion", entity.posx, entity.posy);
				Sprite *sBuilded  = getSpriteElement(cuartel, entity.posx, entity.posy);

				c = new Cuartel(entity.client, entity.id, entity.name, sBuilding, entity.posx, entity.posy);
				c->builded = false;
				c->sBuilding = sBuilding;
				c->sBuilded  = sBuilded;
				c->kind = "Edificio";
				((EntityBase*)c)->life = entity.value;
				
				this->edificiosOnMapList[entity.id] = c;
				this->linkear((EntityBase*)c);
			}
		}
	}
}

void Mapa::deleteEntityWithId(int id){
	std::map<int, Player*>::iterator it;
	it = this->unitsOnMapList.find(id);
	this->deslinkear((EntityBase*)it->second);
	this->unitsOnMapList.erase(it);	
}


void Mapa::processElementStatus(GameUpdate m){
	for (int i = 0; i < m.elements.size(); i++){
		ElementStatus e = m.elements[i];

		map<int, Element*>::iterator itElem;
		map<int, Element*> mapAux = this->elementsOnMapList;

		for (itElem = mapAux.begin(); itElem != mapAux.end(); itElem++)
		{	
			Element* currentElement = itElem->second;

			if (currentElement->id == e.id){
				//chequeo si hay que borrar el element
				if (e.deleted || e.value == 0)
					deleteElementWithId(e.id);
				else
					//actualizo el element con la data que viene.
					currentElement->value = e.value;

			}
		}
	}
}

void Mapa::deleteElementWithId(int id){
	std::map<int, Element*>::iterator it;
	it = this->elementsOnMapList.find(id);	
	this->deslinkear((EntityBase*)it->second);
	this->elementsOnMapList.erase(it);
}

void Mapa::processPlayerStatus(GameUpdate m){
	for (int i = 0; i < m.players.size(); i++){
		PlayerStatus s = m.players[i];
		RealPlayer *rp = this->realPlayersList[s.name];
		if (rp){
			rp->UpdateStatus(s.stone, s.wood, s.gold, s.food, s.status);
		}
		else{
			//hay un nuevo jugador.
			string realPlayerName = s.name;
			RealPlayer* newRealPlayer = new RealPlayer(s.stone, s.wood, s.gold, s.food, s.status, s.name);

			realPlayersList[realPlayerName] = newRealPlayer;
		}
	}
}


void Mapa::disconnectedPlayer(PlayerStatus pStatus){
	this->players.erase(pStatus.name);
}


void Mapa::createNewTimedOutPlayer(PlayerStatus ps){
	Player *discPlayer = this->players[ps.name];
	string sprite_name = "PlayerDisconnected";
	Sprite *characterSprite = this->spriteDictionary->getSpriteFromName(sprite_name);

	//delete discPlayer->spriteAsociado;
	discPlayer->spriteAsociado = characterSprite;

}
