#include "../Headers/Timer.h"


Timer::Timer()
{
	TicksEnInicio = 0;
	TicksEnPausa = 0;

	Pausado = false;
	Iniciado = false;
}

Timer::~Timer()
{
	//No hay memoria para liberar por el momento
}

void Timer::Iniciar()
{
	Iniciado = true;
	Pausado = false;

	TicksEnInicio = SDL_GetTicks();

	TicksEnPausa = 0;
}

void Timer::Detener()
{
	Iniciado = false;
	Pausado = false;

	TicksEnInicio = 0;
	TicksEnPausa = 0;
}

void Timer::Pausa()
{
	if (Iniciado && !Pausado)
	{
		Pausado = true;

		//Calculate the paused ticks:
		TicksEnPausa = SDL_GetTicks() - TicksEnInicio;
		TicksEnInicio = 0;
	}
}

void Timer::Continuar()
{
	if (Iniciado && Pausado)
	{
		Pausado = false;

		//reseteo los ticks
		TicksEnInicio = SDL_GetTicks() - TicksEnPausa;

		TicksEnPausa = 0;
	}
}

Uint32 Timer::getTicks()
{
	Uint32 time = 0;

	if (Iniciado)
	{
		if (Pausado)
		{
			time = TicksEnPausa;
		}
		else
		{
			time = SDL_GetTicks() - TicksEnInicio;
		}
	}

	return time;
}

bool Timer::EstaIniciado()
{
	return Iniciado;
}

bool Timer::EstaPausado()
{
	return Pausado && Iniciado;
}