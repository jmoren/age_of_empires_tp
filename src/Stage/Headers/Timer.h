#ifndef TIMER_H_
#define TIMER_H_


#include "SDL.h"


class Timer
{
public:
	Timer();
	virtual ~Timer();

	void Iniciar();
	void Detener();
	void Pausa();
	void Continuar();

	Uint32 getTicks();

	bool EstaIniciado();
	bool EstaPausado();

private:
	Uint32 TicksEnInicio;		//Tiempo en el comienzo
	Uint32 TicksEnPausa;	//Tiempo a la pausa

	bool Pausado;
	bool Iniciado;
};


#endif /* TIMER_H_ */
