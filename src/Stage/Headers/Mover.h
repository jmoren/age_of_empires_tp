#ifndef MOVER_H_
#define MOVER_H_


#include <iostream>
#include "../../logger/logger.h"
#include "../../entity/Player.h"
class Player;
class Mover{
	public:		
		bool movementIsProducing;
		int selectedTileX;
		int selectedTileY;
		Logger *logger;
		Mover();
		~Mover();

		int getDistance(int X1, int Y1, int X2, int Y2);
		void setMotion();
		void stopMotion();
		void setDirection(Player *entityToMove);
		void userClickedTile(int tileX, int tileY);
		void moveEntity(Player &player, int positionClickPixelsX, int positionClickPixelsY, int entityPositionX, int entityPositionY, int velocity);
		
		int dx, dy, px, py, stepy, stepx;
		string direction;
	};
	
#endif /* MOVER_H_ */
