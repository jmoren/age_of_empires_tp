#ifndef GLOBAL_H_
#define GLOBAL_H_

/*Definir los valores tomados de yaml a este archivo*/

//Constantes:
const bool VSYNC_ACTIVE = false;

const bool USE_CUSTOM_CURSOR = true;

const int PANTALLA_FPS = 25;
const int PANTALLA_TICKS_POR_FRAME = 1000 / PANTALLA_FPS;

//const int ANCHO_PANTALLA = 1024;
//const int ALTO_PANTALLA = 768;
//
const int TAMANIO_TILE = 48;
//
//const int LIMITE_SPRITE = 1024;


#endif /* GLOBAL_HPP_ */