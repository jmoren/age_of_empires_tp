#ifndef MAPA_H_
#define MAPA_H_

#include <string>
#include <cmath>
#include <vector>

#include "../../sprite/Sprite.h"
#include "../../sprite/SpriteMap.h"

#include "../../entity/person/GroupPlayers.h"
#include "../../entity/person/Soldado.h"
#include "../../entity/person/Aldeano.h"
#include "../../entity/person/Pikeman.h"
#include "../../entity/person/Rey.h"
#include "../../entity/building/CentroCivico.h"
#include "../../entity/building/Cuartel.h"
#include "../../parser/config.h"
#include "../../entity/Player.h"
#include "../../entity/EntityBase.h"
#include "../../entity/Element.h"
#include "../../entity/Tile.h"
#include "../../network/Data.h"
#include "../../entity/RealPlayer.h"

#include "Mover.h"
#include "Global.h"

class Mapa
{
public:
	Mapa(SDL_Renderer *renderer, SDL_Window *ventana, Config *aConfig);
	virtual ~Mapa();

	// SDL 
	int screenWidth;
	int screenHeight;

	SDL_Renderer *Renderer;
	SDL_Window *Ventana;
	Config *config;

	// Players and elements
	int sizex;
	int sizey;

	vector<Tile*> tiles;	
	map<string,Player *> players;
	RealPlayer *realPlayer;
	string realPlayerName;
	Tile *current_tile;
	void linkear(EntityBase* entityBase);
	void deslinkear(EntityBase* entityBase);
	//Current entity base(player,edificio), es el que esta seleccionado
	EntityBase* currentEntityBase;
	GroupPlayers* currentGroupPlayer;
	Sprite* getSpriteEntity(EntityStatus entity);
	Sprite* getSpriteElement(EntityStatus entity);

	Sprite* getNewSpriteWithAction(GameEntity entity, string action);
	Sprite* getNewSpriteWithAction(EntityStatus entity, string action);

	bool evaluateBuildingArea(int mousePosX, int mousePosY, int sizeX, int sizeY);

	//mapa de jugadores en partida
	map<string, RealPlayer*> realPlayersList;

	//Son todas las unidades que hay en el mapa.
	map<int,Player*> unitsOnMapList;

	//mapa de elementos en mapa (estructuras, recursos, arboles, etc.)
	map<int, Element*> elementsOnMapList;

	//Son todos los edificios que hay en el mapa.
	map<int, Edificio*> edificiosOnMapList;
	
	//mapa de colores de jugadores
	map<string, string> realPlayersColors;

	bool tileExist(int tilePosX, int tilePosY);

	void addColorToRealPlayer(string realPlayerName, string color);
	string getRealPlayerColor(string realPlayerName);

	void deleteElementWithId(int id);
	void deleteEntityWithId(int id);

	void renderTorch(int posx, int posy, int camaraoffset_x, int camaraoffset_y, float zoom);
	void renderTorchBase(int posx, int posy, int sizeX, int sizeY, int camaraoffset_x, int camaraoffset_y, float zoom);

	//Mover *mover;
	SpriteMap *spriteDictionary;
	void load();
	void loadTable();

	void loadSpriteDictionary();
	void loadRealPlayers();
	

	void loadElement(GameElement ge);
	void loadTerrain(GameElement ge);
	void loadResource(GameElement ge);
	void loadPlayers();

	void doPlayersActions(int camaraOffsetX, int camaraOffsetY, float zoom);
	void performUnitAction(Player* aPlayer, int camaraOffsetX, int camaraOffsetY, int zoom);

	void loadDesign();
	CharacterSprite searchCharacterWithName(string aName);
	ElementSprite searchElementWithName(string aName);
	Sprite* getSpriteEntity(GameEntity entity);
	Sprite* getSpriteElement(GameEntity entity);
	Sprite* getSpriteElement(string entityName, int posX, int posY);
	// render content
	void render(int camaraoffset_x, int camaraoffset_y, float zoom = 1);
	void renderTiles(int camaraoffset_x, int camaraoffset_y, float zoom = 1);
	void renderElements(int camaraoffset_x, int camaraoffset_y, float zoom = 1);
	void renderStructures(int camaraoffset_x, int camaraoffset_y, float zoom = 1);
	bool isInMapRange(EntityBase* currentEntity, int camaraoffset_x, int camaraoffset_y, float zoom);
	void movePlayer(Player *player, int camaraoffset_x, int camaraoffset_y, float zoom);
	void createNewTimedOutPlayer(PlayerStatus ps);

	//message processing
	void processElementStatus(GameUpdate m);
	void processPlayerStatus(GameUpdate m);
	void processEntityStatus(GameUpdate m);

	// Map managment
	bool valid_pos(int x, int y);
	bool isPositionValid(int x, int y);

	int getMaxPixelY(float zoom = 1.0);
	int getMinPixelY(float zoom = 1.0);
	int getMaxPixelX(float zoom = 1.0);
	int getMinPixelX(float zoom = 1.0);

	int getIsoX(float x, float y, int camaraoffset_x, int camaraoffset_y, float zoom = 1.0);
	int getIsoY(float x, float y, int camaraoffset_x, int camaraoffset_y, float zoom = 1.0);

	int getIsoCoordinateX(float x, float y, int camaraoffset_x, int camaraoffset_y, float zoom = 1.0);
	int getIsoCoordinateY(float x, float y, int camaraoffset_x, int camaraoffset_y, float zoom = 1.0);

	int getPixelX(float x, float y, int camaraoffset_x = 0, int camaraoffset_y = 0, float zoom = 1.0);
	int getPixelY(float x, float y, int camaraoffset_x = 0, int camaraoffset_y = 0, float zoom = 1.0);
	
	int MaxPixelXTileIndex;
	int MinPixelXTileIndex;
	int MaxPixelYTileIndex;
	int MinPixelYTileIndex;

	vector<Tile *> currentTiles;

	void setCurrentTiles(int posX,int posY, int destX, int destY);
	Tile* getTile(int x, int y);
	
	void renderMiniatura(int camaraoffset_x, int camaraoffset_y, float zoomMini, float offset_x, float offset_y);
	
	void processMessage(GameUpdate update);
	


	void disconnectedPlayer(PlayerStatus pStatusRemove);
	
	void renderPlayers(int camaraOffsetX, int camaraOffsetY, float zoom);
	void movePlayers(int camaraOffsetX, int camaraOffsetY, float zoom);


	void drawAnjacentOf(int posXVisited, int posYVisited, int camaraoffset_x, int camaraoffset_y, float zoom);
	void drawAdjacents(int posXVisited, int posYVisited, int camaraoffset_x, int camaraoffset_y, float zoom);
	void renderRect(int offset_x,int offset_y, int camaraoffset_x, int camaraoffset_y, float zoom);


private:
	bool DibujarGrilla;
	bool IsUnit(string entityName);
	bool IsBuilding(string entityName);
	bool IsInfantry(string entityName);
};


#endif /* MAPA_H_ */
