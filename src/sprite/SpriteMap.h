#ifndef SPRITEMAP_H_
#define SPRITEMAP_H_

#include "Sprite.h"

#include <iostream>
#include <string>
#include <map>


class SpriteMap{

private:
	std::map<std::string, Sprite*> mapaSprites;

public:
	SpriteMap();
	~SpriteMap();

	void AddSprite(std::string spriteName, Sprite *sprite);
	Sprite* getSpriteFromName(std::string name);

};

#endif //SPRITEMAP_H_