#include "Textura.h"
#include "../logger/logger.h"

SDL_Texture *CargarTextura(std::string file, SDL_Renderer *renderer, bool colorkey){
	Logger *logger = Logger::Instance();

	SDL_Texture *textura = nullptr;
	SDL_Surface *ImagenCargada = IMG_Load(file.c_str());

	if (ImagenCargada != nullptr){
		if (colorkey){
			SDL_SetColorKey(ImagenCargada, SDL_TRUE, SDL_MapRGB(ImagenCargada->format, 0, 0xFF, 0xFF));	//Color Cyan.
		}
		
		textura = SDL_CreateTextureFromSurface(renderer, ImagenCargada);
		SDL_FreeSurface(ImagenCargada);
		
		if (textura == nullptr)
			logger->log(Logger::TypeMessage::LOG_ERROR, "CreacionTexturaDeSDLSurface");
	}
	else
		logger->log(Logger::TypeMessage::LOG_ERROR, "CargaBMP");

	return textura;
}


void RenderizarTextura(SDL_Texture *textura, SDL_Renderer *renderer, int pixelX, int pixelY, int width, int height)
{
	SDL_Rect dst;
	dst.x = pixelX + 10;
	dst.y = pixelY - 10;
	dst.w = width;
	dst.h = height;
	SDL_RenderCopy(renderer, textura, nullptr, &dst);
}

void RenderizarTexturaEnMovimiento(LTexture *texturaMovimiento, SDL_Renderer *renderer, int frameARenderizar, SDL_Rect *currentClip, int pixelX, int pixelY){
	texturaMovimiento->render(pixelX, pixelY, currentClip, renderer, 0, 0);
}

void RenderizarTextura(SDL_Texture *textura, SDL_Renderer *renderer, int pixelX, int pixelY)
{
	int width, height;
	SDL_QueryTexture(textura, NULL, NULL, &width, &height);
	RenderizarTextura(textura, renderer, pixelX, pixelY, width, height);
}
