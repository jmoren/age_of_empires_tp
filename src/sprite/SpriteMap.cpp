#include "SpriteMap.h"

SpriteMap::SpriteMap(){

}

SpriteMap::~SpriteMap(){
	
	for(std::map<std::string, Sprite*>::iterator itr = this->mapaSprites.begin(); itr != this->mapaSprites.end(); itr++) {
		delete itr->second; 
	}
	std::map<std::string, Sprite*>().swap(this->mapaSprites);

}

void SpriteMap::AddSprite(std::string spriteName, Sprite *sprite){
	this->mapaSprites.insert(std::make_pair(spriteName, sprite));
}

Sprite* SpriteMap::getSpriteFromName(std::string name){
	return this->mapaSprites[name];
}