#include "Sprite.h"

Sprite::Sprite(std::string nombreArchivo, float x, float y, SDL_Renderer *renderer, SDL_Window *ventana)
{
	Textura = nullptr;
	Renderer = nullptr;
	Ventana = nullptr;
	this->DynamicTexture = new LTexture();
	this->FluidTexture = new LTexture();
	this->ImgClip = new SDL_Rect();
	Textura = CargarTextura(nombreArchivo, renderer, true);

	Renderer = renderer;
	Ventana = ventana;

	setPosX(x);
	setPosY(y);

	setDestX(x);
	setDestY(y);

	setMoviendo(false);
	this->Recollecting = false;
	this->Attacking = false;
	this->Building = false;
	this->EnMovimiento = false;

	this->desplazamientoX = 0;
	this->desplazamientoY = 0;
	this->spriteState = Terrain;
}

void Sprite::BuildBasicSpriteData(std::string fileName, float x, float y, int width, int height, SDL_Renderer *renderer, SDL_Window *window){
	Textura = nullptr;
	Renderer = nullptr;
	Ventana = nullptr;

	Textura = CargarTextura(fileName, renderer, true);
	Renderer = renderer;
	Ventana = window;

	this->width = width;
	this->height = height;

	setPosX(x);
	setPosY(y);

	setDestX(x);
	setDestY(y);

	setMoviendo(false);
	this->desplazamientoX = 0;
	this->desplazamientoY = 0;
	this->delayFluid = 0;
	this->delayFrameCounter = 0;
	this->fps = 0;
	this->waitingDelay = false;
	this->fpsSkipAmount = 0;
	this->fpsDynamic = 0;
	this->changeClip = true;
}

void Sprite::BuildSpriteClip(std::string fileName, int frames){
	//this->FluidTexture->loadFromFile(fileName, this->Renderer);
	this->ImgClip = new SDL_Rect();
	this->ImgClip->x = 0;
	this->ImgClip->y = 0;
	this->ImgClip->w = this->width;
	this->ImgClip->h = this->height;
	this->frames = frames;
	this->actualFrame = 0;
}

//Este metodo setea el rectangulo en la parte de la spritesheet que hay que renderizar.
void Sprite::MoveClipTo(int direction, int actualFrame){
	this->ImgClip->x = this->ImgClip->w * actualFrame;
	this->ImgClip->y = this->ImgClip->h * direction;
}

Sprite::Sprite(std::string fileName, std::string spriteType, float x, float y, int width, int height, int frames, SDL_Renderer *renderer, SDL_Window *window){
	this->DynamicTexture = new LTexture();
	this->FluidTexture = new LTexture();
	InitializeDirectionMap();

	if (spriteType == "Static"){
		//llamo al constructor de sprite normal con una sola imagen.
		this->spriteState = Static;
		this->BuildBasicSpriteData(fileName, x, y, width, height, renderer, window);
		this->ImgClip = new SDL_Rect();
	}
	else if (spriteType == "Fluid"){
		//Estructura quieta pero con movimiento en el lugar.
		this->spriteState = Fluid;
		this->BuildBasicSpriteData(fileName, x, y, width, height, renderer, window);
		this->FluidTexture->loadFromFile(fileName, renderer);
		this->BuildSpriteClip(fileName, frames);
	}
	else if (spriteType == "Dynamic"){
		//cargo la imagen Idle y la hoja de cuando este en movimiento.
		this->spriteState = Dynamic;
		this->BuildBasicSpriteData(fileName, x, y, width, height, renderer, window);
		this->DynamicTexture->loadFromFile(fileName, renderer);
		this->BuildSpriteClip(fileName, frames);
	}
}

void Sprite::InitializeDirectionMap(){
	this->directionsMap.insert(std::make_pair("S", 0));
	this->directionsMap.insert(std::make_pair("SW", 1));
	this->directionsMap.insert(std::make_pair("W", 2));
	this->directionsMap.insert(std::make_pair("NW", 3));
	this->directionsMap.insert(std::make_pair("N", 4));
	this->directionsMap.insert(std::make_pair("NE", 5));
	this->directionsMap.insert(std::make_pair("E", 6));
	this->directionsMap.insert(std::make_pair("SE", 7));
}

Sprite::~Sprite()
{
	SDL_DestroyTexture(this->Textura);
	this->Textura = nullptr;
	if(this->DynamicTexture != NULL){
		delete this->DynamicTexture;
		this->DynamicTexture = nullptr;
	}
	if(this->FluidTexture != NULL){
		delete this->FluidTexture;	
		this->FluidTexture = nullptr;
	}
	if(this->ImgClip !=NULL){
		delete this->ImgClip;
		this->ImgClip = nullptr;
	}
}

void Sprite::RenderTileSprite(int camaraOffsetX, int camaraOffsetY, float zoom, int altura, int x, int y){
	pixel_y = getPixelY(camaraOffsetX, camaraOffsetY, zoom, x, y) - altura;
	pixel_x = getPixelX(camaraOffsetX, camaraOffsetY, zoom, x, y);

	//Renderizo lo visible:
	const int tolerancia_offscreen = 3 * TAMANIO_TILE*zoom;
	int screen_width;
	int screen_height;
	SDL_GetWindowSize(Ventana, &screen_width, &screen_height);

	if ((pixel_x >= 0 - tolerancia_offscreen) ||
		(pixel_x + TAMANIO_TILE*zoom <= screen_width + tolerancia_offscreen) ||
		(pixel_y >= 0 - tolerancia_offscreen) ||
		(pixel_y + TAMANIO_TILE*zoom <= screen_height + tolerancia_offscreen))
	{
		RenderizarTextura(Textura, Renderer, pixel_x, pixel_y, TAMANIO_TILE*zoom, TAMANIO_TILE*zoom);
	}

	this->setPixelX(this->getPixelX(camaraOffsetX, camaraOffsetY, zoom));
	this->setPixelY(this->getPixelY(camaraOffsetX, camaraOffsetY, zoom));
}

void Sprite::RenderCurrentTileSprite(int camaraOffsetX, int camaraOffsetY, float zoom, int altura, int x, int y){
	pixel_y = getPixelY(camaraOffsetX, camaraOffsetY, zoom, x, y) - altura;
	pixel_x = getPixelX(camaraOffsetX, camaraOffsetY, zoom, x, y);

	RenderizarTextura(Textura, Renderer, pixel_x, pixel_y, TAMANIO_TILE*zoom, TAMANIO_TILE*zoom);

	this->setPixelX(this->getPixelX(camaraOffsetX, camaraOffsetY, zoom));
	this->setPixelY(this->getPixelY(camaraOffsetX, camaraOffsetY, zoom));
}

void Sprite::RenderStaticSprite(int camaraoffset_x, int camaraoffset_y, float zoom, int altura, int x, int y){
	pixel_y = getPixelY(camaraoffset_x, camaraoffset_y, zoom, x, y) - altura;
	pixel_x = getPixelX(camaraoffset_x, camaraoffset_y, zoom, x, y);
	RenderizarTextura(Textura, Renderer, pixel_x + this->Dest_x, pixel_y + Dest_y, this->width, this->height);

	this->setPixelX(this->getPixelX(camaraoffset_x, camaraoffset_y, zoom));
	this->setPixelY(this->getPixelY(camaraoffset_x, camaraoffset_y, zoom));
	
}

void Sprite::setDynamicBlend(int value){
	if (this->DynamicTexture != nullptr){
		this->DynamicTexture->setAlpha(value);
	}
}

void Sprite::RenderFluidSprite(int camaraoffset_x, int camaraoffset_y, float zoom, int posx, int posy){

	if (!this->waitingDelay){
		if (fps != 0){
			this->fpsSkipAmount = PANTALLA_FPS / this->fps;

			if (this->changeClip){
				this->MoveClipTo(this->directionsMap[this->direction], actualFrame);

				pixel_y = getPixelY(camaraoffset_x, camaraoffset_y, zoom, posx, posy);
				pixel_x = getPixelX(camaraoffset_x, camaraoffset_y, zoom, posx, posy);

				this->FluidTexture->render(pixel_x, pixel_y, this->ImgClip, this->Renderer, 32, 32);

				this->actualFrame++;

				if (this->actualFrame / 11.0 > 1.0)
				{
					this->actualFrame = 0;
					this->waitingDelay = true;
					this->MoveClipTo(0, 11);
				}
				//termino de animar y pauso el movimiento del clip apra la proxima.
				this->changeClip = false;
			}
			else{
				this->fpsDynamic++;
				if (this->fpsDynamic > this->fpsSkipAmount){
					//hay que mover el clip en el siguiente frame.
					this->changeClip = true;
					this->fpsDynamic = 0;
				}
				pixel_y = getPixelY(camaraoffset_x, camaraoffset_y, zoom, posx, posy);
				pixel_x = getPixelX(camaraoffset_x, camaraoffset_y, zoom, posx, posy);

				this->FluidTexture->render(pixel_x, pixel_y, this->ImgClip, this->Renderer, 32, 32);
			}
		}
		else{
			this->MoveClipTo(0, actualFrame);

			pixel_y = getPixelY(camaraoffset_x, camaraoffset_y, zoom, posx, posy);
			pixel_x = getPixelX(camaraoffset_x, camaraoffset_y, zoom, posx, posy);

			this->FluidTexture->render(pixel_x, pixel_y, this->ImgClip, this->Renderer, 32, 32);

			this->actualFrame++;

			if (this->actualFrame / 11.0 > 1.0)
			{
				//termine un ciclo de render de sprite.
				this->actualFrame = 0;
				if (this->delayFluid != 0){
					this->waitingDelay = true;
				}
			}
		}
	}
	else{
		if (delayFrameCounter == this->delayFluid * PANTALLA_FPS){
			//el siguiente ciclo va a renderizar la animacion.
			this->waitingDelay = false;
			this->delayFrameCounter = 0;
		}
		else{
			delayFrameCounter++;
		}

		pixel_y = getPixelY(camaraoffset_x, camaraoffset_y, zoom, posx, posy);
		pixel_x = getPixelX(camaraoffset_x, camaraoffset_y, zoom, posx, posy);
		
		this->FluidTexture->render(pixel_x, pixel_y, this->ImgClip, this->Renderer, 32, 32);
	}
}

void Sprite::RenderDynamicSprite(int camaraoffset_x, int camaraoffset_y, float zoom){

	if (fps != 0){
		this->fpsSkipAmount = PANTALLA_FPS / this->fps;

		if (this->changeClip){
			this->MoveClipTo(this->directionsMap[this->direction], actualFrame);

			pixel_y = getPixelY(camaraoffset_x, camaraoffset_y, zoom);
			pixel_x = getPixelX(camaraoffset_x, camaraoffset_y, zoom);

			this->DynamicTexture->render(pixel_x, pixel_y, this->ImgClip, this->Renderer, this->pixelOffsetX, this->pixelOffsetY);

			this->actualFrame++;

			if (this->actualFrame / (float)(this->frames - 1) > 1.0)
			{
				this->actualFrame = 0;
			}
			//termino de animar y pauso el movimiento del clip apra la proxima.
			this->changeClip = false;
		} else{
			this->fpsDynamic++;
			if (this->fpsDynamic > this->fpsSkipAmount){
				//hay que mover el clip en el siguiente frame.
				this->changeClip = true;
				this->fpsDynamic = 0;
			}
			pixel_y = getPixelY(camaraoffset_x, camaraoffset_y, zoom);
			pixel_x = getPixelX(camaraoffset_x, camaraoffset_y, zoom);
			//x = 15 y = -5 ALDEANO
			this->DynamicTexture->render(pixel_x, pixel_y, this->ImgClip, this->Renderer, this->pixelOffsetX, this->pixelOffsetY);
		}
	} else{
		this->MoveClipTo(this->directionsMap[this->direction], actualFrame);

		pixel_y = getPixelY(camaraoffset_x, camaraoffset_y, zoom);
		pixel_x = getPixelX(camaraoffset_x, camaraoffset_y, zoom);

		this->DynamicTexture->render(pixel_x, pixel_y, this->ImgClip, this->Renderer, this->pixelOffsetX, this->pixelOffsetY);

		this->actualFrame++;

		if (this->actualFrame / (float)(this->frames - 1) > 1.0)
		{
			this->actualFrame = 0;
		}
	}
}

void Sprite::RenderDynamicSpriteRecollecting(int camaraoffset_x, int camaraoffset_y, float zoom, int altura){
		this->MoveClipTo(this->directionsMap[this->direction], actualFrame);

		pixel_y = getPixelY(camaraoffset_x, camaraoffset_y, zoom);
		pixel_x = getPixelX(camaraoffset_x, camaraoffset_y, zoom);

		this->DynamicTexture->render(pixel_x, pixel_y, this->ImgClip, this->Renderer, this->pixelOffsetX, this->pixelOffsetY);

		this->actualFrame++;

		if (this->actualFrame / (float)(this->frames - 1) > 1.0)
			this->DynamicTexture->render(pixel_x, pixel_y, this->ImgClip, this->Renderer, this->pixelOffsetX, this->pixelOffsetX);

		this->actualFrame++;

		if (this->actualFrame / (float)(this->frames - 1) > 1.0)
		{
			this->actualFrame = 0;
		}
}

void Sprite::RenderDynamicSpriteBuilding(int camaraoffset_x, int camaraoffset_y, float zoom, int altura){
	this->MoveClipTo(this->directionsMap[this->direction], actualFrame);

	pixel_y = getPixelY(camaraoffset_x, camaraoffset_y, zoom);
	pixel_x = getPixelX(camaraoffset_x, camaraoffset_y, zoom);

	this->DynamicTexture->render(pixel_x, pixel_y, this->ImgClip, this->Renderer, this->pixelOffsetX, this->pixelOffsetY);

	this->actualFrame++;

	if (this->actualFrame / (float)(this->frames - 1) > 1.0)
		this->DynamicTexture->render(pixel_x, pixel_y, this->ImgClip, this->Renderer, this->pixelOffsetX, this->pixelOffsetY);

	this->actualFrame++;

	if (this->actualFrame / (float)(this->frames - 1) > 1.0)
	{
		this->actualFrame = 0;
	}
}

void Sprite::RenderDynamicSpriteAttacking(int camaraoffset_x, int camaraoffset_y, float zoom, int altura){
	this->MoveClipTo(this->directionsMap[this->direction], actualFrame);

	pixel_y = getPixelY(camaraoffset_x, camaraoffset_y, zoom);
	pixel_x = getPixelX(camaraoffset_x, camaraoffset_y, zoom);

	this->DynamicTexture->render(pixel_x, pixel_y, this->ImgClip, this->Renderer, this->pixelOffsetX, this->pixelOffsetY);

	this->actualFrame++;

	if (this->actualFrame / (float)(this->frames - 1) > 1.0)
		this->DynamicTexture->render(pixel_x, pixel_y, this->ImgClip, this->Renderer, this->pixelOffsetX, this->pixelOffsetY);

	this->actualFrame++;

	if (this->actualFrame / (float)(this->frames - 1) > 1.0)
	{
		this->actualFrame = 0;
	}
}

void Sprite::RenderDynamicSpriteIdle(int camaraoffset_x, int camaraoffset_y, float zoom, int altura){
	this->MoveClipTo(this->directionsMap[""], 0);

	pixel_y = getPixelY(camaraoffset_x, camaraoffset_y, zoom);
	pixel_x = getPixelX(camaraoffset_x, camaraoffset_y, zoom);

	this->DynamicTexture->render(pixel_x, pixel_y, this->ImgClip, this->Renderer, this->pixelOffsetX, this->pixelOffsetY);

	this->actualFrame++;

	if (this->actualFrame / (float)(this->frames - 1) > 1.0)
	{
		this->actualFrame = 0;
	}
}

int Sprite::getPixelX(int camaraoffset_x, int camaraoffset_y, float zoom)
{
	pixel_x = 0;

	if (this->EnMovimiento){
		pixel_x = (TAMANIO_TILE*zoom * getPosX() * 0.5) + (TAMANIO_TILE*zoom * getPosY() * 0.5) - camaraoffset_x;
		pixel_x = pixel_x + this->desplazamientoX;
	}
	else{
		pixel_x = (TAMANIO_TILE*zoom * getPosX() * 0.5) + (TAMANIO_TILE*zoom * getPosY() * 0.5) - camaraoffset_x;
	}
	return pixel_x;
}

int Sprite::getPixelY(int camaraoffset_x, int camaraoffset_y, float zoom)
{
	pixel_y = 0;

	if (this->EnMovimiento){
		pixel_y = ((TAMANIO_TILE*zoom * getPosX() * 0.25) - (TAMANIO_TILE*zoom * getPosY() * 0.25)) - camaraoffset_y;
		pixel_y = pixel_y + this->desplazamientoY;
	}
	else{
		pixel_y = ((TAMANIO_TILE*zoom * getPosX() * 0.25) - (TAMANIO_TILE*zoom * getPosY() * 0.25)) - camaraoffset_y;
	}

	return pixel_y;
}

int Sprite::getPixelX(int camaraoffset_x, int camaraoffset_y, float zoom, int posx, int posy){
	int pixel_x = 0;
	if (this->EnMovimiento){
		pixel_x = (TAMANIO_TILE*zoom * posx * 0.5) + (TAMANIO_TILE*zoom * posy * 0.5) - camaraoffset_x;
		pixel_x = pixel_x + this->desplazamientoX;
	}
	else{
		pixel_x = (TAMANIO_TILE*zoom * posx * 0.5) + (TAMANIO_TILE*zoom * posy * 0.5) - camaraoffset_x;
	}
	
	return pixel_x;
}

int Sprite::getPixelY(int camaraoffset_x, int camaraoffset_y, float zoom, int posx, int posy){
	int pixel_y = 0;
	this;
	if (this->EnMovimiento){
		pixel_y = ((TAMANIO_TILE*zoom * posx * 0.25) - (TAMANIO_TILE*zoom * posy * 0.25)) - camaraoffset_y;
		pixel_y = pixel_y + this->desplazamientoY;
	}
	else{
		pixel_y = ((TAMANIO_TILE*zoom * posx * 0.25) - (TAMANIO_TILE*zoom * posy * 0.25)) - camaraoffset_y;
	}
	
	return pixel_y;
}

int Sprite::getZOrder(){
	return getPixelY(0, 0, 1);
}
