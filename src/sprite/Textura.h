#ifndef TEXTURA_H_
#define TEXTURA_H_


#include <iostream>
#include <string>

#include "SDL.h"
#include "SDL_image.h"
#include "LTexture.h"


SDL_Texture *CargarTextura(std::string file, SDL_Renderer *renderer, bool colorkey = true);

void RenderizarTextura(SDL_Texture *textura, SDL_Renderer *renderer, int x, int y, int w, int h);

void RenderizarTexturaEnMovimiento(LTexture *texturaMovimiento, SDL_Renderer *renderer, int frameARenderizar, SDL_Rect *currentClip, int pixelX, int pixelY);

void RenderizarTextura(SDL_Texture *textura, SDL_Renderer *renderer, int x, int y);


#endif /* TEXTURA_H_ */
