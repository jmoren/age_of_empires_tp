#ifndef SPRITE_H_
#define SPRITE_H_


#include <string>
#include <iostream>
#include <map>

#include "SDL.h"
#include "SDL_image.h"
#include "Textura.h"
#include "../Stage/Headers/Global.h"


using namespace std;

class Sprite {

public:
	enum State { Terrain, Static, Fluid, Dynamic };
	Sprite(string nombreFile, float x, float y, SDL_Renderer *renderer, SDL_Window *ventana);
	Sprite(string fileName, string spriteType, float x, float y, int width, int height, int frames, SDL_Renderer *renderer, SDL_Window *window);
	virtual ~Sprite();

	SDL_Texture *Textura;
	LTexture *FluidTexture;
	LTexture *DynamicTexture;
	SDL_Rect *ImgClip;
	SDL_Renderer *Renderer;
	SDL_Window *Ventana;
	State spriteState;

	void BuildBasicSpriteData(string fileName, float x, float y, int width, int height, SDL_Renderer *renderer, SDL_Window *window);
	void BuildSpriteClip(string fileName, int frames);

	void RenderFluidSprite(int camaraoffset_x, int camaraoffset_y, float zoom, int posx, int posy);

	void RenderDynamicSprite(int camaraoffset_x, int camaraoffset_y, float zoom);
	void RenderDynamicSpriteIdle(int camaraoffset_x, int camaraoffset_y, float zoom, int altura);
	void RenderDynamicSpriteRecollecting(int camaraoffset_x, int camaraoffset_y, float zoom, int altura);
	void RenderDynamicSpriteBuilding(int camaraoffset_x, int camaraoffset_y, float zoom, int altura);
	void RenderDynamicSpriteAttacking(int camaraoffset_x, int camaraoffset_y, float zoom, int altura);

	void RenderStaticSprite(int camaraoffset_x, int camaraoffset_y, float zoom, int altura, int x, int y);

	void RenderTileSprite(int camaraoffset_x, int camaraoffset_y, float zoom, int altura, int posx, int posy);
	void RenderCurrentTileSprite(int camaraoffset_x, int camaraoffset_y, float zoom, int altura, int posx, int posy);

	void setDynamicBlend(int value);

	void InitializeDirectionMap();

	int desplazamientoX;
	int desplazamientoY;
	int frames;
	int actualFrame;
	string direction;
	int width;
	int height;
	int pixelOffsetX;
	int pixelOffsetY;
	bool EnMovimiento;
	bool Building;
	bool Recollecting;
	bool Attacking;
	int fps;
	int fpsSkipAmount;
	int fpsDynamic;
	bool changeClip;
	int delayFluid;
	int delayFrameCounter;
	bool waitingDelay;
	map<string, int> directionsMap;

	int getPixelX(int camaraoffset_x, int camaraoffset_y, float zoom, int posx, int posy);
	int getPixelY(int camaraoffset_x, int camaraoffset_y, float zoom, int posx, int posy);
	int getPixelX(int camaraoffset_x, int camaraoffset_y, float zoom);
	int getPixelY(int camaraoffset_x, int camaraoffset_y, float zoom);
	
	void MoveClipTo(int direction, int actualFrame);
	int getZOrder();

	void setPixelX(int posX) { pixel_x = posX; }
	void setPixelY(int posY) { pixel_y = posY; }
	void setPosX(int posx)   { Pos_x = posx; }
	void setDestX(int destx) { Dest_x = destx; }
	void setPosY(int posy) { Pos_y = posy; }
	void setDestY(int desty) { Dest_y = desty; }

	int getPixelX() { return pixel_x; }
	int getPixelY() { return pixel_y; }
	int getPosX()  const { return Pos_x; }
	int getPosY()  const { return Pos_y; }
	int getDestX() const { return Dest_x; }
	int getDestY() const { return Dest_y; }

	bool SeMueve() const { return EnMovimiento; }
	void setMoviendo(bool moviendo) { EnMovimiento = moviendo; }
	
private:
	int Pos_x;
	int Pos_y;
	int pixel_x;
	int pixel_y;
	int Dest_x;
	int Dest_y;
};


#endif /* SPRITE_H_ */