#include <iostream>
#include <fstream>
#include <string>

#include "parser.h"

Parser::Parser(Config *config){
	this->user_file = "./src/configFiles/map.yaml";
	this->default_file = "./src/configFiles/map_default.yaml";
	this->config = config;
	this->logger = Logger::Instance();
};

Parser::~Parser(){
  
};

int Parser::parse(){
	return openFiles();
};

int Parser::openFiles(){
	logger->log(Logger::TypeMessage::LOG_INFO, "Abriendo archivos de configuracion");
	
	ifstream file(this->user_file);
	ifstream def_file(this->default_file);

  if (def_file.good()){
		nodeDefConfig = YAML::Load(def_file);
		logger->log(Logger::TypeMessage::LOG_INFO, "Archivo: " + this->default_file + " ...OK");
	}

	if (file.good()){
		try{
			nodeConfig = YAML::Load(file);
			logger->log(Logger::TypeMessage::LOG_INFO, "Archivo: " + this->user_file + " ...OK");
		}
		catch (YAML::ParserException){
			nodeConfig = nodeDefConfig;
			logger->log(Logger::TypeMessage::LOG_INFO, "Error en el formato del archivo: " + this->user_file + " ... :(");
		}
	}
	else{
		bool file_not_exists = false;
		logger->log(Logger::TypeMessage::LOG_WARNING, "El archivo " + this->user_file + " no existe");
	}

	return parseScreen();
};

int Parser::parseScreen(){
	int parse_result = 0;
	if (nodeConfig["screen"]){
		if (nodeConfig["screen"]["title"])
			config->screen.title = nodeConfig["screen"]["title"].as<string>();
		else{
			config->screen.title = nodeDefConfig["screen"]["title"].as<string>();
			logger->log(Logger::TypeMessage::LOG_INFO, "No existe el nodo screen.title, se toma del default: " + config->screen.title);
		}

		try{
			if (nodeConfig["screen"]["height"])
				config->screen.height = nodeConfig["screen"]["height"].as<int>();
			else{
				config->screen.height = nodeDefConfig["screen"]["height"].as<int>();
				logger->log(Logger::TypeMessage::LOG_WARNING, config->screen.height, "No existe screen.height, se toma del default: %d");
			}
		}
		catch (YAML::TypedBadConversion<int>){
			config->screen.height = nodeDefConfig["screen"]["height"].as<int>();
			logger->log(Logger::TypeMessage::LOG_ERROR, config->screen.height, "Valor invalido en screen.height, se toma el default: %d");
		}
			
		try{
			if (nodeConfig["screen"]["width"])
				config->screen.width = nodeConfig["screen"]["width"].as<int>();
			else{
				config->screen.width = nodeDefConfig["screen"]["width"].as<int>();
				logger->log(Logger::TypeMessage::LOG_WARNING, config->screen.width, "No existe screen.width, se toma del default: %d");
			}
		}
		catch (YAML::TypedBadConversion<int>){
			config->screen.width = nodeDefConfig["screen"]["width"].as<int>();
			logger->log(Logger::TypeMessage::LOG_WARNING, config->screen.width, "Valor invalido en screen.width, se toma el default: %d");
		}

		return parseEntities();
	}

	return ERROR_PARSER_KEY_NOT_FOUND;
}

int Parser::parseEntities(){

	int status = 0;
	YAML::Node list;

	if (nodeConfig["entities"]){
		list = nodeConfig["entities"]["ground"];

		for (std::size_t i = 0; i < list.size(); i++) {

			if (list[i]["name"] && list[i]["path"] && list[i]["height"] && list[i]["width"]){
				GroundSprite ground;
				try{
					ground.name = list[i]["name"].as<string>();
					ground.path = list[i]["path"].as<string>();
					ground.height = list[i]["height"].as<int>();
					ground.width = list[i]["width"].as<int>();
					// check if image exists and load default if fails
					if (!file_exists(ground.path)){
						ground.path = "./resources/images/iso/TileNotFound.png";
					}
					config->entities.ground.push_back(ground);
				}
				catch (YAML::TypedBadConversion<int>){
					logger->log(Logger::TypeMessage::LOG_ERROR, i + 1, "Datos invalidos en entities.ground[%d] - No se agrega");
				}
			}
		}

		list = nodeConfig["entities"]["elements"];
		YAML::Node listdefault = nodeDefConfig["entities"]["elements"];
		for (std::size_t i = 0; i < list.size(); i++) {
			ElementSprite element;
			try{
				if (list[i]["name"])
					element.name = list[i]["name"].as<string>();
				else{
					element.name = listdefault[i]["name"].as<string>();
					logger->log(Logger::TypeMessage::LOG_WARNING, "No existe element.name, se toma del default: " + element.name);
				}

				if (list[i]["path"])
					element.path = list[i]["path"].as<string>();
				else{
					element.path = listdefault[i]["path"].as<string>();
					logger->log(Logger::TypeMessage::LOG_WARNING, "No existe element.path, se toma del default: " + element.path);
				}

				if (list[i]["tipo"])
					element.entityType = list[i]["tipo"].as<string>();
				else{
					element.entityType = listdefault[i]["tipo"].as<string>();
					logger->log(Logger::TypeMessage::LOG_WARNING, "No existe element.tipo, se toma del default: " + element.entityType);
				}

				try{
					if (list[i]["height"])
						element.height = list[i]["height"].as<int>();
					else{
						element.height = listdefault[i]["height"].as<int>();
						logger->log(Logger::TypeMessage::LOG_WARNING, element.height, "No existe element.height, se toma del default: %d");
					}
				}
				catch (YAML::TypedBadConversion<int>){
					element.height = listdefault[i]["height"].as<int>();
					logger->log(Logger::TypeMessage::LOG_WARNING, element.height, "Valor invalido en element.height, se toma el default: %d");
				}

				try{
					if (list[i]["width"])
						element.width = list[i]["width"].as<int>();
					else{
						element.width = listdefault[i]["width"].as<int>();
						logger->log(Logger::TypeMessage::LOG_WARNING, element.width, "No existe element.width, se toma del default: %d");
					}
				}
				catch (YAML::TypedBadConversion<int>){
					element.width = listdefault[i]["width"].as<int>();
					logger->log(Logger::TypeMessage::LOG_WARNING, element.width, "Valor invalido en element.width, se toma el default: %d");
				}

				try{
					if (list[i]["ref_x"])
						element.ref_x = list[i]["ref_x"].as<int>();
					else{
						element.ref_x = 0;
						logger->log(Logger::TypeMessage::LOG_WARNING, element.ref_x, "No existe element.ref_x, se toma del default: %d");
					}
				}
				catch (YAML::TypedBadConversion<int>){
					element.ref_x = 0;
					logger->log(Logger::TypeMessage::LOG_WARNING, element.ref_x, "Valor invalido en element.ref_x, se toma el default: %d");
				}

				try{
					if (list[i]["ref_y"])
						element.ref_y = list[i]["ref_y"].as<int>();
					else{
						element.ref_y = 0;
						logger->log(Logger::TypeMessage::LOG_WARNING, element.ref_y, "No existe element.ref_y, se toma del default: %d");
					}
				}
				catch (YAML::TypedBadConversion<int>){
					element.ref_y = 0;
					logger->log(Logger::TypeMessage::LOG_WARNING, element.ref_x, "Valor invalido en element.ref_y, se toma el default: %d");
				}
				
				try{
					if (list[i]["size_x"])
						element.size_x = list[i]["size_x"].as<int>();
					else{
						element.size_x = 1;
						logger->log(Logger::TypeMessage::LOG_WARNING, element.size_x, "No existe element.size_x, se toma del default: %d");
					}
				}
				catch (YAML::TypedBadConversion<int>){
					element.size_x = 1;
					logger->log(Logger::TypeMessage::LOG_WARNING, element.size_x, "Valor invalido en element.size_x, se toma el default: %d");
				}

				try{
					if (list[i]["size_y"])
						element.size_y = list[i]["size_y"].as<int>();
					else{
						element.size_y = 1;
						logger->log(Logger::TypeMessage::LOG_WARNING, element.size_y, "No existe element.size_y, se toma del default: %d");
					}
				}
				catch (YAML::TypedBadConversion<int>){
					element.size_y = 1;
					logger->log(Logger::TypeMessage::LOG_WARNING, element.size_y, "Valor invalido en element.size_y, se toma el default: %d");
				}

				try{
					if (list[i]["delay"])
						element.delay = list[i]["delay"].as<int>();
					else{
						element.delay = 0;
						logger->log(Logger::TypeMessage::LOG_WARNING, element.delay, "No existe element.delay, se toma del default: %d");
					}
				}
				catch (YAML::TypedBadConversion<int>){
					element.delay = 0;
					logger->log(Logger::TypeMessage::LOG_WARNING, element.delay, "Valor invalido en element.delay, se toma el default: %d");
				}

				try{
					if (list[i]["fps"])
						element.fps = list[i]["fps"].as<int>();
					else{
						element.fps = 0;
						logger->log(Logger::TypeMessage::LOG_WARNING, element.fps, "No existe element.fps, se toma del default: %d");
					}
				}
				catch (YAML::TypedBadConversion<int>){
					element.fps = 0;
					logger->log(Logger::TypeMessage::LOG_WARNING, element.fps, "Valor invalido en element.fps, se toma el default: %d");
				}

				// check if image exists and load default if fails
				if (!file_exists(element.path)){
					element.path = "./resources/images/iso/ElementNotFound.png";
				}
				config->entities.elements.push_back(element);
			}
			catch (YAML::TypedBadConversion<int>){
				logger->log(Logger::TypeMessage::LOG_ERROR, i + 1, "Datos invalidos en entities.elements[%d] - No se agrega");
			}

		}

		list = nodeConfig["entities"]["characters"];
		listdefault = nodeDefConfig["entities"]["characters"];

		for (std::size_t i = 0; i < list.size(); i++) {
			CharacterSprite character;
			try{
				if (list[i]["name"])
					character.name  = list[i]["name"].as<string>();
				else{
					character.name  = listdefault[i]["name"].as<string>();
					logger->log(Logger::TypeMessage::LOG_WARNING, "No existe character.name, se toma del default: " + character.name);
				}
				
				if (list[i]["path"])
					character.path  = list[i]["path"].as<string>();
				else{
					character.path  = listdefault[i]["path"].as<string>();
					logger->log(Logger::TypeMessage::LOG_WARNING, "No existe character.path, se toma del default: " + character.path);
				}
				
				try{
					if (list[i]["fps"])
						character.fps= list[i]["fps"].as<int>();
					else{
						character.fps = listdefault[i]["fps"].as<int>();
						logger->log(Logger::TypeMessage::LOG_WARNING, character.fps, "No existe character.fps, se toma del default: %d");
					}
				}
				catch (YAML::TypedBadConversion<int>){
					character.fps = listdefault[i]["fps"].as<int>();
					logger->log(Logger::TypeMessage::LOG_WARNING, character.fps , "Valor invalido en character.fps, se toma el default: %d");
				}

				try{
					if (list[i]["ref_x"])
						character.ref_x = list[i]["ref_x"].as<int>();
					else{
						character.ref_x = listdefault[i]["ref_x"].as<int>();
						logger->log(Logger::TypeMessage::LOG_WARNING, character.ref_x, "No existe character.ref_x, se toma del default: %d");
					}
				}
				catch (YAML::TypedBadConversion<int>){
					character.ref_x = listdefault[i]["ref_x"].as<int>();
					logger->log(Logger::TypeMessage::LOG_WARNING, character.ref_x , "Valor invalido en character.ref_x, se toma el default: %d");
				}
				
				try{
					if (list[i]["ref_y"])
						character.ref_y = list[i]["ref_y"].as<int>();
					else{
						character.ref_y = listdefault[i]["ref_y"].as<int>();
						logger->log(Logger::TypeMessage::LOG_WARNING, character.ref_y, "No existe character.ref_y, se toma del default: %d");
					}
				}
				catch (YAML::TypedBadConversion<int>){
					character.ref_y = listdefault[i]["ref_y"].as<int>();
					logger->log(Logger::TypeMessage::LOG_WARNING, character.ref_y , "Valor invalido en character.ref_y, se toma el default: %d");
				}

				try{
					if (list[i]["unitWidth"])
						character.unitWidth = list[i]["unitWidth"].as<int>();
					else{
						character.unitWidth = listdefault[i]["unitWidth"].as<int>();
						logger->log(Logger::TypeMessage::LOG_WARNING, character.unitWidth, "No existe character.unitWidth, se toma del default: %d");
					}
				}
				catch (YAML::TypedBadConversion<int>){
					character.unitWidth = listdefault[i]["unitWidth"].as<int>();
					logger->log(Logger::TypeMessage::LOG_WARNING, character.unitWidth, "Valor invalido en character.ref_y, se toma el default: %d");
				}

				try{
					if (list[i]["unitHeight"])
						character.unitHeight = list[i]["unitHeight"].as<int>();
					else{
						character.unitHeight = listdefault[i]["unitHeight"].as<int>();
						logger->log(Logger::TypeMessage::LOG_WARNING, character.unitHeight, "No existe character.unitHeight, se toma del default: %d");
					}
				}
				catch (YAML::TypedBadConversion<int>){
					character.unitHeight = listdefault[i]["unitHeight"].as<int>();
					logger->log(Logger::TypeMessage::LOG_WARNING, character.unitHeight, "Valor invalido en character.unitHeight, se toma el default: %d");
				}

				try{
					if (list[i]["frames"])
						character.frames = list[i]["frames"].as<int>();
					else{
						character.frames = listdefault[i]["frames"].as<int>();
						logger->log(Logger::TypeMessage::LOG_WARNING, character.frames, "No existe character.frames, se toma del default: %d");
					}
				}
				catch (YAML::TypedBadConversion<int>){
					character.frames = listdefault[i]["frames"].as<int>();
					logger->log(Logger::TypeMessage::LOG_WARNING, character.frames, "Valor invalido en character.frames, se toma el default: %d");
				}

				if (list[i]["tipo"])
					character.tipo = list[i]["tipo"].as<string>();
				else{
					character.tipo = listdefault[i]["tipo"].as<string>();
					logger->log(Logger::TypeMessage::LOG_WARNING, "No existe character.tipo, se toma del default: " + character.tipo);
				}


				// check if image exists and load default if fails
				if (!file_exists(character.path)){
					character.path = "./resources/images/iso/ElementNotFound.png";
				}
				if (config->entities.characters.find(character.name) == config->entities.characters.end())
					config->entities.characters.insert(make_pair(character.name, character));

			}
			catch (YAML::TypedBadConversion<int>){
				logger->log(Logger::TypeMessage::LOG_ERROR, i + 1, "Datos invalidos en entities.elements[%d] - No se agrega");
			}

		}

		return parseConfiguration();
	}

	return ERROR_PARSER_KEY_NOT_FOUND;
}

int Parser::parseConfiguration(){
	
	if (nodeConfig["configuration"]){

		if (nodeConfig["configuration"]["server"])
			config->config.server = nodeConfig["configuration"]["server"].as<string>();
		else{
			config->config.server = nodeDefConfig["connfiguration"]["server"].as<string>();
			logger->log(Logger::TypeMessage::LOG_INFO, "No existe el nodo configuration.server, se toma del default: " + config->config.server);
		}

		if (nodeConfig["configuration"]["port"])
			config->config.port = nodeConfig["configuration"]["port"].as<string>();
		else{
			config->config.port = nodeDefConfig["connfiguration"]["port"].as<string>();
			logger->log(Logger::TypeMessage::LOG_INFO, "No existe el nodo configuration.port, se toma del default: " + config->config.port);
		}
		return 0;
	}

	return ERROR_PARSER_KEY_NOT_FOUND;
}


bool Parser::file_exists(string name){
	ifstream image(name);
	if (image.good()){
		image.close();
		return true;
	}
	else{
		logger->log(Logger::TypeMessage::LOG_WARNING, "No existe la imagen: " + name);
		image.close();
		return false;
	}
}