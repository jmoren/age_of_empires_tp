#ifndef   _CONFIG_H_
#define   _CONFIG_H_

#include <string>
#include <vector>
#include "../network/Data.h"
#include "../entity/RealPlayer.h"
#include <map>
#define FILE_NOT_FOUND			   (-1)
#define FILE_ERROR				   (-2)
#define ERROR_PARSER_KEY_NOT_FOUND (-3)

using namespace std;

// local data
struct Screen{
	int height;
	int width;
	string title;
};

struct GroundSprite{
	string name;
	string path;
	int height;
	int width;
};

struct ElementSprite{
	string name;
	string path;
	string entityType;
	int delay;
	int fps;
	int ref_x;
	int ref_y;
	int size_x;
	int size_y;
	int height;
	int width;
};

struct CharacterSprite{
	string name;
	string path;
	string tipo;
	int fps;
	int ref_x;
	int ref_y;
	int unitWidth;
	int unitHeight;
	int frames;
};


// remote data
struct GameConfig{
	int velocity;
	int scroll;
	int scrollVelocity;
	string port;
	string server;
	string name;
	int size_x;
	int size_y;
	vector<int> road;
};

struct MyPlayer{
	string name;
	int mode;
	int id;
	int posx;
	int posy;
	
};

// hold info for create all sprites
struct Entities{
	vector<GroundSprite> ground;
	vector<ElementSprite> elements;
	map<string, CharacterSprite> characters;
};

class Config{
	public:
		Config();
		virtual ~Config();
		
		Screen screen;
		Entities entities;
		
		// all this info cames from server

		MyPlayer character;
		GameConfig config;
		
		vector<GameElement> ground;
		vector<GameElement> elements;
		vector<GameElement> resources;
		vector<PlayerStatus> realPlayers;
		vector<GameEntity> gameEntities;
};

#endif