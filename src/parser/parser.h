#ifndef   _PARSER_H_
#define   _PARSER_H_

#include "yaml-cpp/yaml.h"
#include "config.h"
#include "../logger/logger.h"

class Parser{
	public:
		Parser(Config *config);
		virtual ~Parser();
		Logger *logger;
		Config *config;
		
		string user_file;
		string default_file;
		YAML::Node nodeConfig;
		YAML::Node nodeDefConfig;
		int parse();
		
	private:
		bool file_exists(string name);
		int openFiles();
		int parseScreen();
		int parseConfiguration();
		int parseScenario();
		int parseDesign();
		int parsePrincipalCharacter();
		int parseEntities();
};

#endif