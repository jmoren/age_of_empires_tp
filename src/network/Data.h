#ifndef _DATA_H
#define _DATA_H

#include <string>
#include <iostream>
#include <vector>

#define MAX_DATA_SIZE 255

enum MSG_TYPE {
	NEW_PLAYER = 1,
	RE_CONNECTION = 2,
	MOVEMENT = 3,
	DISCONNECTION = 4,
	LOST_CONNECTION = 5,
	GATHER = 6,
	ATTACK = 7,
	BUILD = 8,
	CREATE = 9,
	KEEP_ALIVE = 10,
	CAPTURE = 11
};

enum PLAYER_STATUS {
	CONNECTED = 1,
	DISCONNECTED = 0,
	DELETED = 2
};

enum STATUS{
	IDLE = 0,
	MOVING = 3,
	ATTACKING = 4,
	BUILDING = 5,
	RECOLLECTING = 6
};

struct Point{
	int x;
	int y;
};

struct Number{
	int value;

	void deserialize(char *buff){
		memcpy(this, buff, sizeof(Number));
	}
};

struct Message{
	char name[32];
	int id;
	int type;
	int posx;
	int posy;
	int target;
	bool groupMoving;

	void serialize(char * data) {
		memcpy(data, this, sizeof(Message));
	}

	void deserialize(char * data) {
		memcpy(this, data, sizeof(Message));
	}
};

struct EntityStatus{
	char client[32];
	char name[32];
	int id;
	int posx;
	int posy;
	int state;
	int value;
	bool deleted;

	void deserialize(char * data) {
		memcpy(this, data, sizeof(EntityStatus));
	}
};

struct PlayerStatus{
	int status;
	char name[32];
	int gold;
	int wood;
	int stone;
	int food;

	void deserialize(char * data) {
		memcpy(this, data, sizeof(PlayerStatus));
	}
};

struct ElementStatus{
	int id;
	int posx;
	int posy;
	int value;
	bool deleted;

	void deserialize(char * data) {
		memcpy(this, data, sizeof(ElementStatus));
	}
};

struct GameEntity{
	char client[32];	
	char name[32];
	int id;
	int posx;
	int posy;
	int value;
	bool deleted;

	void deserialize(char* data) {
		memcpy(this, data, sizeof(GameEntity));
	}
};

struct GameElement{
	char client[32];
	char name[32];
	int id;
	int posx;
	int posy;
	int value;
	char kind[15];
	bool walkable;
	bool occupied;
	bool deleted;

	void deserialize(char * data) {
		memcpy(this, data, sizeof(GameElement));
	}
};

struct RemoteConfig{
	char current_name[32];
	int scroll;
	int scrollVelocity;
	int velocity;
	int size_x;
	int size_y;

	void deserialize(char *buff){
		memcpy(this, buff, sizeof(RemoteConfig));
	}

};

struct GameStatus{
	char current_name[32];
	RemoteConfig config;
	std::vector<GameElement> elements;
	std::vector<GameEntity> entities;
	std::vector<PlayerStatus> players;
	std::vector<int> road;
};

struct GameUpdate{
	std::vector<ElementStatus> elements;
	std::vector<EntityStatus> entities;
	std::vector<PlayerStatus> players;
};

#endif