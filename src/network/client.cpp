#include "client.h"

Client::Client(Config *uConfig) {
	config = uConfig;
	logger = Logger::Instance();
	host = config->config.server;
	port = config->config.port;
	name = config->config.name;
	connected = false;
}

int Client::startConnection(){
	logger->log(Logger::TypeMessage::LOG_INFO, "Starting connection to " + this->host + ":" + this->port);

	WSADATA wsaData;
	ConnectSocket = INVALID_SOCKET;

	struct addrinfo *addr_result = NULL;
	struct addrinfo *ptr = NULL;
	struct addrinfo hints;

	result = WSAStartup(MAKEWORD(2, 2), &wsaData);

	if (result != 0) {
		return WINSOCK_ERROR;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family   = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	result = getaddrinfo(host.c_str(), port.c_str(), &hints, &addr_result);

	if (result != 0){
		WSACleanup();
		return BAD_CONNECTION;
	}

	for (ptr = addr_result; ptr != NULL; ptr = ptr->ai_next) {

		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

		if (ConnectSocket == INVALID_SOCKET) {
			WSACleanup();
			logger->log(Logger::TypeMessage::LOG_ERROR, "Error creating socket!");
			return -1;
		}

		result = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);

		if (result == SOCKET_ERROR){
			closesocket(ConnectSocket);
			logger->log(Logger::TypeMessage::LOG_ERROR, "The server is down...");
			ConnectSocket = INVALID_SOCKET;
		}
	}

	freeaddrinfo(addr_result);

	// if connection failed
	if (ConnectSocket == INVALID_SOCKET){
		logger->log(Logger::TypeMessage::LOG_ERROR, "Unable to connect to server!");
		WSACleanup();
		return SOCKET_ERROR;
	}
	else{
		logger->log(Logger::TypeMessage::LOG_INFO, "Connected to server!");
		this->connected = true;
		return SUCCESS;
	}
}

bool Client::reconnect(){
	logger->log(Logger::TypeMessage::LOG_INFO, "Starting reconnection to " + this->host + ":" + this->port);

	ConnectSocket = INVALID_SOCKET;

	struct addrinfo *addr_result = NULL;
	struct addrinfo *ptr = NULL;
	struct addrinfo hints;


	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	result = getaddrinfo(host.c_str(), port.c_str(), &hints, &addr_result);

	if (result != 0){
		WSACleanup();
		return BAD_CONNECTION;
	}

	for (ptr = addr_result; ptr != NULL; ptr = ptr->ai_next) {

		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

		if (ConnectSocket == INVALID_SOCKET) {
			WSACleanup();
			logger->log(Logger::TypeMessage::LOG_ERROR, "Error creating socket!");
			return -1;
		}

		result = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);

		if (result == SOCKET_ERROR){
			closesocket(ConnectSocket);
			logger->log(Logger::TypeMessage::LOG_ERROR, "The server is down...");
			ConnectSocket = INVALID_SOCKET;
		}
	}

	freeaddrinfo(addr_result);

	// if connection failed
	if (ConnectSocket == INVALID_SOCKET){
		logger->log(Logger::TypeMessage::LOG_ERROR, "Unable to reconnect to server!");
		return false;
	}
	else{
		logger->log(Logger::TypeMessage::LOG_INFO, "Connected again to server!");
		this->connected = true;
		return true;
	}
}

int Client::close(){
	int res = closesocket(ConnectSocket);
	return res;
}

int Client::sendMessage(char *buffer, int size){
	int result = 0;
	int count = 0;
	while (count < size){
		result = send(ConnectSocket, &buffer[count], size, 0);
		if (result == 0)
			return 0;
		else if (result == SOCKET_ERROR){
			this->connected = false;
			return SOCKET_ERROR;
		}
		else{
			count += result;
		}
	}

	return count;
}

int Client::readMessage(char *buffer, int size){

	int result = 0;
	int count = 0;

	while (count < size){
		result = recv(ConnectSocket, buffer + count, size, 0);
		if (result == 0)
			return 0;
		else if (result == SOCKET_ERROR){
			return SOCKET_ERROR;
		}
		else{
			count += result;
		}
	}

	return count;
}
