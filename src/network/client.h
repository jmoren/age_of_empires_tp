#include <winsock2.h>
#include <Windows.h>
#include <ws2tcpip.h>
#include <string>
#include "../logger/logger.h"
#include "Data.h"
#include "../parser/config.h"

#define DEFAULT_BUFLEN 512
#define BAD_CONNECTION -10
#define WINSOCK_ERROR -11
#define SUCCESS 0

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

using namespace std;

class Client{

public:
	int result;
	Logger *logger;
	SOCKET ConnectSocket;	
	Client(Config *config);
	~Client();

	int startConnection();
	bool reconnect();

	string port;
	string host;
	string name;
	Config *config;
	bool connected;

	int sendMessage(char *buff, int size);
	int readMessage(char *recBuffer, int size);
	int close();

};