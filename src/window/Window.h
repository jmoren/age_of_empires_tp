#pragma once
#include <math.h>

#include "SDL.h"
#include "SDL_image.h"
#include "../entity/person/Aldeano.h"
#include "../entity/person/Soldado.h"
#include "../parser/config.h"
#include "../logger/logger.h"
#include "../Stage/Headers/Mapa.h"
#include "../Stage/Headers/Timer.h"
#include "../network/client.h"
#include "SDL_ttf.h"
#include "../../src/button/Button.h"

#define WINDOW_FAILED -10
#define CONNECTION_FAILED -11
#define MAPA_FAILED -12

class Window
{
public:
	Window(Config *aConfig);
	~Window(void);
	
	SDL_Window *window;
	Logger *logger;
	Config *config;
	Mapa *map;
	SDL_Renderer *renderer;
	Client	*client;

	int start();
	int startLoop();
	string playerName;

	bool initWindow();	
	bool initMap();
	vector<EntityBase*> selectAllPlayersInSelection(int posX,int posY,int destX,int destY);
	int initConnection(string host);
	
	int parseConnectionResponse();

	int readConfig();
	void loadConfig(char *cdata);
	void loadElements(char* edata, int total); 
	void loadPlayers(char*pdata, int total);
	void loadEntities(char *cdata, int total);
	void loadRoad(char*pdata, int total);
	Sprite* searchSpriteInDictionary(string spriteName);

	HANDLE threadMessages, threadClientSent, threadClientRecv, threadClientKA;
	DWORD  thMessageId, thClientSId, thClientRId, thClientKId;

	string enterName();
	string enterMode();
private:
	Sprite* spriteTemporalCuartel;
	Element* elementTemporalCuartel;
	int margin_scroll;
	int camaraoffset_x;
	int camaraoffset_y;
	void setCamaraOffSet(Mapa *map, int screenWidth,int screenHeight, float zoom);
	int mouse_x;
	int mouse_y;
	int timecheck;
	void getInfoEntity(Mapa *map, int posX, int posY, SDL_Renderer *renderer, int screenWidth, int screenHeight, SDL_Color color, TTF_Font* font, SDL_Texture** txt);
	void getInfoPlayer(Mapa *map, SDL_Renderer *renderer, SDL_Color color, TTF_Font* font, SDL_Texture** txt, string recurso);
	float zoom;
	int VELOCIDAD_SCROLL;

	SDL_Texture* textureMenuInferior;
	SDL_Texture* textureMenuSuperior;
	SDL_Rect SrcRSuperior;
	SDL_Rect DestRSuperior;
	SDL_Rect SrcRInferior;
	SDL_Rect DestRInferior;
	bool isValidTerrainForBuilding;
	void renderMenuInferior(SDL_Renderer* renderer, int screenWidth, int screenHeight);
	void renderMenuSuperior(SDL_Renderer* renderer, int screenWidth, int screenHeight);
	void renderMenues(SDL_Renderer* renderer, int screenWidth, int screenHeight);
	void loadMenuSuperior(SDL_Renderer* renderer, int screenWidth, int screenHeight);
	void loadMenuInferior(SDL_Renderer* renderer, int screenWidth, int screenHeight);
	void centerCameraOnPlayer();
	void renderCuartelTemporal(int mousePosX, int mousePosY);
	void renderInfoRealPlayer(Mapa *map, SDL_Renderer *renderer, SDL_Color color, TTF_Font* font);
	void renderPantallaInicial(SDL_Renderer* renderer, int screenWidth, int screenHeight);
	void renderPantallaCargaDatos(SDL_Renderer* renderer, int screenWidth, int screenHeight);
	string solicitarDatos(string textBox,SDL_Renderer* renderer, int screenWidth, int screenHeight);	
	void mostrarMensaje(string textbox,SDL_Renderer* renderer, int screenWidth, int screenHeight);
	void renderInfoEntity(Mapa *map, int posX,int posY, SDL_Renderer *renderer, int screenWidth, int screenHeight, SDL_Color color, TTF_Font* font);
	void getNamePlayer(Mapa *map, SDL_Renderer *renderer, SDL_Color color, TTF_Font* font, SDL_Texture** txt);
};


