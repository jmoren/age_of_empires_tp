#include "Window.h"
#include <exception>
#include <vector>
#include <queue>
#include "../entity/person/GroupPlayers.h"

struct Pos{
	int t;

	void deserialize(char *c){
		memcpy(this, c, sizeof(Pos));
	};
};
//Walking animation
LTexture gSpriteSheetTexture;

queue<GameUpdate> newMessages;
queue<Message> toSend;

// Threads
DWORD WINAPI messagesThreadHandler(LPVOID argument){
	Mapa *map = (Mapa*)argument;
	
	while (1){
		if (newMessages.size() > 0){
			GameUpdate toProcess;
			toProcess = newMessages.front();
			newMessages.pop();
			map->processMessage(toProcess);
		}
		
	}
		
}

DWORD WINAPI clientReadThreadHandler(LPVOID argument){
	Client *client = (Client*)argument;
	GameUpdate message;

	int result; int total; int size;
	char bufferSize[sizeof(int)] = { 0 };
	char bufferE[sizeof(ElementStatus)] = { 0 };
	char bufferP[sizeof(PlayerStatus)] = { 0 };
	char bufferEn[sizeof(EntityStatus)] = { 0 };

	while (1){

		message.elements.clear();
		message.players.clear();
		message.entities.clear();

		// read players
		result = client->readMessage(bufferSize, sizeof(int));
		total = int(*bufferSize);
		for (size_t i = 0; i < total; i++){
			result = client->readMessage(bufferP, sizeof(PlayerStatus));

			if (result > 0){
				PlayerStatus p;
				p.deserialize(bufferP);
				message.players.push_back(p);

			}
			memset(bufferP, 0, sizeof(bufferP));
		}

		// read elements
		result = client->readMessage(bufferSize, sizeof(int));
		total = int(*bufferSize);
		for (size_t i = 0; i < total; i++){
			result = client->readMessage(bufferE, sizeof(ElementStatus));

			if (result > 0){
				ElementStatus p;
				p.deserialize(bufferE);
				message.elements.push_back(p);

			}
			memset(bufferE, 0, sizeof(bufferE));
		}

		// read entities
		result = client->readMessage(bufferSize, sizeof(int));
		total = int(*bufferSize);
		for (size_t i = 0; i < total; i++){
			result = client->readMessage(bufferEn, sizeof(EntityStatus));

			if (result > 0){
				EntityStatus p;
				p.deserialize(bufferEn);
				message.entities.push_back(p);

			}
			memset(bufferEn, 0, sizeof(bufferEn));
		}

		newMessages.push(message);
	}
	return 0;
}

DWORD WINAPI clientKeepAliveThreadHandler(LPVOID argument){
	Client *client = (Client*)argument;

	int result;
	char buffer[sizeof(Message)] = { 0 };
	Message message;
	message.type = MSG_TYPE::KEEP_ALIVE;
	strncpy_s(message.name, client->name.c_str(), sizeof(message.name));

	while (1){
		message.serialize(buffer);
		result = client->sendMessage(buffer, sizeof(Message));
		if (result < 0){
			return -90;
		}

		Sleep(5000);
	}
	
	return 0;
}

DWORD WINAPI clientSendThreadHandler(LPVOID argument){
	Client *client = (Client*)argument;

	int result;
	char buffer[sizeof(Message)] = { 0 };
	Message message;

	while (1){
		if (toSend.size() > 0){
			message = toSend.front();
			toSend.pop();

			message.serialize(buffer);
			result = client->sendMessage(buffer, sizeof(Message));
			if (result == -1){
				client->connected = false;
			}
		}
	}
	return 0;
}

// Constructor and destructor
Window::Window(Config *aConfig){
	logger = Logger::Instance();
	map = nullptr;
	this->config = aConfig;

	camaraoffset_x = 0;
	camaraoffset_y = 0;
	mouse_x = 0;
	mouse_y = 0;
	zoom = 1.0;
}

Window::~Window(){
	if (map)
		map->~Mapa();

	//Free loaded images
	gSpriteSheetTexture.free();

	//Destroy window	
	if (renderer)
		SDL_DestroyRenderer(renderer);
	if (window)
		SDL_DestroyWindow(window);

	renderer = NULL;
	window = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

// load menues
void Window::loadMenuSuperior(SDL_Renderer* renderer, int screenWidth, int screenHeight)
{
	string pathMenuSuperior = "./resources/images/Menu-Superior.png";
	SDL_Surface* imgMenuSuperior = IMG_Load(pathMenuSuperior.c_str());	

	SrcRSuperior.x = 0;
	SrcRSuperior.y = 0;
	SrcRSuperior.w = screenWidth;
	SrcRSuperior.h = screenHeight;

	DestRSuperior.x = 0;
	DestRSuperior.y = 0;
	DestRSuperior.w = screenWidth;
	DestRSuperior.h = 20;

	textureMenuSuperior = SDL_CreateTextureFromSurface(renderer, imgMenuSuperior);		
}

void Window::renderMenuSuperior(SDL_Renderer* renderer, int screenWidth, int screenHeight)
{
	SDL_RenderCopy(renderer, textureMenuSuperior, &SrcRSuperior, &DestRSuperior);
}

void Window::loadMenuInferior(SDL_Renderer* renderer, int screenWidth, int screenHeight)
{		
	string pathMenuInferior = "./resources/images/Menu-Inferior.jpg";
	SDL_Surface* imgMenuInferior = IMG_Load(pathMenuInferior.c_str());	


	SrcRInferior.x = 0;
	SrcRInferior.y = 0;
	SrcRInferior.w = screenWidth;
	SrcRInferior.h = screenHeight;

	DestRInferior.x = 0;
	DestRInferior.y =screenHeight-220;
	DestRInferior.w = 800;
	DestRInferior.h = 220;

	textureMenuInferior = SDL_CreateTextureFromSurface(renderer, imgMenuInferior);

}


void Window::renderPantallaInicial(SDL_Renderer* renderer, int screenWidth, int screenHeight)
{		
	string pathImgPantallaInicial = "./resources/images/PantallaInicial.jpg";
	SDL_Surface* imgPantallaInicial = IMG_Load(pathImgPantallaInicial.c_str());	

	SDL_Rect srcOrigen;
	SDL_Rect srcDestino;
	srcOrigen.x = 0;
	srcOrigen.y = 0;
	srcOrigen.w = screenWidth;
	srcOrigen.h = screenHeight;

	srcDestino.x = 0;
	srcDestino.y =0;
	srcDestino.w = 800;
	srcDestino.h = 600;

	SDL_Texture* texturePantallaInicial = SDL_CreateTextureFromSurface(renderer, imgPantallaInicial);
	SDL_RenderCopy(renderer, texturePantallaInicial, &srcOrigen, &srcDestino);
	SDL_DestroyTexture(texturePantallaInicial);
	

}


void Window::renderPantallaCargaDatos(SDL_Renderer* renderer, int screenWidth, int screenHeight)
{		
	string pathImgCargaDatos = "./resources/images/CargaDatos.jpg";
	SDL_Surface* imgCargaDatos = IMG_Load(pathImgCargaDatos.c_str());	

	SDL_Rect srcOrigen;
	SDL_Rect srcDestino;
	srcOrigen.x = 0;
	srcOrigen.y = 0;
	srcOrigen.w = screenWidth;
	srcOrigen.h = screenHeight;

	srcDestino.x = 0;
	srcDestino.y =0;
	srcDestino.w = 800;
	srcDestino.h = 600;

	SDL_Texture* texturaCargaDatos = SDL_CreateTextureFromSurface(renderer, imgCargaDatos);
	SDL_RenderCopy(renderer, texturaCargaDatos, &srcOrigen, &srcDestino);
	SDL_DestroyTexture(texturaCargaDatos);
	SDL_FreeSurface(imgCargaDatos);
}

void Window::renderMenuInferior(SDL_Renderer* renderer, int screenWidth, int screenHeight)
{	

	SDL_RenderCopy(renderer, textureMenuInferior, &SrcRInferior, &DestRInferior);
}

void Window::renderMenues(SDL_Renderer* renderer, int screenWidth, int screenHeight){
	renderMenuSuperior(renderer,screenWidth,screenHeight);
	renderMenuInferior(renderer,screenWidth,screenHeight);
}


void Window::loadEntities(char *cdata, int total){
	int offset = 0;
	for (int i = 0; i < total; i++)
	{
		GameEntity entity;
		entity.deserialize(cdata + offset);
		offset += sizeof(GameEntity);

		config->gameEntities.push_back(entity);
	}
}

Sprite* Window::searchSpriteInDictionary(string spriteName){
	return this->map->spriteDictionary->getSpriteFromName(spriteName);
}

// init window, connection, map
void Window::loadElements(char *cdata, int total){
	int offset = 0;
	for (size_t i = 0; i < total; i++){
		GameElement elem;
		elem.deserialize(cdata + offset);
		offset += sizeof(GameElement);
		// add to config 
		if (string(elem.kind).compare("terrain") == 0){
			config->ground.push_back(elem);
		}
		if (string(elem.kind).compare("entity") == 0){
			config->elements.push_back(elem);
		}
		if (string(elem.kind).compare("resource") == 0)
			if (!elem.deleted){
				config->resources.push_back(elem);
			}
	}
}

// init window, connection, map
void Window::loadRoad(char *cdata, int total){
	int offset = 0;
	for (size_t i = 0; i < total; i++){
		Pos num;
		num.deserialize(cdata + offset);
		config->config.road.push_back(num.t);
		offset += sizeof(int);
	}
}

void Window::loadPlayers(char* pdata, int total){
	int offset = 0;
	for (size_t i = 0; i < total; i++){
		PlayerStatus p;
		p.deserialize(pdata + offset);		
		
		config->realPlayers.push_back(p);

		offset += sizeof(PlayerStatus);

		int mapSize = this->map->realPlayersColors.size();
		if (mapSize == 0) {
			this->map->realPlayersColors[p.name] = "Azul";
		}
		else if (mapSize == 1) {
			this->map->realPlayersColors[p.name] = "Rojo";
		}
		else if (mapSize == 1) {
			this->map->realPlayersColors[p.name] = "Verde";
		}
		else {
			this->map->realPlayersColors[p.name] = "Amarillo";
		}
	}
}

void Window::loadConfig(char *cdata){
	RemoteConfig rm;
	
	rm.deserialize(cdata);

	config->config.scroll = rm.scroll;
	config->config.scrollVelocity = rm.scrollVelocity;
	config->config.velocity = rm.velocity;
	config->config.size_x = rm.size_x;
	config->config.size_y = rm.size_y;
	config->character.name = string(rm.current_name);

	// reassign config
	this->config = config;
	this->margin_scroll = config->config.scroll;
	this->VELOCIDAD_SCROLL = config->config.scrollVelocity;

	delete cdata;
}

int Window::readConfig(){

	char totalBuffer[sizeof(int)] = { 0 };
	int recStatus = 0;

	// config
	char *cBuffer = new char[sizeof(RemoteConfig)];
	recStatus = client->readMessage(cBuffer, sizeof(RemoteConfig));
	cout << "Cargando informacion desde el servidor...." << endl;

	if (recStatus < 0)
		return CONNECTION_FAILED;
	else
		//cargo la configuracion.
		this->loadConfig(cBuffer);

	logger->log(Logger::TypeMessage::LOG_INFO, "Iniciando window");
	
	//Instancio Mapa.
	this->initMap();

	// players
	this->mostrarMensaje("Cargando datos...", renderer, this->config->screen.width, this->config->screen.height);
	memset(totalBuffer, 0, sizeof(int));
	recStatus = client->readMessage(totalBuffer, sizeof(int));
	if (recStatus < 0)
		return CONNECTION_FAILED;

	int pSize = int(*totalBuffer);
	if (pSize > 0){
		char *pBuffer = new char[pSize*sizeof(PlayerStatus)];
		recStatus = client->readMessage(pBuffer, pSize*sizeof(PlayerStatus));
		if (recStatus < 0)
			return CONNECTION_FAILED;
		else
			loadPlayers(pBuffer, pSize);

	}
		
	//Cargo los gameElements del mapa
	memset(totalBuffer, 0, sizeof(int));
	recStatus = client->readMessage(totalBuffer, sizeof(int));
	if (recStatus < 0)
		return CONNECTION_FAILED;

	int eSize = int(*totalBuffer);
	if (eSize > 0){
		char *eBuffer = new char[eSize*sizeof(GameElement)];
		recStatus = client->readMessage(eBuffer, eSize*sizeof(GameElement));
		if (recStatus < 0)
			return CONNECTION_FAILED;
		else
			loadElements(eBuffer, eSize);
	}

	//Cargo los gameEntities del mapa:
	//recibo la cantidad de elementos
	memset(totalBuffer, 0, sizeof(int));
	recStatus = client->readMessage(totalBuffer, sizeof(int));
	if (recStatus < 0)
		return CONNECTION_FAILED;

	int entitySize = int(*totalBuffer);
	if (entitySize > 0){
		char *eBuffer = new char[entitySize*sizeof(GameEntity)];
		recStatus = client->readMessage(eBuffer, entitySize*sizeof(GameEntity));
		if (recStatus < 0)
			return CONNECTION_FAILED;
		else
			loadEntities(eBuffer, entitySize);
	}
	
	//Cargo el mapa.
	this->map->load();


	return this->start();
}

bool Window::initWindow(){
	int imgFlags = IMG_INIT_PNG;

	if (SDL_Init(SDL_INIT_EVERYTHING) == -1){
		logger->log(Logger::TypeMessage::LOG_ERROR, "No se inicializ� correctamente SDL");
		return false;
	}

	window = SDL_CreateWindow(config->screen.title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		config->screen.width, config->screen.height, SDL_WINDOW_SHOWN);

	if (window == nullptr){
		logger->log(Logger::TypeMessage::LOG_ERROR, "Error al inicializar pantalla");
		return false;
	}

	if (!(IMG_Init(imgFlags) & imgFlags)){
		logger->log(Logger::TypeMessage::LOG_ERROR, "SDL_image could not initialize!");
		return false;
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	return true;

}

bool Window::initMap(){

	map = new Mapa(renderer, window, config);	
	map->realPlayerName = this->client->name;
	map->screenWidth  = config->screen.width;
	map->screenHeight = config->screen.height;

	if (map == nullptr){
		return false;
	}

	loadMenuSuperior(renderer,map->screenWidth,map->screenHeight);
	loadMenuInferior(renderer,map->screenWidth,map->screenHeight);

	return true;
}

// recibimos un numero de estado (1=iniciar, 2=esperando jugadores, 3=sala llena, 4=player ya existente)
// usamos message para esta config: 
// --- posx => modo que elije el player
// --- posy => la respuesta del server

string Window::solicitarDatos(string textbox,SDL_Renderer* renderer, int screenWidth, int screenHeight){
	LTexture gInputTextTexture;
	LTexture gPromptTextTexture;

	//Main loop flag
	bool quit = false;

	//Event handler
	SDL_Event e;

	//Set text color as white
	SDL_Color textColor = { 255,255,255, 0xFF };
	SDL_Color textColorRed = { 255,0,0, 0xFF };
	TTF_Init();
	TTF_Font* sans = TTF_OpenFont("resources/Sans.ttf", 24); //Set Font and Size

	std::string promptText = textbox;
	gPromptTextTexture.loadFromRenderedText(renderer,promptText.c_str(),sans, textColorRed);
	//The current input text.
	std::string inputText = "";
	gInputTextTexture.loadFromRenderedText(renderer,inputText.c_str(),sans, textColor);

	//Enable text input
	SDL_StartTextInput();

	//While application is running
	while( !quit )
	{
		//The rerender text flag
		bool renderText = false;

		//Handle events on queue
		while( SDL_PollEvent( &e ) != 0 )
		{
			//User requests quit
			if( e.type == SDL_QUIT )
			{
				quit = true;
			}
			//Special key input
			else if( e.type == SDL_KEYDOWN )
			{
				//Handle backspace
				if( e.key.keysym.sym == SDLK_BACKSPACE && inputText.length() > 0 )
				{
					//lop off character
					inputText.pop_back();
					renderText = true;
				}
				//Handle copy
				else if( e.key.keysym.sym == SDLK_c && SDL_GetModState() & KMOD_CTRL )
				{
					SDL_SetClipboardText( inputText.c_str() );
				}
				//Handle paste
				else if( e.key.keysym.sym == SDLK_v && SDL_GetModState() & KMOD_CTRL )
				{
					inputText = SDL_GetClipboardText();
					renderText = true;
				}
				if(e.key.keysym.sym == SDLK_RETURN || e.key.keysym.sym == SDLK_KP_ENTER)
				{
					return inputText;
				}
			}
			//Special text input event
			else if( e.type == SDL_TEXTINPUT )
			{
				//Not copy or pasting
				if( !( ( e.text.text[ 0 ] == 'c' || e.text.text[ 0 ] == 'C' ) && ( e.text.text[ 0 ] == 'v' || e.text.text[ 0 ] == 'V' ) && SDL_GetModState() & KMOD_CTRL ) )
				{
					//Append character
					inputText += e.text.text;
					renderText = true;
				}
			}
		}

		//Rerender text if needed
		if( renderText )
		{
			//Text is not empty
			if( inputText != "" )
			{
				//Render new text
				gInputTextTexture.loadFromRenderedText(renderer,inputText.c_str(),sans, textColor);
			}
			//Text is empty
			else
			{
				//Render space texture
				gInputTextTexture.loadFromRenderedText(renderer," ",sans, textColor);
			}
		}

		//Clear screen
		//SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF );
		
		SDL_RenderClear( renderer );
		this->renderPantallaCargaDatos(renderer,screenWidth,screenHeight);
		//Render text textures
	
		gPromptTextTexture.render( ( screenWidth - gPromptTextTexture.getWidth() ) / 2, 0,nullptr,renderer,0,200 );
		gInputTextTexture.render( ( screenWidth - gInputTextTexture.getWidth() ) / 2, gPromptTextTexture.getHeight(),nullptr, renderer, 0,200 );

		//Update screen
		SDL_RenderPresent( renderer );
	}

	//Disable text input
	SDL_StopTextInput();

	//free 
	gPromptTextTexture.free();
	gInputTextTexture.free();
	TTF_CloseFont( sans );
	sans = NULL;
	TTF_Quit();
}

void Window::mostrarMensaje(string textbox,SDL_Renderer* renderer, int screenWidth, int screenHeight){
	LTexture gPromptTextTexture;

	//Main loop flag
	bool quit = false;

	//Event handler
	SDL_Event e;

	//Set text color as white
	SDL_Color textColor = { 255,255,255, 0xFF };
	
	TTF_Init();
	TTF_Font* sans = TTF_OpenFont("resources/Sans.ttf", 24); //Set Font and Size

	std::string promptText = textbox;
	gPromptTextTexture.loadFromRenderedText(renderer,promptText.c_str(),sans, textColor);
	
	SDL_RenderClear( renderer );
	this->renderPantallaCargaDatos(renderer,screenWidth,screenHeight);
	//Render text textures
	
	gPromptTextTexture.render( ( screenWidth - gPromptTextTexture.getWidth() ) / 2, 0,nullptr,renderer,0,200 );
	
	//Update screen
	SDL_RenderPresent( renderer );
	
	Sleep(2000);
	//free 
	gPromptTextTexture.free();	
	sans = NULL;
	TTF_Quit();
}


int Window::parseConnectionResponse(){

	if (!initWindow()){
		return WINDOW_FAILED;
	}	

	int width = this->config->screen.width;
	int height = this->config->screen.height;

	SDL_RenderClear(renderer);
	this->renderPantallaInicial(renderer,width,height);
	SDL_RenderPresent(renderer);
	Sleep(3000);
	string textBoxName = "Elige un nombre para jugar:";
	string textBoxModo = "Seleccione un objetivo: \n 1) Capturar bandera \n 2) Destruir centro civico \n 3) Matar al rey\n";
	string textBoxIp = "Seleccione la IP del servidor:";
	string playerName = "";
	string mode = "";
	string ip = "";


	while(playerName=="" && mode=="" && ip==""){
		if(playerName == "")
			playerName = this->solicitarDatos(textBoxName,renderer,width,height);	
		if(mode == "" || (mode.compare("3") != 0 && mode.compare("2") != 0 && mode.compare("1") != 0))
			mode = this->solicitarDatos(textBoxModo,renderer,width,height);	
		if(ip =="")
			ip = this->solicitarDatos(textBoxIp,renderer,width,height);	

	}

	char response[sizeof(Message)] = { 0 };
	char sendBuffer[sizeof(Message)] = { 0 };

	Message m;
	Message resp;

	int resultConnection = this->initConnection(ip);

	if(resultConnection == 0)		
	{
		this->mostrarMensaje("Conexi�n Exitosa...", renderer,width,height);

		int res = -1;
		strncpy_s(m.name, playerName.c_str(), sizeof(m.name));
		m.type = 1;
		try{
			m.posx = stoi(mode);
		}catch(exception &e){m.posx = 0;}
		m.posy = 0;

		m.serialize(sendBuffer);
		res = client->sendMessage(sendBuffer, sizeof(Message));

		while (1){
			client->readMessage(response, sizeof(Message));
			resp.deserialize(response);		
			if(resp.posy == 1){			
				this->mostrarMensaje("Iniciando Juego...", renderer,width,height);
				client->name = resp.name;
				config->character.name = resp.name;
				config->character.mode = resp.posx;
				break;
			}
			else if (resp.posy == 2){				
				this->mostrarMensaje("Esperando mas jugadores...", renderer,width,height);
				client->name = resp.name;
				config->character.name = resp.name;
				config->character.mode = resp.posx;
				break;
			}
			else if (resp.posy == 3){				
				this->mostrarMensaje("No se puede conectar ahora, la sala esta llena...  \n Volve a intentarlo mas tarde...", renderer,width,height);

				break;
			}
			else if (resp.posy == 4){

				this->mostrarMensaje("El nombre que elegiste ya existe...",renderer,width,height);
				playerName = this->solicitarDatos(textBoxName,renderer,width,height);

				strncpy_s(m.name, playerName.c_str(), sizeof(m.name));
				m.type = 1;
				mode = this->solicitarDatos(textBoxModo,renderer,width,height);	
				while(mode == "" || (mode.compare("3") != 0 && mode.compare("2") != 0 && mode.compare("1") != 0)){
					this->mostrarMensaje("Esta opcion no esta disponible por el momento.",renderer,width,height);
					mode = this->solicitarDatos(textBoxModo,renderer,width,height);	
				}
				m.posx = stoi(mode);
				m.posy = 0;
				m.serialize(sendBuffer);
				int res = client->sendMessage(sendBuffer, sizeof(Message));
			}
			else if (resp.posy == 5){

				this->mostrarMensaje("Esta opcion no esta disponible por el momento.",renderer,width,height);

				mode = this->solicitarDatos(textBoxModo,renderer,width,height);	
				while(mode == "" || (mode.compare("3") != 0 && mode.compare("2") != 0 && mode.compare("1") != 0)){
					this->mostrarMensaje("Esta opcion no esta disponible por el momento.",renderer,width,height);
					mode = this->solicitarDatos(textBoxModo,renderer,width,height);	
				}

				m.posx = stoi(mode);
				m.posy = 0;
				m.serialize(sendBuffer);
				int res = client->sendMessage(sendBuffer, sizeof(Message));

			}
			else{			
				this->mostrarMensaje("Esta opcion no existe.",renderer,width,height);				
				mode = this->solicitarDatos(textBoxModo,renderer,width,height);	
				while(mode == "" || (mode.compare("3") != 0 && mode.compare("2") != 0 && mode.compare("1") != 0)){
					this->mostrarMensaje("Esta opcion no esta disponible por el momento.",renderer,width,height);
					mode = this->solicitarDatos(textBoxModo,renderer,width,height);	
				}
				
				m.posx = stoi(mode);
				m.posy = 0;
				m.serialize(sendBuffer);
				int res = client->sendMessage(sendBuffer, sizeof(Message));

			}
		}

		return this->readConfig(); 
	}	
}

int Window::initConnection(string host){

	int width = this->config->screen.width;
	int height = this->config->screen.height;


	this->mostrarMensaje("Iniciando conexi�n al server...",renderer,width,height);
				
	string option;

	client = new Client(config);
	client->host = host;
	int result = client->startConnection();
	
	if (result == SUCCESS){
		client->connected = true;
		return 0;
	}
	else{		
		option = this->solicitarDatos("Error conectandose al servidor...\n Para reintentar ingresa 'y' o cualquier otra letra para salir: ",renderer,width,height);
		if (option.compare("y") == 0){
			string textBoxIp = "Seleccione la IP del servidor:";
			string nuevoHost = this->solicitarDatos(textBoxIp,renderer,width,height);
			return this->initConnection(nuevoHost);
		}
		else{
			return -11;
		}
	}
}

// Start and main loop
int Window::start(){
	int status = 0;
	
	logger->log(Logger::TypeMessage::LOG_INFO, "Iniciando window");
	/*if (!initWindow()){
		return WINDOW_FAILED;
	}

	 //init map with the data from server
	if (!initMap())
		return MAPA_FAILED;*/

	threadMessages = CreateThread(NULL, 0, messagesThreadHandler, this->map, 0, &thMessageId);
	threadClientSent = CreateThread(NULL, 0, clientSendThreadHandler, this->client, 0, &thClientSId);
	threadClientRecv = CreateThread(NULL, 0, clientReadThreadHandler, this->client, 0, &thClientRId);
	threadClientKA = CreateThread(NULL, 0, clientKeepAliveThreadHandler, this->client, 0, &thClientKId);

	if (threadClientRecv == NULL || threadClientSent == NULL || threadMessages == NULL || threadClientKA == NULL)
		return -1;

	status = startLoop();
	
	return status;
}

void Window::centerCameraOnPlayer(){
	/*int posX = this->map->player->getRelativePositionX(camaraoffset_x, camaraoffset_y, zoom);
	int posY = this->map->player->getRelativePositionY(camaraoffset_x, camaraoffset_y, zoom);

	this->camaraoffset_x += posX - 200;
	this->camaraoffset_y += posY - 200;*/
}

vector<EntityBase*> Window::selectAllPlayersInSelection(int posX,int posY,int destX,int destY){
	vector<EntityBase*> entitiesSelected;
	//verifico cu�ntos players hay dentro de lo seleccionado

	if(posX <= destX && posY <= destY){
		for(int i=posX; i <= destX;i++){
			for(int j= posY; j <= destY;j++){
								
				EntityBase* e = map->getTile(i,j)->getElement();
				if(e && e->clientName == this->client->name && e->kind == "Unidad")
				{
					entitiesSelected.push_back(e);
					//map->currentEntityBase = e;
				}
			}
		}
		return entitiesSelected;
	}

	if(posX >= destX && posY <= destY){
		for(int i=posX; i >= destX;i--){
			for(int j= posY; j <= destY;j++){
								
				EntityBase* e = map->getTile(i,j)->getElement();
				if(e && e->clientName == this->client->name && e->kind == "Unidad")
				{
					entitiesSelected.push_back(e);
					//map->currentEntityBase = e;
				}
			}
		}
		return entitiesSelected;
	}

	if(posX <= destX && posY >= destY){
		for(int i=posX; i <= destX;i++){
			for(int j= posY; j >= destY;j--){
								
				EntityBase* e = map->getTile(i,j)->getElement();
				if(e && e->clientName == this->client->name && e->kind == "Unidad")
				{
					entitiesSelected.push_back(e);
					//map->currentEntityBase = e;
				}
			}
		}
		return entitiesSelected;
	}

	if(posX >= destX && posY >= destY){
		for(int i=posX; i >= destX;i--){
			for(int j= posY; j >= destY;j--){
								
				EntityBase* e = map->getTile(i,j)->getElement();
				if(e && e->clientName == this->client->name && e->kind == "Unidad")
				{
					entitiesSelected.push_back(e);
					//map->currentEntityBase = e;
				}
			}
		}
		return entitiesSelected;
	}
		return entitiesSelected;

}

int Window::startLoop(){
	Timer fpsTimer;
	Timer capTimer;

	bool quit = false;
	int status = 0;
	int screenWidth;
	int screenHeight;
	int countedFrames = 0;
	int mouseClickX = 0;
	int mouseClickY = 0;
	this->map->currentEntityBase = nullptr;
	this->map->currentGroupPlayer = nullptr;
	SDL_Event event;
	Message senMessage;

	SDL_Texture* MessageEntity = NULL;

	SDL_Rect Message_RectEntity;
	
	TTF_Init();
	TTF_Font* sans = TTF_OpenFont("resources/Sans.ttf", 16); //Set Font and Size
	TTF_Font* sans2 = TTF_OpenFont("resources/Sans.ttf", 20); //Set Font and Size
	SDL_Color black = {0, 0, 0}; //Set Color
	SDL_Color white = {255, 255, 255}; //Set Color
	const int camaraOffsetMinimapa_x = 590;
	const int camaraOffsetMinimapa_y = 500;
	const double zoomMinimapa = 1.3;

	//this->centerCameraOnPlayer();

	bool showButtonCuartel = false;
	bool showButtonAldeano = false;
	bool showButtonSoldado = false;
	bool showButtonArquero = false;

	bool pressButtonCuartel = false;
	bool pressButtonAldeano = false;
	bool pressButtonSoldado = false;
	bool pressButtonPikeman = false;

	//Make the button
	SDL_GetWindowSize(window, &screenWidth, &screenHeight);

	Button buttonCuartel("buttonCuartel", 30, screenHeight-180, 50, 50 );
	Button buttonCrearAldeano("buttonAldeano", 30, screenHeight-180, 50, 50 );
	Button buttonCrearSoldado("buttonSoldado", 30, screenHeight-180, 50, 50 );
	Button buttonCrearPikeman("buttonPikeman", 82, screenHeight-180, 50, 50 );
	spriteTemporalCuartel = nullptr;
	elementTemporalCuartel = nullptr;
	map->currentEntityBase = nullptr;
	int posX = 0;
	int posY = 0;
	while (!quit && client->connected){

		SDL_GetMouseState(&mouse_x, &mouse_y);
	

		setCamaraOffSet(map, screenWidth, screenHeight, zoom);

		SDL_GetTicks();
		int destX = 0;
		int destY = 0;
		bool mousebuttondown = false;
		bool initialSelect = false;
		

		while (SDL_PollEvent(&event)){			
			
			if (event.type == SDL_QUIT){
				quit = true;
			}
			else if (event.type == SDL_MOUSEBUTTONDOWN && !initialSelect){

				posX = map->getIsoCoordinateX(event.button.x, event.button.y, camaraoffset_x, camaraoffset_y, zoom);
				posY = map->getIsoCoordinateY(event.button.x, event.button.y, camaraoffset_x, camaraoffset_y, zoom);
				mousebuttondown = true;
				initialSelect = true;
				break;
				//Sleep(500);
			}

			if(event.type == SDL_MOUSEBUTTONUP ){
				mousebuttondown = false;
				initialSelect = false;

				if (event.button.button == SDL_BUTTON_LEFT){
					
					destX = map->getIsoCoordinateX(event.button.x, event.button.y, camaraoffset_x, camaraoffset_y, zoom);
					destY = map->getIsoCoordinateY(event.button.x, event.button.y, camaraoffset_x, camaraoffset_y, zoom);
					mousebuttondown = false;
					
					pressButtonCuartel = pressButtonAldeano = pressButtonSoldado = pressButtonPikeman = false;

					if(this->map->currentGroupPlayer && this->map->currentGroupPlayer->entities.size() > 0){
						if(this->map->currentGroupPlayer->getClientName().compare(this->client->name) == 0){
							
							if(this->map->currentGroupPlayer->getSpriteName() == "Aldeano"){
								pressButtonCuartel = buttonCuartel.handle_events(event);
								showButtonCuartel = true;
							}
						}
					}

					if(this->map->currentEntityBase){
						if(this->map->currentEntityBase->getClientName().compare(this->client->name) == 0){
							
							if(this->map->currentEntityBase->sprite_name == "Aldeano"){
								pressButtonCuartel = buttonCuartel.handle_events(event);
								showButtonCuartel = true;

							}
							else if(this->map->currentEntityBase->sprite_name =="CentroCivico"){						
								pressButtonAldeano = buttonCrearAldeano.handle_events(event);
								showButtonAldeano = true;
								if(pressButtonAldeano){
									Message msj = map->currentEntityBase->action(map->realPlayerName, this->map->currentEntityBase, this->map->currentEntityBase->posx, this->map->currentEntityBase->posy, false,pressButtonCuartel,pressButtonAldeano,pressButtonSoldado,pressButtonPikeman, isValidTerrainForBuilding);
									if(msj.name != "")
										toSend.push(msj);
								}

							}
							else if(this->map->currentEntityBase->sprite_name == "Cuartel"){
								pressButtonSoldado = buttonCrearSoldado.handle_events(event);	
								pressButtonPikeman = buttonCrearPikeman.handle_events(event);
								showButtonSoldado = true;
								showButtonArquero = true;
								if(pressButtonSoldado || pressButtonPikeman){
									Message msj = map->currentEntityBase->action(map->realPlayerName, this->map->currentEntityBase, this->map->currentEntityBase->posx, this->map->currentEntityBase->posy, false,pressButtonCuartel,pressButtonAldeano,pressButtonSoldado,pressButtonPikeman,isValidTerrainForBuilding);
									if(msj.name != "")
										toSend.push(msj);	
									
								}

							}
						}
					}

					if(this->map->isPositionValid(posX,posY) && this->map->isPositionValid(destX, destY)){
						vector <EntityBase*> entitiesSelected = this->selectAllPlayersInSelection(posX,posY,destX,destY);

						//TODO: Modificar esto para que se vea toda la zona seleccionada
						map->setCurrentTiles(posX, posY, destX, destY);									
						
						EntityBase* e = nullptr;
						GroupPlayers * g = nullptr;
						if(!pressButtonAldeano && !pressButtonPikeman && !pressButtonCuartel && !pressButtonSoldado){
							if(entitiesSelected.size() > 1){
								EntityBase *e = entitiesSelected.back();
								g = new GroupPlayers(entitiesSelected, e->clientName,e->id, e->sprite_name, e->spriteAsociado,e->posx, e->posy);
							}
							else
								e = map->getTile(posX,posY)->getElement();

							map->currentEntityBase = nullptr;
							map->currentGroupPlayer = nullptr;

							if(g && g->getClientName() == this->client->name){
								map->currentGroupPlayer = g;
							}
						}

						showButtonCuartel =	showButtonAldeano = showButtonSoldado = showButtonArquero = false;						
						
						if(g && g->getClientName() == this->client->name && g && g->getSpriteName() == "Aldeano"){
								showButtonCuartel = true;
								map->currentEntityBase = g;
						}

						if(e && e->clientName == this->client->name){
							map->currentEntityBase = e;

							if(e && e->sprite_name == "Aldeano")						
								showButtonCuartel = true;
							else if(e->sprite_name =="CentroCivico")
								showButtonAldeano = true;	
							else if(e->sprite_name == "Cuartel"){						
								showButtonSoldado = true;
								showButtonArquero = true;	
							}


						}
						else if(!pressButtonAldeano && !pressButtonPikeman && !pressButtonCuartel && !pressButtonSoldado){
						
								map->currentEntityBase = nullptr;						
						}
					}				
						
					this->renderInfoEntity(map,posX,posY,renderer,screenWidth,screenHeight,black,sans2);
				

				}				
				else if (event.button.button == SDL_BUTTON_RIGHT){
					destX = map->getIsoCoordinateX(event.button.x, event.button.y, camaraoffset_x, camaraoffset_y, zoom);
					destY = map->getIsoCoordinateY(event.button.x, event.button.y, camaraoffset_x, camaraoffset_y, zoom);
					if (map->currentEntityBase){
						if (map->valid_pos(destX, destY)){
							map->setCurrentTiles(destX, destY, destX, destY);
							((Player*)this->map->currentEntityBase)->clearActions();

							Tile* t = map->getTile(destX, destY);

							Message msj = map->currentEntityBase->action(map->realPlayerName, t->getElement(), t->posx, t->posy, false,pressButtonCuartel,pressButtonAldeano,pressButtonSoldado,pressButtonPikeman,isValidTerrainForBuilding);
							if(msj.name != "")
								toSend.push(msj);

							if (msj.type == MSG_TYPE::BUILD){
								pressButtonCuartel = false;
							}

						}
					}else if(map->currentGroupPlayer){
						if (map->valid_pos(destX, destY)){
							map->setCurrentTiles(destX, destY, destX, destY);

							Tile* t = map->getTile(destX, destY);

							vector<Message> msjs = map->currentGroupPlayer->action(map->realPlayerName, t->getElement(), t->posx, t->posy, true,pressButtonCuartel,isValidTerrainForBuilding);
							for(int i=0; i < msjs.size(); i++)
								if(msjs.at(i).name != ""){
									toSend.push(msjs.at(i));
									if (msjs.at(i).type == MSG_TYPE::BUILD){
										pressButtonCuartel = false;
									}
								}

							((Player*)this->map->currentGroupPlayer)->clearActions();
						}
					}
				}
			}
		}

		SDL_RenderClear(renderer);
		map->render(camaraoffset_x, camaraoffset_y, zoom);
		renderMenues(renderer,screenWidth,screenHeight);
		map->renderMiniatura(camaraOffsetMinimapa_x, camaraOffsetMinimapa_y, zoomMinimapa, (float)camaraoffset_x, (float)camaraoffset_y);
			
	
		this->renderInfoRealPlayer(map,renderer,white,sans);
		if(map->currentEntityBase)			
			this->renderInfoEntity(map,map->currentEntityBase->posx,map->currentEntityBase->posy,renderer,screenWidth,screenHeight,black,sans2);
		else
			this->renderInfoEntity(map,posX,posY,renderer,screenWidth,screenHeight,black,sans2);

		if(showButtonCuartel)
			buttonCuartel.show(renderer,screenWidth,screenHeight);
		if(showButtonAldeano)
			buttonCrearAldeano.show(renderer,screenWidth,screenHeight);
		if(showButtonSoldado)
			buttonCrearSoldado.show(renderer,screenWidth,screenHeight);
		if(showButtonArquero)
			buttonCrearPikeman.show(renderer,screenWidth,screenHeight);

		//Renderizar con opacidad si se puede o no un edificio, segun si el tile esta ocupado y si se presiono buttonCuartel.
		//Se debe renderizar, donde siguiendo la posicion del mouse.
		if(pressButtonCuartel)
		{
			int posMotionX = map->getIsoCoordinateX(mouse_x, mouse_y, camaraoffset_x, camaraoffset_y, zoom);
			int	posMotionY = map->getIsoCoordinateY(mouse_x, mouse_y, camaraoffset_x, camaraoffset_y, zoom);

			this->renderCuartelTemporal(posMotionX,posMotionY);
		}	


		SDL_RenderPresent(renderer);	

		//FPS:
		float avgFPS = countedFrames / (fpsTimer.getTicks() / 1000.f);
		++countedFrames;

		if (!VSYNC_ACTIVE)
		{
			int frameTicks = capTimer.getTicks();
			if (frameTicks < PANTALLA_TICKS_POR_FRAME)
			{
				SDL_Delay(PANTALLA_TICKS_POR_FRAME - frameTicks);
			}
		}

	}
	
	return status;
}

void Window::renderInfoEntity(Mapa *map, int posX,int posY, SDL_Renderer *renderer, int screenWidth, int screenHeight, SDL_Color color, TTF_Font* font){

		SDL_Texture* MessageEntity = NULL;
		getInfoEntity(map, posX, posY, renderer, screenWidth, screenHeight, color, font, &MessageEntity);
				
		//Determino en que parte de la pantalla muestro el nombre, descripci�n del personaje o tile.

		//create a rect
		SDL_Rect Message_RectEntity;
		Message_RectEntity.x = screenWidth/3 + 20;  //controls the rect's x coordinate 
		Message_RectEntity.y = screenHeight-180; // controls the rect's y coordinte
		Message_RectEntity.w = 300; // controls the width of the rect
		Message_RectEntity.h = 100; // controls the height of the rect
				
		SDL_RenderCopy(renderer, MessageEntity, NULL, &Message_RectEntity); 

		SDL_DestroyTexture(MessageEntity);
}

void Window::renderInfoRealPlayer(Mapa *map, SDL_Renderer *renderer, SDL_Color color, TTF_Font* font)
{
		
		SDL_Texture* MessagePlayer = NULL;
		getInfoPlayer(map, renderer, color, font, &MessagePlayer,"Wood");
		SDL_Rect Message_RectPlayer;
		Message_RectPlayer.x = 23;  //controls the rect's x coordinate 
		Message_RectPlayer.y = 1; // controls the rect's y coordinte
		Message_RectPlayer.w = 20; // controls the width of the rect
		Message_RectPlayer.h = 20; // controls the height of the rect
		SDL_RenderCopy(renderer, MessagePlayer, NULL, &Message_RectPlayer); 

		getInfoPlayer(map, renderer, color, font, &MessagePlayer,"Food");

		Message_RectPlayer.x = 70;  //controls the rect's x coordinate 
		Message_RectPlayer.y = 1; // controls the rect's y coordinte
		Message_RectPlayer.w = 20; // controls the width of the rect
		Message_RectPlayer.h = 20; // controls the height of the rect
		SDL_RenderCopy(renderer, MessagePlayer, NULL, &Message_RectPlayer); 


		getInfoPlayer(map, renderer, color, font, &MessagePlayer,"Gold");

		Message_RectPlayer.x = 120;  //controls the rect's x coordinate 
		Message_RectPlayer.y = 1; // controls the rect's y coordinte
		Message_RectPlayer.w = 20; // controls the width of the rect
		Message_RectPlayer.h = 20; // controls the height of the rect
		SDL_RenderCopy(renderer, MessagePlayer, NULL, &Message_RectPlayer); 

		getInfoPlayer(map, renderer, color, font, &MessagePlayer,"Stone");

		Message_RectPlayer.x = 170;  //controls the rect's x coordinate 
		Message_RectPlayer.y = 1; // controls the rect's y coordinte
		Message_RectPlayer.w = 20; // controls the width of the rect
		Message_RectPlayer.h = 20; // controls the height of the rect
		SDL_RenderCopy(renderer, MessagePlayer, NULL, &Message_RectPlayer); 

		getNamePlayer(map, renderer, color, font, &MessagePlayer);
		Message_RectPlayer.x = 600;  //controls the rect's x coordinate 
		Message_RectPlayer.y = 1; // controls the rect's y coordinte
		Message_RectPlayer.w = 80; // controls the width of the rect
		Message_RectPlayer.h = 20; // controls the height of the rect
		SDL_RenderCopy(renderer, MessagePlayer, NULL, &Message_RectPlayer); 

		SDL_DestroyTexture(MessagePlayer);


}
void Window::renderCuartelTemporal(int mousePosX, int mousePosY){
	const string entityName = (string)"Cuartel" + "-" + this->map->realPlayersColors[this->map->realPlayerName];
		
	if(spriteTemporalCuartel){		
		spriteTemporalCuartel->setPixelX(map->getPixelX(mousePosX,mousePosY,camaraoffset_x,camaraoffset_y,zoom));
		spriteTemporalCuartel->setPixelY(map->getPixelY(mousePosX,mousePosY,camaraoffset_x,camaraoffset_y,zoom));
		spriteTemporalCuartel->setPosX(mousePosX);
		spriteTemporalCuartel->setPosY(mousePosY);
		spriteTemporalCuartel->pixelOffsetX = -85;
		spriteTemporalCuartel->pixelOffsetY = -55;
	}
	else
		spriteTemporalCuartel = this->map->getSpriteElement(entityName,mousePosX,mousePosY);

	isValidTerrainForBuilding = this->map->evaluateBuildingArea(mousePosX, mousePosY, 6, 6);
	
	if(elementTemporalCuartel){
		elementTemporalCuartel->blendValue = 255;
		elementTemporalCuartel->kind = "Edificio";
		elementTemporalCuartel->renderWithFilter(camaraoffset_x, camaraoffset_y, zoom, isValidTerrainForBuilding);
		elementTemporalCuartel->posx = mousePosX;
		elementTemporalCuartel->posy = mousePosY;
	}
	else
		elementTemporalCuartel = new Cuartel(this->client->name, 0,entityName,spriteTemporalCuartel, mousePosX, mousePosY);

}


// camara and entities info
void Window::getInfoPlayer(Mapa *map, SDL_Renderer *renderer, SDL_Color color, TTF_Font* font, SDL_Texture** txt, string recurso){
	
	const int longlinea = 800;				
	SDL_Surface* surfaceMessage = TTF_RenderText_Blended_Wrapped(font, map->realPlayer->getInfo(recurso).c_str(), color,longlinea); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first			
	
	SDL_DestroyTexture(*txt);
	(*txt) = SDL_CreateTextureFromSurface(renderer, surfaceMessage); //now you can convert it into a texture

	SDL_FreeSurface(surfaceMessage);	

}

void Window::getNamePlayer(Mapa *map, SDL_Renderer *renderer, SDL_Color color, TTF_Font* font, SDL_Texture** txt){
	
	const int longlinea = 800;				
	string infoPlayer = "Player: " + map->realPlayerName;
	SDL_Surface* surfaceMessage = TTF_RenderText_Blended_Wrapped(font, infoPlayer.c_str(), color,longlinea); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first			
	
	SDL_DestroyTexture(*txt);
	(*txt) = SDL_CreateTextureFromSurface(renderer, surfaceMessage); //now you can convert it into a texture

	SDL_FreeSurface(surfaceMessage);	

}

// camara and entities info
void Window::getInfoEntity(Mapa *map, int posX, int posY, SDL_Renderer *renderer, int screenWidth, int screenHeight, SDL_Color color, TTF_Font* font, SDL_Texture** txt){
	if(map->isPositionValid(posX,posY)){
		Tile *tile = map->getTile(posX, posY);
		SDL_Surface* surfaceMessage;
		const int longlinea = 300;
		if(tile->visited){
			
			if(this->map->currentEntityBase){
				surfaceMessage = TTF_RenderText_Blended_Wrapped(font, this->map->currentEntityBase->getInfo().c_str(), color,longlinea); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first

			}
			else if(this->map->currentGroupPlayer){
				surfaceMessage = TTF_RenderText_Blended_Wrapped(font, this->map->currentGroupPlayer->getInfo().c_str(), color,longlinea); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first

			}
			else{
				EntityBase* e = tile->getElement();
				if(e){
					if(e->kind =="resource"){
						Element* elem = (Element*)e;
						surfaceMessage = TTF_RenderText_Blended_Wrapped(font, elem->getInfo().c_str(), color,longlinea); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first
					}
					else
						surfaceMessage = TTF_RenderText_Blended_Wrapped(font, e->getInfo().c_str(), color,longlinea); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first

				}

				else
				{
					string vacio = "";
					surfaceMessage = TTF_RenderText_Blended_Wrapped(font, vacio.c_str(), color,longlinea); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first

				}	
			}

		}
		else{	
			string vacio = "";
			surfaceMessage = TTF_RenderText_Blended_Wrapped(font, vacio.c_str(), color,longlinea); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first

		}
		SDL_DestroyTexture(*txt);

		(*txt) = SDL_CreateTextureFromSurface(renderer, surfaceMessage); //now you can convert it into a texture

		SDL_FreeSurface(surfaceMessage);
	}
}

void Window::setCamaraOffSet(Mapa *map, int screenWidth, int screenHeight, float zoom){
	if (mouse_x < margin_scroll && mouse_x > 0 && camaraoffset_x > map->getMinPixelX(zoom)){
		int scrollMove = margin_scroll - mouse_x;
		float scrollRate = (float)scrollMove / (float)margin_scroll;
		int moveDistance = scrollRate * VELOCIDAD_SCROLL;
		
		camaraoffset_x -= moveDistance;
	}
	if (mouse_x > screenWidth - margin_scroll && mouse_x < screenWidth - 1 && camaraoffset_x < map->getMaxPixelX(zoom) - screenWidth + TAMANIO_TILE*zoom){
		int scrollMove = margin_scroll - (screenWidth - mouse_x);
		float scrollRate = (float)scrollMove / (float)margin_scroll;
		int moveDistance = scrollRate * VELOCIDAD_SCROLL;

		camaraoffset_x += scrollRate *  VELOCIDAD_SCROLL;
	}
	if (mouse_y > screenHeight - margin_scroll && mouse_y < screenHeight && camaraoffset_y < map->getMaxPixelY(zoom) - screenHeight + TAMANIO_TILE*zoom + 150){
		int scrollMove = margin_scroll - (screenHeight - mouse_y);
		float scrollRate = (float)scrollMove / (float)margin_scroll;
		int moveDistance = scrollRate * VELOCIDAD_SCROLL;

		camaraoffset_y += scrollRate * VELOCIDAD_SCROLL;
	}
	if (mouse_y < margin_scroll && mouse_y > 0 && camaraoffset_y > map->getMinPixelY(zoom)){
		int scrollMove = margin_scroll - mouse_y;
		float scrollRate = (float)scrollMove / (float)margin_scroll;
		int moveDistance = scrollRate * VELOCIDAD_SCROLL;

		camaraoffset_y -= scrollRate * VELOCIDAD_SCROLL;
	}
}